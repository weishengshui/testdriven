#testdriven

#测试驱动开发的艺术 演示代码

## apt根据注解生成文件

cd src\test\java\com\wss\lsl\test\driven\learning

java -d . ExtractInterface.java

javac -d .  ExtractInterfaceProcess.java

javac -d . ExtractInterfaceProcessFactory.java

apt -factory com.wss.lsl.test.driven.learning.ExtractInterfaceProcessFactory Multiply.java -s ./

