package com.wss.lsl.test.driven.learning;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * 引用 测试
 * 
 * @author sean
 * 
 */
public class ReferenceTest {

	@Test
	public void test() {
		BigDecimal b1 = new BigDecimal(1);
		BigDecimal b2 = b1;
		assertEquals(b1, b2);
		b2 = new BigDecimal(2);
		assertFalse(b1 == b2);
	}

}
