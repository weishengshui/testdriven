package com.wss.lsl.test.driven.arithmetic.search;

import org.junit.Test;

/**
 * 二分搜索第一个测试
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public class BinArraySearchFirstTest {

	@Test
	public void testSearch() {
		int count = 10;
		Integer[] data = new Integer[count];
		for (int i = 0; i < count; i += 2) {
			data[i] = i;
			data[i + 1] = i;
		}

		BinSearch<Integer> bsf = new BinArraySearchFirst<Integer>(data);
		int p = bsf.search(2);
		bsf.validate(2, p);

		p = bsf.search(1);
		bsf.validate(1, p);
	}

}
