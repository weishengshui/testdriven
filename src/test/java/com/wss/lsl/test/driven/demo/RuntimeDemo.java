package com.wss.lsl.test.driven.demo;

import org.junit.Test;

public class RuntimeDemo {

	@Test
	public void testAvailableProcessors() {
		int N_CPUS = Runtime.getRuntime().availableProcessors();
		System.out.println(N_CPUS);
	}

}
