package com.wss.lsl.test.driven.itextpdf.html2pdf;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.io.util.UrlUtil;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.DocumentProperties;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.utils.CompareTool;
import com.itextpdf.layout.font.FontProvider;
import com.itextpdf.layout.font.FontSet;
import com.itextpdf.test.ExtendedITextTest;
import com.itextpdf.test.annotations.type.IntegrationTest;

@Category(IntegrationTest.class)
public class Html2PdfTest extends ExtendedITextTest {

    public static final String sourceFolder = "./src/test/resources/com/itextpdf/html2pdf/Html2PdfTest/";
    public static final String destinationFolder = "./target/test/com/itextpdf/html2pdf/Html2PdfTest/";

    @BeforeClass
    public static void beforeClass() {
        createDestinationFolder(destinationFolder);
    }

    @Test
    // TODO DEVSIX-1124
    public void helloParagraphNestedInTableDocumentTest() throws IOException, InterruptedException {
        HtmlConverter.convertToPdf(new File(sourceFolder + "hello_paragraph_nested_in_table.html"), new File(destinationFolder + "hello_paragraph_nested_in_table.pdf"));
        System.out.println("html: file:///" + UrlUtil.toNormalizedURI(sourceFolder + "hello_paragraph_nested_in_table.html").getPath() + "\n");
        Assert.assertNull(new CompareTool().compareByContent(destinationFolder + "hello_paragraph_nested_in_table.pdf", sourceFolder + "cmp_hello_paragraph_nested_in_table.pdf", destinationFolder, "diff03_"));
    }

    @Test
    public void helloParagraphWithSpansDocumentTest() throws IOException, InterruptedException {
        HtmlConverter.convertToPdf(new File(sourceFolder + "hello_paragraph_with_span.html"), new File(destinationFolder + "hello_paragraph_with_span.pdf"));
        System.out.println("html: file:///" + UrlUtil.toNormalizedURI(sourceFolder + "hello_paragraph_with_span.html").getPath() + "\n");
        Assert.assertNull(new CompareTool().compareByContent(destinationFolder + "hello_paragraph_with_span.pdf", sourceFolder + "cmp_hello_paragraph_with_span.pdf", destinationFolder, "diff03_"));
    }

    @Test
    public void geren_daiqian() throws IOException, InterruptedException {
    		
    	// fileToGeren_daiqian();
    	htmlToGeren_daiqian();
    }
    
    public void fileToGeren_daiqian() throws IOException {
    	FontSet fontSet = new FontSet();
    	fontSet.addDirectory("C:\\Windows\\Fonts");
    	fontSet.addDirectory("D:\\tmp\\font");
    	FontProvider fontProvider = new FontProvider(fontSet);
    	ConverterProperties converterProperties = new ConverterProperties();
    	converterProperties.setFontProvider(fontProvider);
    	converterProperties.setBaseUri("https://treport.1hykj.com/");
    	
    	File htmlFile = new File(sourceFolder + "geren_daiqian.html");
    	File pdfFile = new File(destinationFolder + "geren_daiqian.pdf");
    	HtmlConverter.convertToPdf(htmlFile, pdfFile, converterProperties);
    }
    
    private void htmlToGeren_daiqian() throws IOException {
    	FontSet fontSet = new FontSet();
    	fontSet.addDirectory("C:\\Windows\\Fonts");
    	fontSet.addDirectory("D:\\tmp\\font");
    	FontProvider fontProvider = new FontProvider(fontSet);
    	ConverterProperties converterProperties = new ConverterProperties();
    	converterProperties.setFontProvider(fontProvider);
    	converterProperties.setBaseUri(sourceFolder);
    	
    	String html = Files.lines(Paths.get(sourceFolder + "geren_daiqian.html")).collect(Collectors.joining("\n"));
    	
    	File pdfFile = new File(destinationFolder + "geren_daiqian2.pdf");
    	DocumentProperties documentProperties = new DocumentProperties();
    	PdfDocument pdfDocument = new PdfDocument(new PdfWriter(pdfFile), documentProperties);
    	pdfDocument.setDefaultPageSize(PageSize.A2);
    	
    	HtmlConverter.convertToPdf(html, pdfDocument, converterProperties);
    }
}
