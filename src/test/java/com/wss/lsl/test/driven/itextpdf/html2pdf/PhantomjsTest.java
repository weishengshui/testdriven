package com.wss.lsl.test.driven.itextpdf.html2pdf;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.util.UUIDHexGenerator;

public class PhantomjsTest {

	private static final Runtime runtime = Runtime.getRuntime();

	private String pdfDir = "/home/data/logs/risk/front-testhy/";

	ExecutorService executorService = Executors.newFixedThreadPool(10);

	@Before
	public void before() {
		File dir = new File(pdfDir);
		if (dir.exists() == false) {
			dir.mkdirs();
		}
	}

	@Test
	public void test() throws IOException, InterruptedException {
		int count = 1;
		for (int i = 0; i < count; i++) {
			exportPdf();
//			executorService.execute(new Runnable() {
//
//				@Override
//				public void run() {
//					try {
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
//			});
		}

		// executorService.awaitTermination(10, TimeUnit.SECONDS);
	}

	private void exportPdf() throws IOException {
		String jsPath = "D:\\ftoul_workspace\\testdriven\\src\\main\\resources\\static\\js\\export_pdf.js";
		String url = "https://treport.1hykj.com/blackList?id=8a2132686d4214f7016d42379b26000c&expansion=true";
//		String url = "http://192.168.52.197:8080/blackList?id=8a2132686d4214f7016d42379b26000c&expansion=true";
		String pdfFilePath = Stream.of(pdfDir, UUIDHexGenerator.generate(), ".pdf").collect(Collectors.joining());
		Process p = runtime.exec(String.format("phantomjs.exe %s \"%s\" \"%s\"", jsPath, url, pdfFilePath));
		InputStream is = p.getInputStream();
		String res = IOUtils.readLines(is, StandardCharsets.UTF_8).stream().collect(Collectors.joining());
		System.out.println(res);
		if (res != null && res.contains("success")) {
			System.out.println("export pdf success");
		} else {
			System.out.println("fail");
		}
	}

}
