package com.wss.lsl.test.driven.effective.entry43;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

public class MapMergeTest {

	private static final BigInteger TWO = BigInteger.valueOf(2);

	private Map<String, Integer> map;
	private Map<String, List<String>> multiValuesMap;

	@Before
	public void setUp() {
		map = new HashMap<>();
		multiValuesMap = new HashMap<>();
	}

	@Test
	public void test() {
		String key = "apple";
		Assert.assertTrue(map.get(key) == null);

		map.merge(key, 1, (count, incr) -> count + incr);
		Assert.assertTrue(map.get(key) == 1);

		map.merge(key, 1, (count, incr) -> count + incr);
		Assert.assertTrue(map.get(key) == 2);
	}

	@Test
	public void testMultiValuesMap() {
		String key = "paramName";
		Assert.assertTrue(multiValuesMap.get(key) == null);

		multiValuesMap.merge(key, new ArrayList<>(Arrays.asList("1")), (t, u) -> {
			t.addAll(u);
			return t;
		});
		Assert.assertTrue(1 == multiValuesMap.get(key).size());

		multiValuesMap.merge(key, new ArrayList<>(Arrays.asList("1")), (t, u) -> {
			t.addAll(u);
			return t;
		});
		Assert.assertTrue(2 == multiValuesMap.get(key).size());
	}

	static Stream<BigInteger> primes() {
		return Stream.iterate(BigInteger.valueOf(2), BigInteger::nextProbablePrime);
	}

	@Test
	public void test1() {
		BigInteger bi = BigInteger.valueOf(2).pow(2);
		System.out.println(bi);
	}

	public static void main(String[] args) {
		primes().limit(10).forEach(System.out::println);
		primes().map(p -> TWO.pow(p.intValueExact()).subtract(BigInteger.ONE))
				.filter(mersenne -> mersenne.isProbablePrime(50)).limit(20).forEach(System.out::println);
	}

	enum EnumA {
		A1, A2, A3, A4
	}

	enum EnumB {
		B1, B2
	}

	static class CoupleC {
		public final EnumA aa;
		public final EnumB bb;

		public CoupleC(EnumA aa, EnumB bb) {
			super();
			this.aa = aa;
			this.bb = bb;
		}

		@Override
		public String toString() {
			return "CoupleC [aa=" + aa + ", bb=" + bb + "]";
		}
	}

	@Test
	public void test2() {
		Stream.of(EnumA.values()).flatMap(aa -> Stream.of(EnumB.values()).map(bb -> new CoupleC(aa, bb)))
				.forEach(System.out::println);

		System.out.println();
		Stream.of(EnumA.values()).map(aa -> Stream.of(EnumB.values()).map(bb -> new CoupleC(aa, bb))).flatMap(t -> t)
				.forEach(System.out::println);

		String text = "";
		Message<String> message = JSONObject.parseObject(text, new MessageStringTypeReference());
		System.out.println();
		System.out.println(message);
	}

	@Test
	public void test3() {
		System.out.println(1 & 1); // 最后一位是1，输出1
		System.out.println(2 & 1); // 最后一位是0，输出0
		System.out.println(3 & 1); // 最后一位是1，输出1
	}
}
