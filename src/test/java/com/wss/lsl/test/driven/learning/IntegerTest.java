package com.wss.lsl.test.driven.learning;

import static org.junit.Assert.*;

import org.junit.Test;

public class IntegerTest {

	@Test
	public void test() {
		Integer a = 100;
		Integer b = 100;
		assertTrue(a == b);
		
		a = 1000;
		b = 1000;
		assertFalse(a == b);
		
		assertEquals(0, -12 % 2);
		assertEquals(-1, -11 % 2);
		
		Integer num = 1;
		assertTrue(num == 1);
	}
	
	@Test
	public void test2(){
		// parseInt效率最高
		String a = "123456";
		
		int count = 100000;
		
		System.out.println("=========================================");
		long start = System.currentTimeMillis();
		for(int i = 0 ; i < count; i++){
			Integer.valueOf(a);
		}
		System.out.println("valueOf: " + (System.currentTimeMillis() - start));
		
		System.out.println("=========================================");
		start = System.currentTimeMillis();
		for(int i = 0 ; i < count; i++){
			Integer.parseInt(a);
		}
		System.out.println("parseInt: " + (System.currentTimeMillis() - start));
		
		System.out.println("=========================================");
		start = System.currentTimeMillis();
		for(int i = 0 ; i < count; i++){
			Integer.decode(a);
		}
		System.out.println("decode: " + (System.currentTimeMillis() - start));
	}

}
