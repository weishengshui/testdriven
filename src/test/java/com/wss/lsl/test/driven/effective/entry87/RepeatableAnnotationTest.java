package com.wss.lsl.test.driven.effective.entry87;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.Assert;
import org.junit.Test;

public class RepeatableAnnotationTest {

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	@Repeatable(Filters.class)
	static @interface Filter {

		String value();
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	static @interface Filters {

		Filter[] value();
	}

	@Filter("1")
	@Filter("2")
	public interface Filtersable {

	}

	@Filters({ @Filter("1"), @Filter("2") })
	public interface Filtersable2 {

	}

	@Test
	public void test() {
		int i = 1;
		for (Filter f : Filtersable.class.getAnnotationsByType(Filter.class)) {
			Assert.assertTrue((i + "").equals(f.value()));
			i++;
		}

		i = 1;
		Filters fs = Filtersable.class.getAnnotation(Filters.class);
		for (Filter f : fs.value()) {
			Assert.assertTrue((i + "").equals(f.value()));
			i++;
		}
	}

	@Test
	public void test2() {
		int i = 1;
		for (Filter f : Filtersable2.class.getAnnotationsByType(Filter.class)) {
			Assert.assertTrue((i + "").equals(f.value()));
			i++;
		}

		i = 1;
		Filters fs = Filtersable2.class.getAnnotation(Filters.class);
		for (Filter f : fs.value()) {
			Assert.assertTrue((i + "").equals(f.value()));
			i++;
		}
	}

}
