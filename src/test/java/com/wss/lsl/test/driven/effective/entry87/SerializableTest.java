package com.wss.lsl.test.driven.effective.entry87;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

/**
 * 序列化前后版本加减字段，下面的4个用例都能通过
 * 
 * @author weiss
 *
 */
public class SerializableTest {

	private static class Member implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

//		private String id = "1111";
//
//		public String getId() {
//			return id;
//		}
	}

	// 在执行前将id字段删除
	@Test
	public void testWriteNoField() throws FileNotFoundException, IOException {

		Member m = new Member();
		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(new File("e:\\tmp\\java\\member_no_field")))) {
			oos.writeObject(m);
		}
	}

//	@Test
//	public void testReadNoFieldWithNewFieldVersion() throws FileNotFoundException, IOException, ClassNotFoundException {
//
//		try (ObjectInputStream ois = new ObjectInputStream(
//				new FileInputStream(new File("e:\\tmp\\java\\member_no_field")))) {
//			Member m = (Member) ois.readObject();
//			System.out.println(m.getId());
//		}
//	}

	@Test
	public void testWriteField() throws FileNotFoundException, IOException {
		Member m = new Member();
		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(new File("e:\\tmp\\java\\member_with_field")))) {
			oos.writeObject(m);
		}
	}

	// 在执行前将id字段删除
	@Test
	public void testReadFieldWithNewNoFieldVersion() throws FileNotFoundException, IOException, ClassNotFoundException {

		try (ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(new File("e:\\tmp\\java\\member_with_field")))) {
			Member m = (Member) ois.readObject();
			System.out.println(m);
		}
	}

	@Test
	public void testFloorMod() {

		Assert.assertFalse(Math.floorMod(-1, 10) == 1);
		Assert.assertTrue(Math.floorMod(1, 10) == 1);
	}

	@Test
	public void testLastIndexOf() {
		String str = "aaaa";
		// 是从字符串尾端检索的第一个字符
		Assert.assertTrue(str.lastIndexOf("a") == str.length() - 1);
		// 是从fromIndex开始检索的第一个子串
		Assert.assertTrue(str.lastIndexOf("a", 1) == 1);

		// U+1D546
		// 是辅助字符，用两个代码单元表示
		str = "\uD835\uDD46";
		Assert.assertTrue(str.length() == 2); // length()表示的是代码单元的数量
		System.out.println(str.charAt(0)); // 是输出不了字符的
		System.out.println(str.charAt(1)); // 是输出不了字符的
		int index = str.offsetByCodePoints(0, 0);
		int cp = str.codePointAt(index);
		// 这个码点使用两个代码单元表示的
		Assert.assertTrue(Integer.toHexString(cp).equalsIgnoreCase("1D546"));

	}

	@Test
	public void testFormat() {

		// + 标志输出数字的符号
		Assert.assertTrue("+333.33".equals(String.format("%+6.2f", 333.33)));
		Assert.assertTrue("-333.33".equals(String.format("%+6.2f", -333.33)));

		// 0标志在数字前面补0
		Assert.assertTrue("00333.33".equals(String.format("%08.2f", 333.33)));
		Assert.assertTrue("-0333.33".equals(String.format("%08.2f", -333.33)));

		// 空格，在数字之前添加空格
		Assert.assertTrue("  333.33".equals(String.format("% 8.2f", 333.33)));
		Assert.assertTrue(" -333.33".equals(String.format("% 8.2f", -333.33)));

		// -标志左对齐
		Assert.assertTrue("333.33  ".equals(String.format("%-8.2f", 333.33)));
		Assert.assertTrue("-333.33 ".equals(String.format("%-8.2f", -333.33)));

		// (标志将负数括在括号内
		Assert.assertTrue("333.33".equals(String.format("%(.2f", 333.33)));
		// 注意负号不见了
		Assert.assertTrue("(333.33)".equals(String.format("%(.2f", -333.33)));
		Assert.assertTrue("(333.330000)".equals(String.format("%(f", -333.33)));

		// ,标志添加分组分隔符
		Assert.assertTrue("3,333.33".equals(String.format("%,(.2f", 3333.33)));
		// 注意负号不见了
		Assert.assertTrue("(3,333.33)".equals(String.format("%,(.2f", -3333.33)));

		// #标志对f包含小数点
		// 好像#对f没啥用
		Assert.assertTrue("3333.1".equals(String.format("%#.1f", 3333.123)));
		// #标志对16进制加前缀0x
		Assert.assertTrue("d05".equals(String.format("%x", 3333)));
		Assert.assertTrue("D05".equals(String.format("%X", 3333)));
		Assert.assertTrue("0xd05".equals(String.format("%#x", 3333)));
		Assert.assertTrue("0XD05".equals(String.format("%#X", 3333)));
		// #标志对8进制加前缀0
		Assert.assertTrue("6405".equals(String.format("%o", 3333)));
		Assert.assertTrue("06405".equals(String.format("%#o", 3333)));

		// $指定参数索引，一个参数可以多次格式化
		Assert.assertTrue("1 0x1".equals(String.format("%1$d %1$#x", 1)));

		// <标志格式化前面说明的值
		Assert.assertTrue("1 0x1".equals(String.format("%d %<#x", 1)));
	}

	@Test
	public void testMapPut() {
		Map<String, String> map = new HashMap<>(1);
		map.put("1", "1");

		Assert.assertTrue("hello: 11".equals("hello: " + 1 + 1));
		Assert.assertTrue("hello: 11, xx".equals("hello: " + 1 + 1 + ", xx"));

		int a = 10;
		int[] is = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
		int index = -1;
		while (a-- > 0) {
			// 输出9~0
			index++;
			Assert.assertTrue(a == is[index]);
		}
		Assert.assertTrue(0 == is[index]); // 校验，最后的定位是0

		a = 10;
		index = -1;
		while (--a > 0) {
			// 输出9~1
			index++;
			Assert.assertTrue(a == is[index]);
		}
		Assert.assertTrue(1 == is[index]); // 校验，最后的定位是1

		byte b = 0x7f; // 最大 byte 型16进制值 0111_1111;
		Assert.assertTrue(0b0111_1111 == b);

		// 科学计数法:e/E是以10为基数的低
		Assert.assertTrue(Double.compare(1000.0, 10e2) == 0);

		Assert.assertTrue(false || true);
		Assert.assertTrue(true | false);
		Assert.assertTrue(true ^ false);
		Assert.assertTrue(false ^ true);
		Assert.assertFalse(false ^ false);
		Assert.assertFalse(true ^ true);

		int i = 1 << 32;
		Assert.assertTrue(1 == i);
		i = 1 >> 32;
		Assert.assertTrue(1 == i);
		i = 1 >> 1;
		Assert.assertTrue(0 == i);
		i = 1 >> 33;
		Assert.assertTrue(0 == i);
		i = -2 >>> 1; // 以补码格式移位操作
		Assert.assertTrue(0b0111_1111_1111_1111_1111_1111_1111_1111 == i);

		// 字符串开头，拼接的字符串，没有执行数字加法
		// 字符串在后面，拼接的字符串，前面的数字执行了算术运算
		System.out.println("==============================");
		int x = 0, y = 1, z = 2;
		String s = "x, y, z ";
		Assert.assertTrue("x, y, z 012".equals(s + x + y + z));
		Assert.assertTrue("x, y, z 012".equals(s + 0 + 1 + 2));
		Assert.assertTrue("x, y, z 012".equals("x, y, z " + 0 + 1 + 2));
		Assert.assertTrue("hello: 01".equals("hello: " + 0 + 1));
		Assert.assertTrue("11 hello".equals(10 + 1 + " hello"));

	}

	/**
	 * 测试空指针情况
	 * 
	 */
	@Test(expected = NullPointerException.class)
	public void testStringSwitch() {

		stringSwitch(null);
	}

	@Test
	public void testStringSwitch2() {

		stringSwitch("1");
		stringSwitch("2");
	}

	private void stringSwitch(String str) {

		switch (str) {
		case "1":
			System.out.println(str);
			break;
		case "2":
			System.out.println(str);
			break;
		}
	}

	/**
	 * 测试标签
	 * 
	 */
	@Test
	@SuppressWarnings("unused")
	public void testLabel() {

		int i = 0, j = 0;
		outer: for (; i < 10; i++) {
			for (; j < 10; j++) {
				break outer;
			}
		}
		Assert.assertTrue(i == 0);
		Assert.assertTrue(j == 0);

		i = 0;
		j = 0;
		outer: for (; i < 10; i++) {
			for (; j < 10; j++) {
				continue outer;
			}
		}
		Assert.assertTrue(i == 10);
		Assert.assertTrue(j == 0);
	}

	@Test
	public void testStream() {
		String str = "hello world ABC hello2world";
		// \pL 表达式匹配所有字母
		// \PL 是上面表达式取反，非字母
		Stream<String> stream = Pattern.compile("\\P{L}+").splitAsStream(str);
		List<String> splitedList = stream.collect(toList());
		Assert.assertEquals(Arrays.asList("hello", "world", "ABC", "hello", "world"), splitedList);

		int sum = IntStream.rangeClosed(0, 100).sum();
		Assert.assertTrue(5050 == sum);

		sum = IntStream.rangeClosed(0, 100).reduce(0, (x, y) -> x + y);
		Assert.assertTrue(5050 == sum);

		int arrayCount = 10;
		Stream<IntStream> ints = Stream.generate(() -> IntStream.rangeClosed(1, 100)).limit(arrayCount);
		IntSummaryStatistics intSummaryStatistics = ints.collect(Collectors.summarizingInt(t -> t.sum()));
		Assert.assertTrue(5050 * arrayCount == intSummaryStatistics.getSum());

		List<String> list = Arrays.asList("1", "2", "1");
		Assert.assertEquals(Arrays.asList("1", "2"), list.stream().distinct().collect(toList()));

		Optional<Date> optional = getOptional1().flatMap(Optional1To2::get).flatMap(Optional2To3::getOptionalDate);
		Assert.assertTrue(optional.isPresent());

		optional = getOptional1().flatMap(Optional1To2::get).flatMap(Optional2To3::getNullOptionalDate);
		Assert.assertFalse(optional.isPresent());

		optional = getOptional1().flatMap(Optional1To2::getNull).flatMap(Optional2To3::getNullOptionalDate);
		Assert.assertFalse(optional.isPresent());

		Stream<Stream<Stream<String>>> flatTest = Stream.empty();
		Stream<String> strings = flatTest.flatMap(t -> t).flatMap(t -> t);
		Assert.assertTrue(0 == strings.count());

		// toArray(String[]::new); // 为什么这种不行

		toArray2(TreeSet::new);
	}

	@SuppressWarnings("unused")
	private String[] toArray(Supplier<String[]> t) {
		return t.get();
	}

	private <E> TreeSet<E> toArray2(Supplier<TreeSet<E>> t) {
		return t.get();
	}

	private Optional<Optional1To2> getOptional1() {
		return Optional.of(new Optional1To2());
	}

	static class Optional1To2 {

		public Optional<Optional2To3> get() {
			return Optional.of(new Optional2To3());
		}

		public Optional<Optional2To3> getNull() {
			return Optional.empty();
		}
	}

	static class Optional2To3 {

		public Optional<Date> getOptionalDate() {
			return Optional.of(new Date());
		}

		public Optional<Date> getNullOptionalDate() {
			return Optional.empty();
		}
	}

	@Test
	public void testOptionalDouble() {
		Optional<Double> optional = getDoubleOptional1(null).flatMap(this::getDoubleOptional2);
		Assert.assertFalse(optional.isPresent());

		Double x = -1.0;
		optional = getDoubleOptional1(x).flatMap(this::getDoubleOptional2);
		Assert.assertTrue(Double.compare(x - 20, optional.get()) == 0);

		x = -0.0;
		optional = getDoubleOptional1(x).flatMap(this::getDoubleOptional2);
		Assert.assertTrue(Double.compare(x - 20, optional.get()) == 0);

		x = 0.1;
		optional = getDoubleOptional1(x).flatMap(this::getDoubleOptional2);
		Assert.assertTrue(Double.compare(x + 4, optional.get()) == 0);
	}

	private Optional<Double> getDoubleOptional1(Double x) {
		if (x == null) {
			return Optional.empty();
		}
		return x <= 0 ? Optional.of(x - 10) : Optional.of(x + 2);
	}

	private Optional<Double> getDoubleOptional2(Double y) {

		return y <= 0 ? Optional.of(y - 10) : Optional.of(y + 2);
	}

	static class A {

		protected Date hireDay;
	}

	static class C extends A {

	}

	static class B extends A {

		public void xxx(A a) {
			System.out.println(a.hireDay);
		}

		public void xxx2(C c) {
			System.out.println(c.hireDay);
		}
	}

	@Test
	public void testProtected() {

		new B();
	}

	@Test
	public void testToString() {
		Object[] arr = { 1, 2, 3 };
		Assert.assertTrue(arr.toString().startsWith("[Ljava.lang.Object;@"));

		int[] arr2 = { 1, 2, 3 };
		Assert.assertTrue(arr2.toString().startsWith("[I@")); // 基本类型没有分号 FIXME

		Integer[] arr3 = { 1, 2, 3 };
		Assert.assertTrue(arr3.toString().startsWith("[Ljava.lang.Integer;@"));

	}

	@Test
	public void test1() {
		Assert.assertTrue(64578 == "ABC".hashCode());
		Assert.assertTrue(2107246 == "DRSS".hashCode());
	}

	static enum Enum1 {
		A {
			@Override
			public void get() {
				// TODO Auto-generated method stub
			}
		};

		public abstract void get();
	}

	@Test
	public void test2() {
		String clazzName = "com.wss.lsl.test.driven.effective.entry87.SerializableTest$Enum1$1";
		// getClass()是子类化最具体的名字，匿名内部类能看出不同
		Assert.assertEquals(clazzName, Enum1.A.getClass().getName());

		clazzName = "com.wss.lsl.test.driven.effective.entry87.SerializableTest$Enum1";
		Assert.assertEquals(clazzName, Enum1.A.getDeclaringClass().getName()); // 返回定义枚举类本身
	}

	@Test
	public void test3() {
		// 数组类型class没有域
		Class<?> c = int[].class;
		Field[] fields = c.getDeclaredFields();
		Assert.assertTrue(0 == fields.length);

		// 返回数组的组件类型，即元素的类型
		// 在获取数组中元素类型有用
		// getComponentType()

		// 基本类型直接是基本类型的名字
		Assert.assertEquals("int", c.getComponentType().getName());
		Assert.assertEquals("int", c.getComponentType().toString());
		// toString()多了"class "前缀
		Assert.assertEquals("class java.util.Date", Date[].class.getComponentType().toString());
		Assert.assertEquals("java.util.Date", Date[].class.getComponentType().getName());
	}

	static class AAA implements Cloneable {

		protected String age;
		public String phoneNo;
	}

	static class BBB extends AAA {

		private String name;
		private String code;
		private String email;

		{
			System.out.println(age);
		}

		public String getName() {
			return this.name;
		}

		public String getCode() {
			return this.code;
		}

		public String getEmail() {
			return this.email;
		}
	}

	@Test
	public void test4() {
		Field[] fields = AAA.class.getFields(); // 只返回公有域
		Assert.assertTrue(1 == fields.length);

		fields = AAA.class.getDeclaredFields(); // 自己定义的所有域，不包含上级
		Assert.assertTrue(2 == fields.length);

		fields = BBB.class.getFields(); // 包含自己和上级所有的公有域
		Assert.assertTrue(1 == fields.length);
		// 返回定义该字段的类或接口
		// getDeclaringClass()
		Assert.assertEquals(AAA.class, fields[0].getDeclaringClass());

		fields = BBB.class.getDeclaredFields(); // 自己定义的所有域，不包含上级
		Assert.assertEquals(3, fields.length);
	}

	/** 无返回值测试 */
	protected static void noReturnTest() {

	}

	/** 基本类型测试 */
	protected static int primitiveTest() {
		return 0;
	}

	/** 对象类型测试 */
	protected static Date notPrimitiveTest() {
		return new Date();
	}

	protected static Date notPrimitiveTest(int i) {
		return new Date();
	}

	protected static LocalDate notPrimitiveTest(Integer i) {
		return LocalDate.now();
	}

	@Test
	public void test5() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		// case 1：无返回值测试
		Method m = SerializableTest.class.getDeclaredMethod("noReturnTest");
		Assert.assertNull(m.invoke(null));

		// case 2：基本类型测试
		m = SerializableTest.class.getDeclaredMethod("primitiveTest");
		Assert.assertNotNull(m.invoke(null));
		int i = (Integer) m.invoke(null);
		Assert.assertEquals(0, i);

		// case 3：对象类型测试
		m = SerializableTest.class.getDeclaredMethod("notPrimitiveTest");
		Assert.assertNotNull((Date) m.invoke(null));

		m = SerializableTest.class.getDeclaredMethod("notPrimitiveTest", new Class<?>[] { int.class });
		Assert.assertNotNull((Date) m.invoke(null, 10));

		m = SerializableTest.class.getDeclaredMethod("notPrimitiveTest", new Class<?>[] { Integer.class });
		Assert.assertNotNull((LocalDate) m.invoke(null, 10));
	}

	static class CloneSupper implements Cloneable {

		private String name = "张三";

		public String getName() {
			return name;
		}

		// 必须覆盖clone()方法，并设为public
		@Override
		public CloneSupper clone() throws CloneNotSupportedException {
			return (CloneSupper) super.clone();
		}
	}

	static class CloneChild extends CloneSupper {

		private Date birthday = new Date();
		private Date expireDate = new Date();

		public Date getBirthday() {
			return birthday;
		}

		public Date getExpireDate() {
			return expireDate;
		}

		@Override
		public CloneChild clone() throws CloneNotSupportedException {
			CloneChild cc = (CloneChild) super.clone();
			cc.expireDate = (Date) cc.expireDate.clone();
			return cc;
		}

	}

	@Test
	public void test6() throws CloneNotSupportedException {
		CloneSupper cs = new CloneSupper();
		CloneSupper copy = cs.clone();
		Assert.assertTrue(cs != copy);
		Assert.assertTrue(cs.getName().equals(copy.getName()));

		CloneChild cc = new CloneChild();
		CloneChild ccCopy = (CloneChild) cc.clone();
		Assert.assertTrue(cc != ccCopy);
		Assert.assertTrue(cc.getName().equals(ccCopy.getName()));
		Assert.assertTrue(cc.getBirthday().equals(ccCopy.getBirthday()));
		Assert.assertTrue(cc.getBirthday() == ccCopy.getBirthday()); // 默认不是深度拷贝

		Assert.assertTrue(cc.getExpireDate().equals(ccCopy.getExpireDate()));
		// 需要在子类中定义深度拷贝才有用
		Assert.assertTrue(cc.getExpireDate() != ccCopy.getExpireDate());

		Date[] dates = { new Date() };
		Date[] copyDates = dates.clone();
		Assert.assertTrue(dates[0] == copyDates[0]); // 数组拷贝不是深度拷贝
	}

	@Test
	public void test7() {
		String[] strings = {};
		Arrays.sort(strings, String::compareToIgnoreCase);
		// Class::instanceMethod
		// (x1, x2)-> x1.instanceMethod(x2)

		List<String> list = new ArrayList<>();
		list.add("346");
		list = list.stream().filter(Predicate.isEqual("123")).collect(toList());
		Assert.assertTrue(0 == list.size());

		list = new ArrayList<>();
		list.add("346");
		list.add("123");
		list.add("123");
		list = list.stream().filter(Predicate.isEqual("123")).collect(toList());
		Assert.assertTrue(2 == list.size());

		list = new ArrayList<>();
		list.add("346");
		list.add("123");
		list.add("123");
		list.add(null);
		list = list.stream().filter(Predicate.isEqual(null)).collect(toList());
		Assert.assertTrue(1 == list.size());

		Predicate<String> pre = (String t) -> t == null;
		Object o = pre;
		Assert.assertNotNull(o);

		// Object o2 = (String t) -> t == null; // lambda表达式不能直接赋值给Object
	}

}
