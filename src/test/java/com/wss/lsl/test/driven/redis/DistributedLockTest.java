package com.wss.lsl.test.driven.redis;

import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2017年12月12日
 * @copyright wonhigh.cn
 */
public class DistributedLockTest {

	private DistributedLock distributedLock;
	private JedisPool jedisPool;

	private static final int THREAD_COUNT = 50;
	// 线程池模拟竞争
	private ExecutorService executorService = Executors
			.newFixedThreadPool(THREAD_COUNT);

	@Before
	public void setup() throws Exception {
		String host = "172.17.194.59";
		int port = 6348;

		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(500);

		jedisPool = new JedisPool(poolConfig, host, port);

		distributedLock = new DistributedLock(jedisPool);
	}

	@After
	public void teardown() throws Exception {
		System.out.println("teardown");
		// jedisPool.close();
	}

	@Test
	public void testAcquireLock() {

		String key = "locked_key";
		// 每次设定唯一的值
		String value = UUID.randomUUID().toString();
		int lockTime = 100;
		int waitTime = 101;
		boolean locked = distributedLock.acquireLock(key, value, lockTime,
				waitTime);
		// 校验锁定成功
		Assert.assertTrue(locked);
		distributedLock.releaseLock(key, value);

		value = UUID.randomUUID().toString();
		waitTime = 5;
		locked = distributedLock.acquireLock(key, value, lockTime, waitTime);
		// 校验锁定成功
		Assert.assertTrue(locked);
		distributedLock.releaseLock(key, value);
	}

	@Test
	public void testReleaseLock() throws Exception {

		final CyclicBarrier cb = new CyclicBarrier(THREAD_COUNT + 1);
		final Random random = new Random();
		final AtomicInteger ai = new AtomicInteger();

		// 50个线程模拟竞争条件
		for (int i = 0; i < THREAD_COUNT; i++) {
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					boolean locked = false;
					String key = "locked_key";
					String value = UUID.randomUUID().toString();
					int lockTime = 10;
					int waitTime = 11;
					try {
						cb.await();

						locked = distributedLock.acquireLock(key, value,
								lockTime, waitTime);

						System.out.println(ai.getAndIncrement());
						if (!locked) {
							// 没有获取到分布式锁
							return;
						}
						System.out.println("线程："
								+ Thread.currentThread().getName()
								+ " 获取到了分布式锁 ");
						System.out.println(new Date());

						// 模拟业务执行时间
						TimeUnit.MILLISECONDS.sleep(5000 + random
								.nextInt(10000));

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						if (locked) {
							distributedLock.releaseLock(key, value);
							System.out.println("线程："
									+ Thread.currentThread().getName()
									+ " 释放了分布式锁 ");
							System.out.println(new Date());
						}
					}
				}
			});
		}

		cb.await();

		System.in.read();
	}
	
	/**
	 * 比较流水线和普通方式执行命令的效率
	 * 
	 * @author wei.ss
	 */
	@Test
	public void testPipeline() {
		Jedis jedis = jedisPool.getResource();
		
		long begin = System.currentTimeMillis();
		Pipeline pipeline = jedis.pipelined();
		int count = 10000;
		
		// 
		for(int i = 0; i < count; i++) {
			pipeline.set(i+"", "" + i);
		}
		System.out.println((System.currentTimeMillis() - begin));
		pipeline.sync();
		// System.out.println(response);
		
		begin = System.currentTimeMillis();
		for(int i = 0; i < count; i++) {
			jedis.set(i+"", "" + i);
		}
		System.out.println((System.currentTimeMillis() - begin));
		
		
		// pipeline.exec();
		// pipeline.sync();
	}

}
