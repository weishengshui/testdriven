package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import org.apache.wicket.protocol.http.WebApplication;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.page.MyHomePage;

public class ApplicationClassTest {

	@Test
	public void testHomePageHasBeenDefined() {

		WebApplication app = new MyWicketApp();

		assertEquals(MyHomePage.class, app.getHomePage());
	}

}
