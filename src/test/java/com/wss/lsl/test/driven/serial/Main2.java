package com.wss.lsl.test.driven.serial;


import java.io.*;

public class Main2{
	
	public static void main(String[] args) throws Exception {
		
		String fileName = "test.out";
		
		System.out.println("read begin");
		
		A a = new A(22);
		System.out.println("a="+a);
		
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
		A b = (A)ois.readObject();
		A c = (A)ois.readObject();
		@SuppressWarnings("unchecked")
		Class<A> aType = (Class<A>)ois.readObject();
		ois.close();
		
		System.out.println("b="+b);
		System.out.println("c="+c);
		
		System.out.println("read b age=" + b.getAge());
		System.out.println("read c age=" + c.getAge());
		// 静态域序列化需要手工显示处理。
		System.out.println("A class static age2=" + aType.getField("age2").get(b));
		
		System.out.println("end");
	}
}