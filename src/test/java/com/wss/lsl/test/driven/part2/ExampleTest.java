package com.wss.lsl.test.driven.part2;

import org.easymock.EasyMock;
import org.easymock.EasyMockRule;
import org.easymock.Mock;
import org.easymock.MockType;
import org.easymock.TestSubject;
import org.junit.Rule;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.dao.UserDao;
import com.wss.lsl.test.driven.part2.entity.User;

public class ExampleTest {

	@Rule
	public EasyMockRule mockRule = new EasyMockRule(this);

	@TestSubject
	private ClassUnderTest underTest = new ClassUnderTest();

	@Mock(type=MockType.NICE, name="niceUserDao", fieldName="niceUserDao")
	private UserDao niceUserDao;
	
	@Mock(type=MockType.STRICT, name="strictUserDao",  fieldName="strictUserDao")
	private UserDao strictUserDao;

	private User user = new User();
	
	@Test
	public void testNiceType() {
		EasyMock.replay(niceUserDao);
		underTest.niceSave(user);
		EasyMock.verify(niceUserDao);

	}
	
	@Test
	public void testStrictType() {
		
		EasyMock.expect(strictUserDao.save(EasyMock.isA(User.class))).andReturn(1);
		EasyMock.replay(strictUserDao);
		underTest.strictSave(user);
		EasyMock.verify(strictUserDao);
	}

}
