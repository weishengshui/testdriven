package com.wss.lsl.test.driven.learning;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * TODO: 增加描述
 * 
 * @author wei.ss
 * @date 2017年4月17日
 * @copyright wonhigh.cn
 */
public class TestBigDecimal {

	@Test
	public void test() {

		BigDecimal smsCost = new BigDecimal("203.32");
		BigDecimal consumptionAmount = new BigDecimal("380636");

		String inputOutputRatio = calcInputOutputRatio(smsCost,
				consumptionAmount);
		System.out.println(inputOutputRatio);
	}

	private String calcInputOutputRatio(BigDecimal smsCost,
			BigDecimal consumptionAmount) {
		smsCost = smsCost == null ? BigDecimal.ZERO : smsCost;
		consumptionAmount = consumptionAmount == null ? BigDecimal.ZERO
				: consumptionAmount;
		smsCost.setScale(2, BigDecimal.ROUND_HALF_UP);
		consumptionAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
		if (smsCost.doubleValue() > 0) {
			consumptionAmount = consumptionAmount.multiply(BigDecimal.ONE
					.divide(smsCost, 10, BigDecimal.ROUND_HALF_UP));
			smsCost = BigDecimal.ONE;
		}

		consumptionAmount.setScale(0, BigDecimal.ROUND_HALF_UP);
		String inputOutputRatio = smsCost.intValue() + ":"
				+ consumptionAmount.intValue();

		return inputOutputRatio;
	}
	
	@Test
	public void test2(){
		double a = 1.1;
		System.out.println(Math.ceil(a));
		a = 1.8;
		System.out.println(Math.ceil(a));
	}
}
