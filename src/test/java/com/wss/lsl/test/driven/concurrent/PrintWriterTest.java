package com.wss.lsl.test.driven.concurrent;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Test;

public class PrintWriterTest {

	private PrintWriter printWriter;

	@Test
	public void test() throws IOException {
		printWriter = new PrintWriter(System.out);

		printWriter.println("控制台输出");
		printWriter.flush();
		
		File logFile = new File("/tmp/log.txt");
		if(!logFile.exists()) {
			logFile.createNewFile();
		}
		printWriter = new PrintWriter(logFile);

		printWriter.println("文件输出");
		printWriter.flush();

		System.out.println("控制台输出2");
	}

}
