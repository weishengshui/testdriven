package com.wss.lsl.test.driven.part2;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CounterTest {

	private Counter counter;
	private Runnable runnable;
	final int numberOfThreads = 20;
	final int incrementOfPerThread = 10000;

	@Before
	public void setUp() {
		counter = new Counter();
		runnable = new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < incrementOfPerThread; i++) {
					counter.increment();
				}
			}
		};
	}

	/**
	 * 测试计数器的基本功能
	 */
	@Test
	public void testBasicFunctionality() {
		Assert.assertEquals(0, counter.value());
		counter.increment();
		Assert.assertEquals(1, counter.value());
		counter.increment();
		Assert.assertEquals(2, counter.value());
	}

	/**
	 * 测试计数器的线程安全性
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testForThreadSafety() throws InterruptedException {

		for (int i = 0; i < numberOfThreads; i++) {
			new Thread(runnable).start();
		}

		Thread.sleep(3000);
		Assert.assertEquals(numberOfThreads * incrementOfPerThread,
				counter.value());
	}

	@Test
	public void testForThreadSafetyUsingCyclicBarrierToMaximizeConcurrency()
			throws InterruptedException, BrokenBarrierException {

		// + 1 表示当前线程
		CyclicBarrier entryBarrier = new CyclicBarrier(numberOfThreads + 1);
		CyclicBarrier exitBarrier = new CyclicBarrier(numberOfThreads + 1);

		for (int i = 0; i < numberOfThreads; i++) {
			new SynchedThread(runnable, entryBarrier, exitBarrier).start();
		}

		Assert.assertEquals(0, counter.value());

		entryBarrier.await();
		exitBarrier.await();

		Assert.assertEquals(numberOfThreads * incrementOfPerThread,
				counter.value());
	}
}
