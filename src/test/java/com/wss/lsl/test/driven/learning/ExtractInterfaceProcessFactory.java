//package com.wss.lsl.test.driven.learning;
//
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.Collections;
//import java.util.Set;
//
//import com.sun.mirror.apt.AnnotationProcessor;
//import com.sun.mirror.apt.AnnotationProcessorFactory;
//import com.sun.mirror.apt.AnnotationProcessorEnvironment;
//import com.sun.mirror.declaration.AnnotationTypeDeclaration;
//
//@SuppressWarnings({ "deprecation", "restriction" })
//public class ExtractInterfaceProcessFactory implements
//		AnnotationProcessorFactory {
//
//	@Override
//	public AnnotationProcessor getProcessorFor(
//			Set<AnnotationTypeDeclaration> arg0,
//			AnnotationProcessorEnvironment env) {
//		return new ExtractInterfaceProcess(env);
//	}
//
//	@Override
//	public Collection<String> supportedAnnotationTypes() {
//		return Arrays
//				.asList("com.wss.lsl.test.driven.learning.ExtractInterface");
//	}
//
//	@Override
//	public Collection<String> supportedOptions() {
//		return Collections.emptyList();
//	}
//}
