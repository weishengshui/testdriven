package com.wss.lsl.test.driven.concurrent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BarrierTimerTest {

	private BlockingQueue<Object> bq;
	private BarrierTimer barrierTimer = new BarrierTimer();
	private CyclicBarrier barrier;

	private int nPair;
	private int entryNum;
	private ExecutorService executorService = Executors.newCachedThreadPool();

	public BarrierTimerTest() {
	}

	public BarrierTimerTest(int nPair, int capacity, int entryNum) {
		super();
		this.nPair = nPair;
		this.barrier = new CyclicBarrier(this.nPair * 2 + 1, barrierTimer);
		bq = new ArrayBlockingQueue<Object>(capacity);
		this.entryNum = entryNum;
	}

	/**
	 * CyclicBarrier可以循环使用。barrierTimer统计开始关卡到结束关卡之间的时间，
	 * 每到一次关卡就执行一次barrierTimer。所以我们发现
	 */
	public static void main(String[] args) throws InterruptedException,
			BrokenBarrierException {

		int nPair = 100;
		int capacity = 10;
		int entryNum = 10;

		BarrierTimerTest test = new BarrierTimerTest(nPair, capacity, entryNum);
		test.test();

		// final BarrierTimerTest test = new BarrierTimerTest();
		// BarrierTimer barrierTimer = test.new BarrierTimer();
		// barrierTimer.clear();
		// test.barrier = new CyclicBarrier(2 + 1, barrierTimer);
		// Thread thread = new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// test.barrier.await();
		// Thread.sleep(10000);
		// test.barrier.await();
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// } catch (BrokenBarrierException e) {
		// e.printStackTrace();
		// }
		// }
		// });
		// thread.start();
		// Thread thread2 = new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// try {
		// test.barrier.await();
		// Thread.sleep(10000);
		// test.barrier.await();
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// } catch (BrokenBarrierException e) {
		// e.printStackTrace();
		// }
		// }
		// });
		// thread2.start();
		// test.barrier.await();
		// test.barrier.await();
		//
		// System.out.println(barrierTimer.getTime());
	}

	public void test() throws InterruptedException, BrokenBarrierException {
		barrierTimer.clear();
		for (int i = 0; i < nPair; i++) {
			executorService.execute(new Producer());
			executorService.execute(new Consumer());
		}
		executorService.shutdown();
		barrier.await();
		barrier.await();
		long time = barrierTimer.getTime();
		System.out.println(time);
		System.out.println(time / (nPair * entryNum));
	}

	class BarrierTimer implements Runnable {

		private boolean started = false;
		private long startTime, endTime;

		@Override
		public synchronized void run() {
			long t = System.nanoTime();
			if (!started) {
				started = true;
				startTime = t;
			} else {
				endTime = t;
			}
		}

		public synchronized void clear() {
			started = false;
		}

		public long getTime() {
			return endTime - startTime;
		}
	}

	class Producer implements Runnable {

		@Override
		public void run() {
			try {
				barrier.await();
				for (int i = 0; i < entryNum; i++) {
					bq.put(new Object());
				}
				Thread.sleep(10);
				barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}

	}

	class Consumer implements Runnable {

		@Override
		public void run() {
			try {
				barrier.await();
				for (int i = 0; i < entryNum; i++) {
					bq.take();
				}
				Thread.sleep(10);
				barrier.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (BrokenBarrierException e) {
				e.printStackTrace();
			}
		}

	}

}
