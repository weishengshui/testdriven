package com.wss.lsl.test.driven.util;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

public class ZipFileUtilsTest {

	private File inputFile = new File("E:\\tmp\\elasticjob-lite-demo");
	private File outputFile = new File("f:\\tmp.zip");

	@Test
	public void testWriteToZipFile() throws InterruptedException {
		ZipFileUtils.writeToZipFile(inputFile, outputFile);
		Assert.assertTrue(outputFile.exists());
	}

	@Test
	public void testReadFromZipFile() throws Exception {
		testWriteToZipFile();

		inputFile = outputFile;
		outputFile = new File("E:\\tmp2");
		ZipFileUtils.readFromZipFile(inputFile, outputFile);
		Assert.assertTrue(outputFile.exists());
	}

}
