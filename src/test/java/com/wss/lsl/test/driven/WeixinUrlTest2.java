package com.wss.lsl.test.driven;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.common.SignUtil;

/**
 * 生成微信url
 * 
 * @author sean
 *
 */
@SuppressWarnings("unused")
public class WeixinUrlTest2 {
	
	// 补丁授权url
	private static final String patch_url="http://patch.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 开发授权url
	private static final String dev_url="http://dev.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 测试授权url
	private static final String test_url="http://test.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 用户测试授权url
	private static final String usertest_url="http://usertest.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 预测试授权url
	private static final String pre_url="http://pre.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 生产授权url
	private static final String wx_url="http://wx.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 培训环境授权url
	private static final String demo_url="http://demo.qxclub.cn/wechat/wx/authorize.do?"
				+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	private String redirectUrl;
	
	@Before
	public void setUp(){
		redirectUrl = "https://www.baidu.com";
	}
	
	/**
	 * 测试补丁
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testPatch() throws UnsupportedEncodingException {
		String original_id = "gh_7c80f28e26a6";
		String brandCode = "TM";
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(patch_url, redirectUrl, original_id, authSign);
		
		System.out.println("补丁环境地址: \n" + finalUrl);
	}
	
	private String getAuthSign2(String redirectUrl, String originalId){
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("redirectUrl", redirectUrl);
		treeMap.put("oauthTypeBase", "1");
		treeMap.put("originalId", originalId);
		treeMap.put("type", "1");
		
		String sign = SignUtil.createSign(treeMap, originalId);
		return sign;
	}
	
	/**
	 * 测试开发
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testDev() throws UnsupportedEncodingException {
		String original_id = "gh_5fdb1e85433c";
		String brandCode = "MA";

		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(dev_url, redirectUrl, original_id, authSign);
		
		System.out.println("开发环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试培训环境 
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void testDemo() throws UnsupportedEncodingException {
		String original_id = "gh_4eec2c0da696";
		String brandCode = "MA";
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(demo_url, redirectUrl, original_id, authSign);
		
		System.out.println("培训环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试“测试”
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testTest() throws UnsupportedEncodingException {
		String original_id = "gh_b3825b7a38c5";
		String brandCode = "TM";
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(test_url, redirectUrl, original_id, authSign);
		
		System.out.println("测试环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试“用户测试”
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testUserTest() throws UnsupportedEncodingException {
		String original_id = "gh_c348e222ab0f";
		String brandCode = "TM";
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(usertest_url, redirectUrl, original_id, authSign);
		
		System.out.println("用户测试环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试“预测试”
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testPreTest() throws UnsupportedEncodingException {
		String original_id = "gh_a1a0a310bc42";
		String brandCode = "JP";
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(pre_url, redirectUrl, original_id, authSign);
		
		System.out.println("预测试环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试生产
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testWx() throws UnsupportedEncodingException{
		String original_id = "gh_e0614ad15746";
		String brandCode = "TM";
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(wx_url, redirectUrl, original_id, authSign);
		
		System.out.println("正式环境地址: \n" + finalUrl);
	}

}
