package com.wss.lsl.test.driven.effective;

import org.junit.Assert;
import org.junit.Test;

/**
 * 克隆学习
 * 
 * @author weiss
 *
 */
public class CloneTest {

	static class Member implements Cloneable {

		@Override
		protected Member clone() {
			try {
				return (Member) super.clone();
			} catch (Exception e) {
				throw new AssertionError();
			}
		}
	}

	static class A implements Cloneable {
		private Member member;
//		private final Member member;

		public A() {
			this.member = new Member();
		}

		public Member getMember() {
			return member;
		}

		@Override
		public A clone() {
			try {
				A a = (A) super.clone();
				// 要克隆的引用属性，不能用final修饰
				a.member = member.clone(); // member如果是常量，无法重新复制新的值
				return a;
			} catch (CloneNotSupportedException e) {
				throw new AssertionError();
			}
		}
	}

	static class B extends A {

		private Member member2;

		public B() {
			this.member2 = new Member();
		}

		public Member getMember2() {
			return member2;
		}

		@Override
		public B clone() {
			B b = (B) super.clone();
			b.member2 = member2.clone();
			return b;
		}
	}

	@Test
	public void testClone() {
		A a1 = new A();
		A a2 = a1.clone();

		Assert.assertTrue(a1 != a2);
		Assert.assertTrue(a1.getMember() != a2.getMember());
	}

	@Test
	public void testCloneExtends() {
		B b1 = new B();
		B b2 = b1.clone();

		Assert.assertTrue(b1 != b2);
		Assert.assertTrue(b1.getClass() == b2.getClass());
		Assert.assertTrue(b1.getMember2() != b2.getMember2());
	}

	@Test
	public void test1() {

		double d = Double.NEGATIVE_INFINITY + 1;
		Assert.assertTrue(Double.compare(d, Double.NEGATIVE_INFINITY) == 0);
		Assert.assertFalse(Double.NaN == Double.NaN + 1);

		Assert.assertFalse(Double.compare(0.0, -0.0) == 0); // double认为0.0和-0.0是不一样的
	}

}
