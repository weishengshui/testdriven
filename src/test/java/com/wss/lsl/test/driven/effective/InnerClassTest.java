package com.wss.lsl.test.driven.effective;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

/**
 * 内部类、嵌套类、匿名类、局部类的测试
 * 
 * @author weiss
 *
 */
public class InnerClassTest {

	private String parentProperty = "hello";
	private static String staticProperty = "static property"; // 静态属性

	/**
	 * 非静态内部类
	 * 
	 * @author weiss
	 *
	 */
	class NotStaticInnerClass {
//		private static String staticPro = ""; // 不能定义静态属性

//		public static void staticMethod() { // 不能定义静态方法
//			
//		}

		public void method() {
			System.out.println(InnerClassTest.this.parentProperty);

			InnerClassTest.this.test1();
		}
	}

	/**
	 * 内部类：只有静态内部类可以定义静态属性、静态方法。其他3中类型都不可以定义。
	 * 
	 * @author weiss
	 *
	 */
	static class StaticInnerClass {
		public static String staticPro = "staticPro"; // 静态属性

		public static void staticMethod() { // 静态方法

		}

		public void method() {
			// 不能访问宿主类的实例属性和方法
//			System.out.println(InnerClassTest.this.parentProperty);
//
//			InnerClassTest.this.test1();
		}
	}

	/**
	 * 匿名类测试
	 */
	@Test
	public void test1() {

		System.out.println("非静态上下文测试");
		parentProperty = "12333";
		Comparator<String> comparator = new Comparator<String>() {

//			public static String staticPro = "0000"; // 不能定义静态属性

			@Override
			public int compare(String o1, String o2) {
				method();
				System.out.println(InnerClassTest.this.parentProperty);
				return 0;
			}

			public void method() {// 这个方法对外部来说是不可见的

			}

//			public static void staticMethod() { // 不能定义静态方法
//				
//			}
		};

		Assert.assertTrue(comparator instanceof Comparator);
		System.out.println(comparator.compare("1", "2"));

		System.out.println("\n静态上下文测试");
		staticMethod();
	}

	private static void staticMethod() {

		String localProperty = "xxx"; // 局部属性
		Comparator<String> comparator = new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
//				System.out.println(InnerClassTest.this.parentProperty);  // 非法的，无法引用
				System.out.println(staticProperty);
				System.out.println(localProperty);
				return 0;
			}
		};

		System.out.println(comparator.compare("1", "2"));
	}

	/**
	 * 测试局部类
	 */
	@Test
	public void testLocalClass() {

		String localVariable = "localVariable";
		class A {

//			public static String staticPro = "0000"; // 不能定义静态属性

			public void method() {
				System.out.println(parentProperty);
				System.out.println(localVariable);
				System.out.println(staticProperty);
			}

//			public static void staticMethod() { // 不能定义静态方法
//				
//			}
		}

		A a = new A();
		a.method();
		System.out.println(new A());
	}

	static class AA {

	}

	static class B extends AA {

	}

	@Test
	public void test2() {
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");

//		list.toArray(new String[0]); // 入参数组的元素个数小于list的元素个数，将创建一个新的数组
		System.out.println(Arrays.toString(list.toArray(new String[0])));
	}

	public void test3(AA a) {

	}

	public void test3(B b) {

	}

	@SuppressWarnings("rawtypes")
	@Test
	public void test4() {
		List<Object> list = new ArrayList<>();
		list.add("111");

		System.out.println(list);
		System.out.println(list instanceof List<?>);
//		System.out.println(list instanceof List<Object>); // 非法的，因为类型删除 
		System.out.println(list.getClass());

		List list2 = new ArrayList();
		System.out.println(list2.getClass());

		List<?> list3 = list2;
		System.out.println(list3 instanceof List);
		System.out.println(list3);
	}

	public <T> void test5(Class<T> c, @SuppressWarnings("unchecked") T... t) {

	}

	@Test
	public void test6() {
		test5(Integer.class, 1, 2, 3);
		test5(String.class, "1", "2", "3");
	}

	static class C<T> {

		private T[] ts;

		public void setTs(T[] ts) {
			this.ts = ts;
		}

		public T[] getTs() {
			return this.ts;
		}
	}

	static class Chooser<T> {
		private final T[] ts;

		@SuppressWarnings("unchecked")
		public Chooser(Collection<T> c) {
//			ts = c.toArray(new T[0]); // compile error  数组初始化的类型必须是具体的
//			ts = new T[0]; // compile error 
			ts = (T[]) c.toArray(); // compile warnning
		}

		public T[] getTs() {
			return this.ts;
		}

	}

	@Test
	public void test7() {
		List<String> a = Collections.emptyList(); // 不可变的。单例就够了，不用担心别的引用修改值。

		List<String> b = Collections.emptyList();

//		a.add("1");

		System.out.println(a == b);
	}

	abstract class CCC {

		public void method() {
			System.out.println(parentProperty);
		}
	}

	@Test
	public void test8() {
		System.out.println(InnerClassTest.CCC.class);
	}

	// 内部接口是static的，静态的
	interface DDD {

	}

	@Test
	public void test9() {
		System.out.println(InnerClassTest.DDD.class);
	}

	static class Member {
		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	}

	@Test
	public void testGroupingBy() {
		List<Member> members = new ArrayList<>();

		members.stream().collect(Collectors.groupingBy(p -> p.getId()));
		members.stream().collect(Collectors.groupingBy(p -> p.getId(), () -> new TreeMap<>(), Collectors.toList()));
	}
}
