package com.wss.lsl.test.driven.mqtt;

import org.junit.Assert;
import org.junit.Test;

public class LengthTest {

	@Test
	public void test() {
		int x = 1;
		Assert.assertEquals(1, calcLength(x));
		x = 56;
		Assert.assertEquals(1, calcLength(x));
		x = 127;
		Assert.assertEquals(1, calcLength(x));
		x = 128;
		Assert.assertEquals(2, calcLength(x));
		
		x = 129;
		Assert.assertEquals(2, calcLength(x));
	}
	 
	private int calcLength(int x){
		
		int digit = 0;
		do{
			digit = x % 128;
			System.out.println(digit);
			x = x / 128;
			if(x > 0){
				digit = digit | 0x80;
			} 
			System.out.println(digit);
		} while(x > 0);
		
		return digit;
	}

}
