package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class LearningBigDecimalTest {

	@Test
	public void testRoundHalfUp() {
		assertTrue(0 == new BigDecimal("0.004").setScale(2,
				BigDecimal.ROUND_HALF_UP).doubleValue());
		
		double d = 0.004;
		String s = "0.004";
		System.out.println(new BigDecimal(d).doubleValue());
		System.out.println(new BigDecimal(s).doubleValue());
		
		d = 0.1;
		s = "0.1";
		System.out.println(new BigDecimal(d).doubleValue());
		System.out.println(new BigDecimal(s).doubleValue());
		
		System.out.println(new BigDecimal(d).toString());
		System.out.println(new BigDecimal(s).toString());
	}

}
