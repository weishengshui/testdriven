package com.wss.lsl.test.driven.learning;

import org.junit.Assert;
import org.junit.Test;

/**
 * 枚举学习
 * 
 * @author wei.ss
 * @date 2016年11月8日
 * @copyright wonhigh.cn
 */
public class EnumTest {

	static enum EnumDemo {

		A(1), B(2), C(3);

		private int type;

		EnumDemo(int type) {
			this.type = type;
		}

		public int getType() {
			return this.type;
		}
	}

	@Test
	public void test1() {

		String str = "A";
		EnumDemo enumDemo = Enum.valueOf(EnumDemo.class, str);

		Assert.assertTrue(enumDemo.equals(EnumDemo.A));

		str = "sss";
		try {
			enumDemo = Enum.valueOf(EnumDemo.class, str);
			Assert.fail();
		} catch (Exception e) {
		}
		
		try {
			enumDemo = Enum.valueOf(EnumDemo.class, null);
			Assert.fail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
