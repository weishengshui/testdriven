package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.exception.MissingValueException;

public class TestTemplate {

	private Template template;

	@Before
	public void setUp() {
		template = new Template("${one}, ${two}, ${three}");
		template.set("one", "1");
		template.set("two", "2");
		template.set("three", "3");
	}

	@Test
	public void multipleVariables() {
		assertTemplateEvaluatesTo("1, 2, 3");
	}

	@Test(expected = MissingValueException.class)
	public void variablesValueNotFound() {
		new Template("${foo}").evaluate();
	}

	@Test
	public void variablesValueNotFound2() {
		try {
			new Template("${foo}").evaluate();
		} catch (MissingValueException e) {
			assertEquals("No value for ${foo}", e.getMessage());
		}
	}

	@Test
	public void unknownVariablesAsIgnore() {
		template.set("doesnotexists", "whatever");
		assertTemplateEvaluatesTo("1, 2, 3");
	}

	private void assertTemplateEvaluatesTo(String expected) {
		assertEquals(expected, template.evaluate());
	}

	// 以下是重构之前的测试
	// 单变量模版
	@Test
	public void oneVariable() {
		Template template = new Template("Hello, ${name}");
		template.set("name", "Reader");
		assertEquals("Hello, Reader", template.evaluate());
	}

	@Test
	public void differentValue() {
		Template template = new Template("Hello, ${name}");
		template.set("name", "some one");
		assertEquals("Hello, some one", template.evaluate());
	}

	@Test
	public void differentTemp() {
		Template template = new Template("Hi, ${name}");
		template.set("name", "Reader");
		assertEquals("Hi, Reader", template.evaluate());
	}

	/**
	 * 多变量模版
	 */
	@Test
	public void moreThanOneValue() {
		Template template = new Template("${one}, ${two}, ${three}");
		template.set("one", "1");
		template.set("two", "2");
		template.set("three", "3");
		assertEquals("1, 2, 3", template.evaluate());
	}

	@Test(expected = MissingValueException.class)
	public void valueNotFound() {
		Template template = new Template("${one}, ${two}, ${three}");
		template.set("one", "1");
		template.set("three", "3");
		assertEquals("1, ${two}, 3", template.evaluate());
	}

	@Test
	public void particularValue() {
		Template template = new Template("${one}, ${two}, ${three}");
		template.set("one", "1");
		template.set("two", "${foo}");
		template.set("three", "3");
		assertEquals("1, ${foo}, 3", template.evaluate());
	}

	@Test
	public void unknownVariablesAreIngore() {
		Template template = new Template("Hello, ${name}");
		template.set("name", "Reader");
		template.set("doesnotexists", "Hi");
		assertEquals("Hello, Reader", template.evaluate());
	}

}
