package com.wss.lsl.test.driven.part2;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp.BasicDataSource;
import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.junit.Assert;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.dao.impl.JdbcTemplatePersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;

public class PersonDaoDbUnitIntegrationTest extends DbUnitIntegrationTestCase {

	private List<Person> expected;
	private String lastname = "smith";

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		expected = createListOfPersonWithLastname(lastname);
	}

	@Test
	public void testFindAll() {
		JdbcTemplatePersonDao dao = new JdbcTemplatePersonDao();
		dao.setDataSource(new BasicDataSource() {
			@Override
			public Connection getConnection() throws SQLException {
				try {
					return getJdbcConnection();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				return null;
			}
		});

		List<Person> actual = dao.findAll();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testSavePerson() throws Exception {
		JdbcTemplatePersonDao dao = new JdbcTemplatePersonDao();
		dao.setDataSource(new BasicDataSource() {
			@Override
			public Connection getConnection() throws SQLException {
				try {
					return getJdbcConnection();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				return null;
			}
		});
		List<Person> persons = dao.findAll();
		int count = persons.size();
		Person person = new Person("John", "Doe");
		dao.save(person);
		persons = dao.findAll();
		// 确实添加了一个person
		Assert.assertEquals(count + 1, persons.size());

		IDataSet expectedData = getDataSet("afterSavePerson");
		String[] tables = new String[] { "people" };
		IDataSet actualData = getConnection().createDataSet(tables);
		Assertion.assertEquals(expectedData, actualData);
	}

	private List<Person> createListOfPersonWithLastname(String lastname) {

		List<Person> expected = new ArrayList<Person>();
		expected.add(new Person("Alice", lastname));
		expected.add(new Person("Billy", lastname));
		expected.add(new Person("Clark", lastname));

		return expected;
	}

}
