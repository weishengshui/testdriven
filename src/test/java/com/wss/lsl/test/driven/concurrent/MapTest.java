package com.wss.lsl.test.driven.concurrent;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MapTest {

	@Test
	public void testUnmodifiableMap() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("1", 1);

		// unmodifiableMap 依赖 map的引用
		Map<String, Integer> unmodifiableMap = Collections.unmodifiableMap(map);
		// unmodifiableMap2 只依赖map里面当前的元素
		Map<String, Integer> unmodifiableMap2 = Collections.unmodifiableMap(new HashMap<String, Integer>(map));
		assertEquals(1, unmodifiableMap.size());
		assertEquals(1, unmodifiableMap2.size());

		// unmodifiableMap还是受原来的map引用的影响
		map.put("2", 2);
		assertEquals(2, unmodifiableMap.size());
		assertEquals(1, unmodifiableMap2.size());

		// unmodifiableMap还是受原来的map引用的影响
		map.put("1", 3);
		assertEquals(new Integer(3), unmodifiableMap.get("1"));
		assertEquals(new Integer(1), unmodifiableMap2.get("1"));
	}

}
