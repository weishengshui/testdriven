package com.wss.lsl.test.driven.learning;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.Test;

/**
 * 泛型形式测试
 * 
 * @author sean
 * 
 */
public class GenericTest {

	@Test
	public void testWildcard() {
		// 在两个维度展开：泛型类自身的继承关系、类型参数的继承关系(使用通配符时，可以在类型参数的继承关系上展开)。
		Set<? extends Number> set = null;
		List<? extends Number> list = null;
		// list = set;// error
		// 泛型类的继承关系: Set/List 继承自Collection
		@SuppressWarnings("unused")
		Collection<? extends Number> col = null;
		col = set;
		col = list;

		// 类型参数的继承关系：Double 继承自 Number
		Set<Double> setDouble = null;
		List<Double> listDouble = null;
		set = setDouble;
		list = listDouble;
		col = setDouble;
		col = listDouble;

		// 没有使用通配符，不能在类型参数的继承关系上展开
		@SuppressWarnings("unused")
		List<Object> listObject = null;
		@SuppressWarnings("unused")
		List<String> listString = null;
		// listObject = listString; // error
	}

}

/**
 * 接口、类的类型参数只能指定上界，不能指定下界：即super不能使用。
 * 
 * @author sean
 * 
 * @param <T>
 */
class A<T extends Number> {

	/**
	 * list 中元素的类型未知：由于类型未知，我们不能添加任何类型的元素。
	 * 
	 * @param list
	 */
	public void wildcard(List<?> list) {
		// list.add(1); // error
		// list.add((Object)1); // error
		// list.add("1"); // error
		@SuppressWarnings("unused")
		Object o = list.get(0);
	}

	/**
	 * 下界：list中的元素是Number或Number的父类
	 * 
	 * @param list
	 */
	public void lowerbound(List<? super Number> list) {

	}

	/**
	 * 上界：list中的元素是Number或Number的子类
	 * 
	 * @param list
	 */
	public void upperbound(List<? extends Number> list) {

	}

	public <X> X getSessionAttr(String key) {

		return null;
	}

	public <X> X getSessionAttr(Class<X> classz, String key) {

		return null;
	}

	public Object getSessionAttr() {

		return null;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		A<Integer> a = new A<Integer>();
		String key = "xxx";
		Object o1 = a.getSessionAttr("xxx");
		Double d = a.getSessionAttr(Double.class, "xxx");
		Object o2 = a.getSessionAttr();
	}
}
