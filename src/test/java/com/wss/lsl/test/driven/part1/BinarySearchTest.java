package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BinarySearchTest {
	
	private BinarySearch<String> binarySearch;
	
	@Before
	public void setUp() {
		binarySearch = new BinarySearch<String>();
	}
	
	// 测试非法参数
	@Test(expected=NullPointerException.class)
	public void testIllegalParameter() {
		binarySearch.search(null, null);
	}
	
	// 测试空数组
	@Test
	public void testEmptyArray() {
		String[] emptyArray = new String[]{};
		assertEquals(-1, binarySearch.search(emptyArray, "123"));
		assertEquals(-1, binarySearch.search(emptyArray, ""));
		assertEquals(-1, binarySearch.search(emptyArray, " "));
		assertEquals(-1, binarySearch.search(emptyArray, "1"));
	}
	
	// 数组中只包含一个元素的情况
	@Test
	public void testArrayOnlyContainsOneElement() {
		String[] containsOneElementArray = new String[]{"1"};
		// 找不到的情况
		assertEquals(-1, binarySearch.search(containsOneElementArray, "123"));
		assertEquals(-1, binarySearch.search(containsOneElementArray, ""));
		assertEquals(-1, binarySearch.search(containsOneElementArray, null));
		// 能找到的情况
		assertEquals(0, binarySearch.search(containsOneElementArray, "1"));
	}
	
	// 测试数组中包含多个元素的情况
	@Test
	public void testArrayContainsMultiElement() {
		String[] containsOneElementArray = new String[]{"1", "2", "3", "4"};
		// 找不到的情况
		assertEquals(-1, binarySearch.search(containsOneElementArray, "123"));
		assertEquals(-1, binarySearch.search(containsOneElementArray, "23"));
		assertEquals(-1, binarySearch.search(containsOneElementArray, ""));
		assertEquals(-1, binarySearch.search(containsOneElementArray, null));
		// 能找到的情况
		assertEquals(0, binarySearch.search(containsOneElementArray, "1"));
		assertEquals(1, binarySearch.search(containsOneElementArray, "2"));
		assertEquals(2, binarySearch.search(containsOneElementArray, "3"));
		assertEquals(3, binarySearch.search(containsOneElementArray, "4"));
	}
	

}
