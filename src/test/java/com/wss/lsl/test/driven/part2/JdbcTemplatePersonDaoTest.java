package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.wss.lsl.test.driven.part2.dao.impl.JdbcTemplatePersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;
import com.wss.lsl.test.driven.part2.mapper.PersonRowMapper;

public class JdbcTemplatePersonDaoTest {

	@Test
	public void testFindByLastname() {

		final String lastname = "smith";
		final List<Person> persons = createListOfPersonWithLastname(lastname);

		JdbcTemplate jdbcTemplate = EasyMock.createMock(JdbcTemplate.class);
		jdbcTemplate.query(
				EasyMock.eq("select * from people where last_name = ?"),
				EasyMock.aryEq(new String[] { lastname }),
				EasyMock.isA(PersonRowMapper.class));
		EasyMock.expectLastCall().andReturn(persons);

		EasyMock.replay(jdbcTemplate);

		JdbcTemplatePersonDao dao = new JdbcTemplatePersonDao();
		dao.setJdbcTemplate(jdbcTemplate);
		List<Person> actual = dao.findByLastname(lastname);

		assertEquals(persons, actual);
		EasyMock.verify(jdbcTemplate);
	}

	private List<Person> createListOfPersonWithLastname(String lastname) {

		List<Person> expected = new ArrayList<Person>();
		expected.add(new Person("Alice", lastname));
		expected.add(new Person("Billy", lastname));
		expected.add(new Person("Clark", lastname));

		return expected;
	}
}
