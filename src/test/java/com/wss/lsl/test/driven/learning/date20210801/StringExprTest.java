package com.wss.lsl.test.driven.learning.date20210801;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 字符串表达式
 * 
 * @author weiss
 *
 */
public class StringExprTest {
	
	private List<String> operators = new ArrayList<>(4);
	
	@Before
	public void setUp() {
		operators.add("+");
		operators.add("-");
		operators.add("*");
		operators.add("/");
	}
	
	/**
	 * 补全表达式左括号
	 */
	@Test
	public void test() {
		// 1 + 2 ) * 3 - 4 ) * 5 - 6 ) ) )
		// 实现目标：左括号 左表达式  操作符 右表达式 右括号
		try (Scanner sc = new Scanner(System.in);) {
			String expr = sc.nextLine();
			String[] elements = expr.split("\\s+");
			Stack<String> stack = new Stack<>();
			StringBuilder right = new StringBuilder();
			for (String element : elements) {
				if (!")".equals(element)) {
					stack.push(element);
					continue;
				}
				right.setLength(0);
				right.append(")");
				// 右操作数
				right.insert(0, " ").insert(0, stack.pop());
				// 操作符
				right.insert(0, " ").insert(0, stack.pop());
				// 左操作数
				right.insert(0, " ").insert(0, stack.pop());
				// 左括号
				right.insert(0, " ").insert(0, "(");
				// 将右表达式作为一个整体压入堆栈
				stack.push(right.toString());
			}
			
			System.out.println("加上左括号的表达式：" + stack.pop());
		}
	}
	
	/**
	 * 中序转后序
	 */
	@Test
	public void testInToPost() {
		// ( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )
		String expr = "( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )";
		String[] elements = expr.split("\\s+");
		Stack<String> stack = new Stack<>();
		for (String element : elements) {
			if (!")".equals(element)) {
				stack.push(element);
				continue;
			}
			String right = stack.pop();
			String operator = stack.pop();
			String left = stack.pop();
			stack.pop();
			stack.push(left + " " + right + " " + operator);
		}
		System.out.println("后序表达式：" + stack.pop());
	}
	
	/**
	 * 计算后序表达式的值
	 */
	@Test
	public void testEvaluatePostValue() {
		String expr = "1 2 + 3 4 - 5 6 - * *";
		int result = evaluatePostValue(expr);
		// 期望结果是3
		Assert.assertEquals(3, result);
		
		expr = "1 4 + 3 * 10 5 / +";
		result = evaluatePostValue(expr);
		// 期望结果是17
		Assert.assertEquals(17, result);
		
	}
	
	private int evaluatePostValue(String expr) {
		String[] elements = expr.split("\\s+");
		Stack<String> stack = new Stack<>();
		int result = 0;
		for (String element : elements) {
			if (!operators.contains(element)) {
				stack.push(element);
				continue;
			}
			int right = Integer.valueOf(stack.pop());
			int left = Integer.valueOf(stack.pop());
			switch(element) {
			case "+":
				result = left + right;
				break;
			case "-":
				result = left - right;
				break;
			case "*":
				result = left * right;
				break;
			case "/":
				result = left / right;
				break;
			default:
				throw new IllegalArgumentException("无效的计算表达式");
			}
			stack.push(result + "");
		}
		
		return result;
	}

}
