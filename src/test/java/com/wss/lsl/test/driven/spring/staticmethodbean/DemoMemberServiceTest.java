package com.wss.lsl.test.driven.spring.staticmethodbean;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.wss.lsl.test.driven.BaseTest;

public class DemoMemberServiceTest extends BaseTest {

	@Test
	public void test() {
		DemoMemberService demoMemberService = applicationContext.getBean(DemoMemberService.class);
		Assert.assertNotNull(demoMemberService);

		StaticConfigation staticConfigation = applicationContext.getBean(StaticConfigation.class);
		Assert.assertNotNull(staticConfigation);
		
		String[] activePorfiles = applicationContext.getEnvironment().getActiveProfiles();
		System.out.println(Arrays.toString(activePorfiles));
	}
	
	@Test
	public void test1() {
		System.out.println(10000000D);
	}

}
