package com.wss.lsl.test.driven.arithmetic;

import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 最大子数组算法
 * 
 * @author weiss
 *
 */
public class FindMaxImumSubArray2Test {

	private static final int ARRAY_SIZE = 10000;
	private static final int MAX_NUMBER = 1000;

	private int testCount = 1000; // 测试次数
	private Random random = new Random();

	@Before
	public void before() throws Exception {
		testCount = 1000;
	}

	/**
	 * 测试所有负数的情况
	 * 
	 */
	@Test
	public void testAllNegativeNumber() {
		while (testCount-- > 0) {
			int[] data = new int[ARRAY_SIZE];
			for (int i = 0; i < ARRAY_SIZE; i++) {
				data[i] = -random.nextInt(MAX_NUMBER);
			}

			int max = Arrays.stream(data).max().getAsInt();
			int[] result = FindMaxImumSubArray2.findMaxImumSubArray(data, 0);

			Assert.assertEquals(max, result[2]);
			Assert.assertEquals(max, data[result[0]]);
		}
	}

	/**
	 * 测试所有正数的情况
	 */
	@Test
	public void testAllPositiveNumber() {
		while (testCount-- > 0) {
			int[] data = new int[ARRAY_SIZE];
			for (int i = 0; i < ARRAY_SIZE; i++) {
				data[i] = random.nextInt(MAX_NUMBER) + 1; // 保证所有元素都大于0
			}

			int sum = Arrays.stream(data).sum();
			int[] result = FindMaxImumSubArray2.findMaxImumSubArray(data, 0);

			Assert.assertEquals(0, result[0]);
			Assert.assertEquals(data.length - 1, result[1]);
			Assert.assertEquals(sum, result[2]);
		}
	}

	/**
	 * 测试负数、0、正数混合的情况
	 * 
	 */
	@Test
	public void testNegativeAndPositiveAndZeroMixNumber() {
		while (testCount-- > 0) {
			int[] data = new int[ARRAY_SIZE];
			for (int i = 0; i < ARRAY_SIZE; i++) {
				if (random.nextInt(10) <= 3) {// 30%的概率生成负数
					data[i] = -random.nextInt(MAX_NUMBER);
				} else {// 70%的概率生成正数
					data[i] = random.nextInt(MAX_NUMBER);
				}
			}

			int sum = Arrays.stream(data).sum();
			int[] result = FindMaxImumSubArray2.findMaxImumSubArray(data, 0);
//			System.out.println(Arrays.toString(data));
//			System.out.println(Arrays.toString(result));
			// 如何验证 FIXME
			// 与和比较一下？
			Assert.assertTrue(result[2] >= sum);
		}
	}

}
