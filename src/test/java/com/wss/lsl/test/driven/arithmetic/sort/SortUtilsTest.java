package com.wss.lsl.test.driven.arithmetic.sort;

import java.util.Random;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 排序算法的测试
 * 
 * @author sean
 * 
 */
public class SortUtilsTest {

	private Random random;
	private int arrayCount;
	private int size;
	private int[][] array;

	@Before
	public void before() {
		// 初始化数据
		random = new Random();
		arrayCount = 1;
		size = 100000;
		array = new int[arrayCount][size];
		for (int i = 0; i < arrayCount; i++) {
			for (int j = 0; j < size; j++) {
				array[i][j] = random.nextInt();
			}
		}
	}

	@After
	public void after() {
		for (int i = 0; i < arrayCount; i++) {
			// System.out.println(Arrays.toString(array[i]));
			for (int j = 0; j < size - 1; j++) {
				Assert.assertTrue(array[i][j] + "大于" + array[i][j + 1],
						array[i][j] <= array[i][j + 1]);
			}
		}
	}

	@Test
	public void testQuicklySort() {
		for (int i = 0; i < arrayCount; i++) {
			SortUtils.quicklySort(array[i], 0, array[i].length - 1);
		}
	}

	@Test
	public void testQsort2() {
		for (int i = 0; i < arrayCount; i++) {
			SortUtils.qsort2(array[i], 0, array[i].length - 1);
		}

		// System.out.println("\n\n");
	}

	@Test
	public void testIsort() {
		for (int i = 0; i < arrayCount; i++) {
			SortUtils.isort(array[i]);
		}
	}
}
