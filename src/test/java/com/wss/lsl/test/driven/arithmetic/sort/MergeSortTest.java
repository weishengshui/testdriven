package com.wss.lsl.test.driven.arithmetic.sort;

import java.util.Arrays;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class MergeSortTest {
	
	private Random random;
	private int count;
	private int[] data;
	private int[] sortedData;

	@Before
	public void before() {
		random = new Random();
		count = 1000000;

		data = new int[count];
		sortedData = new int[count];
		for (int i = 0; i < count; i++) {
			data[i] = random.nextInt(100000);
			sortedData[i] = data[i];
		}
		Arrays.sort(sortedData);
	}
	
	@Test
	public void testSort() {
		MergeSort.sort(data, 0, data.length - 1);
//		System.out.println(Arrays.toString(data));
//		Assert.assertArrayEquals(sortedData, data);
	}
	
	@Test
	public void testSort2() {
		data =  InsertionSort.sort(data);
	}

}
