package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class RegexLearningTest {

	@Test
	public void testHowGroupCountWorks() {
		String haystack = "x99SuperJava The needle shop sells needles x99SuperJava";
		String regex = "(needle)";
		Matcher matcher = Pattern.compile(regex).matcher(haystack);
		assertEquals(1, matcher.groupCount());
		assertTrue(matcher.find());
		assertEquals("needle", matcher.group(0));
		assertEquals("needle", matcher.group(1));
		assertTrue(matcher.find());
		assertEquals("needle", matcher.group(0));
		assertEquals("needle", matcher.group(1));
		assertEquals(1, matcher.groupCount());
		
		
		regex = "\\w(\\d\\d)(\\w+)";
		matcher = Pattern.compile(regex).matcher(haystack);
		assertEquals(2, matcher.groupCount());
		assertTrue(matcher.find());
		assertEquals("x99SuperJava", matcher.group(0));
		assertEquals("99", matcher.group(1));
		assertEquals("SuperJava", matcher.group(2));
	}
	
	@Test
	public void testFindStartAndEnd() {
		String haystack = "The needle shop sells needles.";
		String regex = "(needle)";
		Matcher matcher = Pattern.compile(regex).matcher(haystack);
		assertEquals(1, matcher.groupCount());
		assertTrue(matcher.find());
		assertEquals(4, matcher.start());
		assertEquals(10, matcher.end());

		assertTrue(matcher.find());
		assertEquals(22, matcher.start());
		assertEquals(28, matcher.end());

	}

}
