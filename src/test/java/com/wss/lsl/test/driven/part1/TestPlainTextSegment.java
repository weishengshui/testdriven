package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * 测试纯文本片段
 * 
 * @author Administrator
 *
 */
public class TestPlainTextSegment {

	@Test
	public void plainTextEvaluateAsIs() {
		Map<String, String> variables = new HashMap<String, String>();
		String text = "abc def";
		assertEquals(text, new PlainText(text).evaluate(variables));
	}

}
