package com.wss.lsl.test.driven;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ProducerConsumerTest {

	static class Producer implements Runnable {

		private List<Integer> queue;

		public Producer(List<Integer> queue) {
			this.queue = queue;
		}

		@Override
		public void run() {
			try {
				int count = 0;
				while (true) {
					synchronized (queue) {
						while (queue.size() == 10) {
							queue.wait();
						}

						count++;
						System.out.println("生产：" + count);
						queue.add(count);
						queue.notify();
					}
					Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	static class Consumer implements Runnable {
		private List<Integer> queue;

		public Consumer(List<Integer> queue) {
			super();
			this.queue = queue;
		}

		@Override
		public void run() {

			try {
				while (true) {
					synchronized (queue) {
						while (queue.isEmpty()) {
							queue.wait();
						}

						System.out.println("消费：" + queue.get(0));
						queue.remove(0);
						queue.notify();
					}
					Thread.sleep(5000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	@Test
	public void test() throws InterruptedException {
		List<Integer> queue = new ArrayList<>(10);

		Thread t1 = new Thread(new Producer(queue));
		Thread t2 = new Thread(new Consumer(queue));

		t1.start();
		t2.start();

		t1.join();
		t2.join();
	}

}
