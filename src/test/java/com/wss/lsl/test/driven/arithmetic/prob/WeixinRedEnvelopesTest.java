package com.wss.lsl.test.driven.arithmetic.prob;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class WeixinRedEnvelopesTest {

	@Test
	public void testGeneralPlay() {
		BigDecimal money = new BigDecimal("1.00");
		int numberOfPeople = 3;
		BigDecimal[] result = WeixinRedEnvelopes.generalPlay(money,
				numberOfPeople);
		for (BigDecimal item : result) {
			System.out.println(item);
		}

		BigDecimal actualMoney = new BigDecimal("0.00");
		for (BigDecimal item : result) {
			if (item.doubleValue() < 0.01) {
				assertFalse(true);
			}
			actualMoney = actualMoney.add(item);
		}
		assertTrue(money.equals(actualMoney));
	}

}
