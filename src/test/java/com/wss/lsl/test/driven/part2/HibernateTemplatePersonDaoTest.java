package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.dao.impl.HibernateTemplatePersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;

public class HibernateTemplatePersonDaoTest {

	private SessionFactory sessionFactory;
	private Session session;
	private Query query;

	@Before
	public void setUp() {
		sessionFactory = EasyMock.createMock(SessionFactory.class);
		session = EasyMock.createMock(Session.class);
		query = EasyMock.createMock(Query.class);
	}

	@Test
	public void testFindByLastname() {
		String hql = "from Person where lastname = :lastname";
		String lastname = "smith";
		List<Person> expected = createListOfPersonWithLastname(lastname);

		// 记录期望的API调用
		EasyMock.expect(sessionFactory.openSession()).andReturn(session);
		EasyMock.expect(session.createQuery(hql)).andReturn(query);
		EasyMock.expect(query.setString("lastname", lastname)).andReturn(query);
		EasyMock.expect(query.list()).andReturn(expected);

		EasyMock.replay(sessionFactory, session, query);

		HibernateTemplatePersonDao dao = new HibernateTemplatePersonDao();
		// 手动注入sessionFactory
		dao.setSessionFactory(sessionFactory);

		// 期望的查询方法返回硬编码列表
		List<Person> actual = dao.findByLastname(lastname);
		Assert.assertEquals(expected, actual);

		EasyMock.verify(sessionFactory, session, query);
	}

	@Test
	public void testFindByLastnameReturnsEmptyListUponException() {
		String hql = "from Person where lastname = :lastname";
		String lastname = "smith";
		HibernateException exception = new HibernateException("");
		RuntimeException error = new RuntimeException(exception);

		EasyMock.expect(sessionFactory.openSession()).andReturn(session);
		EasyMock.expect(session.createQuery(hql)).andReturn(query);
		EasyMock.expect(query.setString("lastname", lastname)).andReturn(query);
		EasyMock.expect(query.list()).andThrow(error);

		EasyMock.replay(sessionFactory, session, query);

		HibernateTemplatePersonDao dao = new HibernateTemplatePersonDao();
		dao.setSessionFactory(sessionFactory);
		try {
			dao.findByLastname(lastname);
			fail("should've throw an exception");
		} catch (Exception e) {
			Assert.assertSame(error, e);
		}

		EasyMock.verify(sessionFactory, session, query);

	}

	private List<Person> createListOfPersonWithLastname(String lastname) {

		List<Person> expected = new ArrayList<Person>();
		expected.add(new Person("Alice", lastname));
		expected.add(new Person("Billy", lastname));
		expected.add(new Person("Clark", lastname));

		return expected;
	}

}
