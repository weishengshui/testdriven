package com.wss.lsl.test.driven.redis;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

/**
 * twemproxy测试
 * 
 * @author Administrator
 *
 */
public class TwemproxyTest {

	private JedisPool jedisPool;
	private Jedis jedis;

	@Before
	public void setup() throws Exception {
		String host = "192.168.1.150";
		int port = 22121;

		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(500);

		jedisPool = new JedisPool(poolConfig, host, port);
		jedis = jedisPool.getResource();
	}

	@After
	public void teardown() {
		jedis.close();
		jedisPool.close();
	}
	
	/**
	 * 测试set命令
	 */
	@Test
	public void testSet() {
		int count = 10000;
		while (count-- > 0) {
			jedis.set("wss" + count, count + "");
		}
		
		// test hash tag
		count = 10000;
		while (count-- > 0) {
			jedis.set("{_wss}" + count, count + "");
		}
	}
	
	/**
	 * 测试管道
	 */
	@Test
	public void testPipeline(){
		Pipeline p = jedis.pipelined();
		// p.multi();
		int count = 10000;
		while (count-- > 0) {
			p.set("22wss" + count, count + "");
		}
		// p.exec(); // 不支持事务
		p.sync(); // 管道是支持的，分片也正常有效
	}
	
	@Test
	public void testPipeline2(){
		// Pipeline p = jedis.pipelined();
		// p.multi();
		// int count = 10000;
		// while (count-- > 0) {
		// p.set("33wss" + count, count + "");
		// }
		// p.exec(); // 不支持事务
	}

}
