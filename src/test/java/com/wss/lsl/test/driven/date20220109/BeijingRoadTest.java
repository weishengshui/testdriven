package com.wss.lsl.test.driven.date20220109;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 北京道路是横平竖直的，向东、向北各走N个街区，有多少种走法。
 * <pre>
 * 其实这是一个卡特兰数，但是本人无法证明
 * </pre>
 * 
 * @author weiss
 *
 */
public class BeijingRoadTest {
	
	@Test
	public void test1() {
		
		int N = 1;
		int count = road(N, N);
		assertEquals(2, count);
	}
	
	@Test
	public void test2() {
		
		int N = 2;
		int count = road(N, N);
		assertEquals(6, count);
	}
	
	@Test
	public void test3() {
		
		int N = 3;
		int count = road(N, N);
		assertEquals(6, count);
	}
	
	private int road(int i , int j) {
		if (i == 0 || j == 0) {
			return 1;
		}
		
		return road(i, j - 1) + road(i - 1, j);
	}

}
