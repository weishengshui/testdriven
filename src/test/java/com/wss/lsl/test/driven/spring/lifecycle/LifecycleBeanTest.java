package com.wss.lsl.test.driven.spring.lifecycle;

import org.junit.AfterClass;
import org.junit.Test;

import com.wss.lsl.test.driven.BaseTest;

public class LifecycleBeanTest extends BaseTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testPrintName() {
		LifecycleBean bean = applicationContext.getBean(LifecycleBean.class);
		bean.printName();
	}

}
