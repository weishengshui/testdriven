package com.wss.lsl.test.driven.effective.enummap37;

import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

public enum Phase {
	SOLID, LIQUID, GAS;

	public enum Transition {
		MELT(SOLID, LIQUID), FREEZE(LIQUID, SOLID), BOIL(LIQUID, GAS), CONDENSE(GAS, LIQUID), SUBLIME(SOLID, GAS),
		DEPOSIT(GAS, SOLID);

		private final Phase from;
		private final Phase to;

		Transition(Phase from, Phase to) {
			this.from = from;
			this.to = to;
		}

		// Initialize the phase transition map
		private static final Map<Phase, Map<Phase, Transition>> m = Stream.of(values()).collect(Collectors.groupingBy(
				t -> t.from, () -> new EnumMap<>(Phase.class),
				Collectors.toMap(t -> t.to, t -> t, (x, y) -> y, () -> new EnumMap<Phase, Transition>(Phase.class))));

		public static Transition from(Phase from, Phase to) {
			return m.get(from).get(to);
		}
	}

	public static void main(String[] args) {

		Assert.assertEquals(Transition.MELT, Transition.from(SOLID, LIQUID));
		Assert.assertEquals(Transition.DEPOSIT, Transition.from(GAS, SOLID));

		System.out.println("Done");
	}

	@Test
	public void test(Collection<? extends Comparable<?>> opSet) {

	}

	@Test
	public void test2(Collection<? extends Enum<?>> opSet) {

	}

	// 为什么这种方法签名是无效的
//	@Test
//	public void test3(Collection<? extends Enum<?> & Comparable<?>> opSet) {
//		
//	}

	@Test
	public <T extends Enum<T> & Comparable<T>> void test(Class<T> c) {
		for (Comparable<T> cp : c.getEnumConstants()) {
			System.out.println(cp);
		}
	}

	public List<String>[] test4() {

//		return new ArrayList<String>[0]; // 报编译错误
		return null;
	}
}
