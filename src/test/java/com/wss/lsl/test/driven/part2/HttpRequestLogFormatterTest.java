package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import java.text.DateFormat;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.common.log.HttpRequestLogFormatter;
import com.wss.lsl.test.driven.part2.common.time.SystemTime;
import com.wss.lsl.test.driven.part2.common.time.TimeSource;

public class HttpRequestLogFormatterTest {

	@After
	public void tearDown() {
		SystemTime.reset();
	}

	@Test
	public void testCommonLogFormatter() {
		final long time = SystemTime.asMillis();
		SystemTime.setTimeSource(new TimeSource() {

			@Override
			public long millis() {
				return time;
			}
		});

		DateFormat dateFormat = HttpRequestLogFormatter.DATE_FORMAT;
		String timestamp = dateFormat.format(SystemTime.asDate());
		String expected = "1.2.3.4 bob [" + timestamp
				+ "] \"GET /ctx/resource HTTP/1.1\" 200 2326";
		HttpServletRequest request = EasyMock
				.createMock(HttpServletRequest.class);
		EasyMock.expect(request.getRemoteAddr()).andReturn("1.2.3.4");
		EasyMock.expect(request.getRemoteUser()).andReturn("bob");
		EasyMock.expect(request.getMethod()).andReturn("GET");
		EasyMock.expect(request.getRequestURI()).andReturn("/ctx/resource");
		EasyMock.expect(request.getProtocol()).andReturn("HTTP/1.1");
		EasyMock.replay(request);

		HttpRequestLogFormatter logFormatter = new HttpRequestLogFormatter();
		assertEquals(expected, logFormatter.format(request, 200, 2326));

		EasyMock.verify(request);
	}

	/**
	 * 测试时间戳的格式
	 */
	@Test
	public void testTimestampFormat() {

		String regex = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
		String time = HttpRequestLogFormatter.DATE_FORMAT.format(SystemTime
				.asDate());
		assertTrue("DateFormat should be yyyy-MM-dd HH:mm:ss",
				time.matches(regex));
	}
}
