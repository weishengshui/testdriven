package com.wss.lsl.test.driven.arithmetic.sort;

import java.util.Random;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * 排序测试基类
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public class SortBaseTest {

	protected Integer[][] data;
	protected int arrayCount;
	private Random random = new Random();
	protected Sort<?>[] sort;

	@Before
	public void before() {
		arrayCount = 10000;
		data = new Integer[arrayCount][];
		int size;
		for (int i = 0; i < arrayCount; i++) {
			size = 10000 + random.nextInt(100);
			data[i] = new Integer[size];
			for (int j = 0; j < size; j++) {
				data[i][j] = random.nextInt();
			}
		}

		sort = new Sort<?>[arrayCount];
	}

	@Test
	@Ignore
	public void _1() {
	}

}
