package com.wss.lsl.test.driven.concurrent;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

public class CompletableFutureTest {

	@Test
	public void test() throws InterruptedException, ExecutionException {
		CompletableFuture<Integer> completableFutureToBeCompleted = CompletableFuture.supplyAsync(() -> {

			for (int i = 0; i < 10; i--) {
				System.out.println(" i " + i);
			}
			return 10;
		});

		CompletableFuture<Integer> completor = CompletableFuture.supplyAsync(() -> {
			System.out.println("other completor");

			completableFutureToBeCompleted.complete(222);

			return 10;
		});

		System.out.println("completor-->" + completor.get());
		System.out.println("completableFutureToBeCompleted-->" + completableFutureToBeCompleted.get());
	}

	@Test
	public void testThenComposeThenApply() {
		@SuppressWarnings("unused")
		CompletableFuture<Integer> bigComplete = CompletableFuture.supplyAsync(() -> {
			return 10;
		});

//		CompletableFuture thenCompose = bigComplete.thenCompose(Combinin)

	}
}
