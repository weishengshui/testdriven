package com.wss.lsl.test.driven;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

public class AppTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test1() {
		Integer[] a = { 1, 2, 3, 4 };
		Integer[] c = a;
		Integer[] b = Arrays.asList(3, 4, 5).toArray(a);
		assertTrue(b[0] == 3);
		System.out.println(Arrays.toString(a));
		System.out.println(Arrays.toString(c));
		assertTrue(a[0] == 3); // a已经被改变了
	}

	@Test
	public void test2() {
		// 打印嵌套类：public才打印
		for (Class<?> c : AppTest.class.getClasses()) {
			System.out.println(c.getSimpleName());
		}

		B[] a = new B[10];
		Arrays.sort(a, new BComparator());
	}

	@Test
	public void test3() {
		Set<String> s = new LinkedHashSet<String>();
		s.add("1");
		s.add("2");
		s.add("3");
		s.add("1");
		System.out.println(s);
	}

	public static class A {
	}

	public static class B extends A {
	}

	static class BComparator implements Comparator<A> {

		@Override
		public int compare(A o1, A o2) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

	@Test
	public void test4() {
		System.out.println(System.currentTimeMillis() / 1000);
	}

	@Test
	public void test5() {
		BigDecimal a = new BigDecimal("1");
		a = a.subtract(new BigDecimal(1).divide(new BigDecimal("3"), 10, BigDecimal.ROUND_HALF_UP))
				.add(new BigDecimal(1).divide(new BigDecimal("5"), 10, BigDecimal.ROUND_HALF_UP))
				.multiply(new BigDecimal("4"));
		System.out.println(a.doubleValue());
		System.out.println(a.subtract(new BigDecimal(Math.PI)).doubleValue());
	}
	
	@Test
	public void test6() {
		BigDecimal a = new BigDecimal("1");
		a = a.subtract(new BigDecimal(1).divide(new BigDecimal("3"), 10, BigDecimal.ROUND_HALF_UP))
//				.add(new BigDecimal(1).divide(new BigDecimal("5"), 10, BigDecimal.ROUND_HALF_UP))
				.multiply(new BigDecimal("4"))
				;
		System.out.println(a.doubleValue());
		System.out.println(a.subtract(new BigDecimal(Math.PI)).doubleValue());
	}


	/**
	 * 测试合并排序
	 */
	@Test
	public void testMergeSort() {
		int count = 1000;
		int size = 100000;
//		int size = 100;
		Random random = new Random();
		for (int i = 0; i < count; i++) {
			int[] data = new int[size];
			int[] expectedData = new int[size];
			for (int j = 0; j < size; j++) {
				data[j] = random.nextInt();
				expectedData[j] = data[j];
			}

			Arrays.sort(expectedData);
			mergeSort(data, 0, data.length - 1);

			Assert.assertArrayEquals(expectedData, data);
		}
	}

	public static void mergeSort(int[] data, int p, int r) {
		if (p >= r) {
			return;
		}

		// 元素较少时，插入排序
		if (r - p + 1 <= 128) {
			for (int i = p + 1; i <= r; i++) {
				int key = data[i];
				int j = i - 1;
				while (j >= p && data[j] > key) {
					data[j + 1] = data[j];
					j--;
				}

				data[j + 1] = key;
			}

			return;
		}

		int q = (p + r) / 2;
		mergeSort(data, p, q);
		mergeSort(data, q + 1, r);
		merge(data, p, q, r);
	}

	private static void merge(int[] data, int p, int q, int r) {

		int l1 = q - p + 1;
		int l2 = r - q;
		int[] L = new int[l1];
		int[] R = new int[l2];

		for (int i = 0; i < l1; i++) {
			L[i] = data[p + i];
		}
		for (int j = 0; j < l2; j++) {
			R[j] = data[q + 1 + j];
		}

		int i = 0;
		int j = 0;
		int k;
		while (i < l1 || j < l2) {
			k = p + i + j;
			if (((i < l1 && j < l2) && L[i] < R[j]) || j == l2) {
				data[k] = L[i];
				i++;
			} else {
				data[k] = R[j];
				j++;
			}
		}
	}

	@Test
	public void testGetYesterdayByFormat() {
		String timeFormat = "yyyy-MM-dd";
		System.out.println(getYesterdayByFormat(timeFormat));
	}

	public static String getYesterdayByFormat(String timeFormat) {
		return LocalDateTime.now().minusDays(1).format(DateTimeFormatter.ofPattern(timeFormat));
	}
}
