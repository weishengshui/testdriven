package com.wss.lsl.test.driven.concurrent;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class LoggerServiceTest {

	private LoggerService loggerService;
	private int loggerNumber = 100;

	@Before
	public void setUp() throws IOException {
		loggerService = new LoggerService();
		loggerService.start();
	}

	@Test
	public void testLog() throws InterruptedException {
		
		loggerService.log("enter method!");
		for (int i = 0; i < loggerNumber; i++) {
			loggerService.log("within method " + i + "!");
		}
		loggerService.log("exit method!");
	}
}
