package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.wss.lsl.test.driven.common.Constants;
import com.wss.lsl.test.driven.part2.service.AuthenticationService;
import com.wss.lsl.test.driven.part2.service.FakeAuthenticationService;

public class LoginServletTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private LoginServlet servlet;
	private AuthenticationService authenticationService;

	@Before
	@SuppressWarnings("serial")
	public void setUp() {

		authenticationService = new FakeAuthenticationService();
		authenticationService.addUser(Constants.VALID_USERNAME,
				Constants.CORRECT_PASSWORD);

		servlet = new LoginServlet() {
			@Override
			protected AuthenticationService getAuthenticationService() {
				return authenticationService;
			}
		};

		request = new MockHttpServletRequest("GET", "/login");
		response = new MockHttpServletResponse();
	}

	/**
	 * 测试登录失败的情形: 用户名、密码错误
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void wrongPasswordShouldRedirectToErrorPage()
			throws ServletException, IOException {
		request.addParameter(Constants.USERNAME_PARAMETER,
				Constants.VALID_USERNAME);
		request.addParameter(Constants.PASSWORD_PARAMETER, "wrongPassword");
		servlet.service(request, response);
		assertEquals("/invalidlogin", response.getRedirectedUrl());
	}

	/**
	 * 登录成功跳到首页并保存用户名到会话中
	 * 
	 * @throws IOException
	 * @throws ServletException
	 */
	@Test
	public void vaildLoginForwardsToFrontPageAndStoresUsername()
			throws ServletException, IOException {
		request.addParameter(Constants.USERNAME_PARAMETER,
				Constants.VALID_USERNAME);
		request.addParameter(Constants.PASSWORD_PARAMETER,
				Constants.CORRECT_PASSWORD);
		servlet.service(request, response);

		assertEquals("/frontpage", response.getRedirectedUrl());
		assertEquals(Constants.VALID_USERNAME, request.getSession()
				.getAttribute("username"));
	}

}
