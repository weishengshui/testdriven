package com.wss.lsl.test.driven.learning;

import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class RegexTest {

	@Test
	public void test() {
		String a = "/*     */";
		String b = "/*  123   */";
		String c = "/";
		System.out.println("\\/");
		Pattern pattern = Pattern.compile("/\\*[\\s\\d]+\\*/");
		assertTrue(Pattern.compile("/").matcher(c).matches());
		assertTrue(pattern.matcher(a).matches());
		assertTrue(pattern.matcher(b).matches());
	}

	@Test
	public void test2() {
		String content = "int(12225)".split("\\(")[1].split("\\)")[0];
		System.out.println(content);
	}

	@Test
	public void test3() {
		String str = "xfooxxxxxxfoo";

		// 贪婪量词：吃掉整个，从右边回退字符匹配
		Pattern p1 = Pattern.compile(".*foo");// 贪婪量词
		System.out.println("\n" + p1);
		findStr(p1, str);

		// 勉强量词：从0个字符匹配开始，从左边一个个吞掉字符匹配
		Pattern p2 = Pattern.compile(".*?foo"); // 勉强量词
		System.out.println("\n" + p2);
		findStr(p2, str);

		// 侵占量词，吃掉整个，不会回退
		Pattern p3 = Pattern.compile(".*+foo"); // 侵占量词
		System.out.println("\n" + p3);
		findStr(p3, str);

		Pattern p4 = Pattern.compile("x*+foo"); // 侵占量词
		System.out.println("\n" + p4);
		findStr(p4, str);
	}

	private void findStr(Pattern p, String str) {
		Matcher m = p.matcher(str);
		int start, end;
		while (m.find()) {
			start = m.start();
			end = m.end();
			System.out.println(str.substring(start, end));
		}
	}

}
