package com.wss.lsl.test.driven.other;

import java.util.Arrays;
import java.util.Random;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class Other20210508Test {

	int arrayLength = 100;
	static final int N = 10000;
	Random random;
	
	@Before
	public void setUp() {
		random = new Random();
	}
	
	private int[] init(int arrayLength) {
		int[] a = new int[arrayLength];
		
		resetArray(a);
		
		return a;
	}
	
	/**复位矩阵**/
	private void resetArray(int[] array) {
		for(int i = 0; i < array.length; i++) {
			array[i] = i;
		}
	}
	
	private int[] shuffle(int[] array) {
		for(int i = 0; i < array.length; i++) {
			int r = i + random.nextInt(array.length - i);
			int temp = array[i];
			array[i] = array[r];
			array[r] = temp;
		}
		
		return array;
	}
	
	@Test
	@Ignore
	public void test() {
		int[] array = init(arrayLength);
		array = shuffle(array);
		System.out.println(Arrays.toString(array));
	}
	
	@Test
	public void shuffleTest() {
		int[][] m = new int[arrayLength][arrayLength];
		
		int[] array = init(arrayLength);
		
		// 初始化矩阵
		for(int i = 0; i < arrayLength; i++) {
			// 初始化矩阵的第i行
			for (int shuffleCount = 0; shuffleCount < N; shuffleCount++) {
				resetArray(array);
				array = shuffle(array);
				for(int j = 0; j < array.length; j++) {
					if (array[j] == i) {
						m[i][j] += 1;
					}
				}
			}
		}
		
		int[] colSum = new int[arrayLength];
		for(int j = 0; j < arrayLength; j++) {
			for(int i = 0; i < arrayLength; i++) {
				colSum[j] += m[i][j];
			}
		}
		
		System.out.println(Arrays.toString(colSum));
		System.out.println("======================");
		int count = 0;
		double mid = ((double)N)/arrayLength;
		double begin = mid - (mid * 0.2);
		double end = mid + (mid * 0.2);
		for(int i = 0; i < arrayLength; i++) {
			for(int j = 0; j < m[i].length; j++) {
				int t = m[i][j];
				if (t>= begin && t <= end) {
					count++;
				}
			}
		}
		
		// 结论：接近理论值的比例很高，0.95的样子
		// 打乱的次数越多，月接近理论值
		// M = 10, N = 100，60%的数字接近
		// M = 10, N = 1000，95%的数字接近
		// M = 10, N = 10000，差不多100%的数字接近
		System.out.println("接近理论值的数量 " + count);
		System.out.println("接近理论值的比例 " + ((double)count)/(arrayLength * arrayLength));
	}
	
	

}
