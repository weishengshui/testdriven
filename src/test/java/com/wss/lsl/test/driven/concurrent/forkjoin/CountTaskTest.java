package com.wss.lsl.test.driven.concurrent.forkjoin;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

import org.junit.Test;

public class CountTaskTest {

	@Test
	public void test() {
		ForkJoinPool forkJoinPool = new ForkJoinPool();

		CountTask countTask = new CountTask(1, 100);
		ForkJoinTask<Integer> forkJoinTask = forkJoinPool.submit(countTask);

		try {
			System.out.println(forkJoinTask.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

	}

}
