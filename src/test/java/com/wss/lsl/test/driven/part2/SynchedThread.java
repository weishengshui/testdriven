package com.wss.lsl.test.driven.part2;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class SynchedThread extends Thread {

	private CyclicBarrier entryBarrier;
	private CyclicBarrier exitBarrier;

	public SynchedThread(Runnable runnable, CyclicBarrier entryBarrier,
			CyclicBarrier exitBarrier) {
		super(runnable);
		this.entryBarrier = entryBarrier;
		this.exitBarrier = exitBarrier;
	}

	@Override
	public void run() {
		try {
			entryBarrier.await();
			super.run();
			exitBarrier.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

}
