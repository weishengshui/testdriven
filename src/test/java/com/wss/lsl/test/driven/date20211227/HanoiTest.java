package com.wss.lsl.test.driven.date20211227;


import java.util.Stack;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

class HanoiTest {
	
	Hanoi hanoi;
	Stack<Integer> pillarA = new Stack<>();
	Stack<Integer> pillarB = new Stack<>();
	Stack<Integer> pillarT = new Stack<>();
	
	int pillarCount;
	
	@Before
	void setUp() throws Exception {
		pillarA = new Stack<>();
		pillarB = new Stack<>();
		pillarT = new Stack<>();
		
		pillarCount = 30;
		for (int i = pillarCount; i > 0; i--) {
			pillarA.push(i);
		}
		
		hanoi = new Hanoi(pillarA, pillarB, pillarT);
	}

	@Test
	void test() {
		hanoi.movePillarA2B();
		
		// 校验
		assertTrue(pillarA.isEmpty());
		assertTrue(pillarT.isEmpty());
		
		for (int i= 1; i < pillarCount; i++) {
			// 当前最小的元素
			int minCurrent = pillarB.pop();
			assertTrue(i == minCurrent);
		}
	}

}
