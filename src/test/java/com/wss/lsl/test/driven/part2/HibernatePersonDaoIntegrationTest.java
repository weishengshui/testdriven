package com.wss.lsl.test.driven.part2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.dao.impl.HibernateTemplatePersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;

/**
 * hibernate 集成测试
 * 
 * @author Administrator
 * 
 */
public class HibernatePersonDaoIntegrationTest {

	private SessionFactory sf;
	private Session session;
	private Transaction tx;
	private HibernateTemplatePersonDao dao;
	private String lastname = "smith";
	private List<Person> expected;

	// 使用事务夹具，提高执行速度
	@Before
	public void setUp() throws HibernateException, IOException {

		expected = createListOfPersonWithLastname(lastname);

		sf = getSessionFactory();
		dao = new HibernateTemplatePersonDao();
		dao.setSessionFactory(sf);
		session = sf.openSession();
		tx = session.beginTransaction();
		tx.begin();
		persist(expected);
	}

	@After
	public void tearDown() {
		tx.rollback();
	}

	@Test
	public void persistedObjectExistsInDatasource() throws HibernateException,
			IOException {
		dao.setSessionFactory(sf);

		Person person = new Person("John", "Doe");
		dao.save(person);

		Assert.assertNotNull(person.getId());
		Person copy = (Person) session.get(Person.class, person.getId());
		Assert.assertEquals(person, copy);
	}

	@Test
	public void testFindByLastname() {
		// 期望的查询方法返回硬编码列表
		List<Person> actual = dao.findByLastname(lastname);
		Assert.assertEquals(expected, actual);
	}

	private void persist(List<? extends Object> objects) {
		for (Object object : objects) {
			session.save(object);
		}
		// 马上执行
		session.flush();
	}

	private List<Person> createListOfPersonWithLastname(String lastname) {

		List<Person> expected = new ArrayList<Person>();
		expected.add(new Person("Alice", lastname));
		expected.add(new Person("Billy", lastname));
		expected.add(new Person("Clark", lastname));

		return expected;
	}

	private SessionFactory getSessionFactory() throws HibernateException,
			IOException {
		return createConfiguration().buildSessionFactory();
	}

	private Configuration createConfiguration() throws IOException {
		Configuration cfg = loadProductionConfiguration();
		loadTestConfigInto(cfg, "/hibernate.test.properties");
		return cfg;
	}

	private void loadTestConfigInto(Configuration cfg, String path)
			throws IOException {

		Properties properties = loadPropertiesFrom(path);
		Enumeration<Object> keys = properties.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = properties.getProperty(key);
			cfg.setProperty(key, value);
		}
	}

	private Properties loadPropertiesFrom(String path) throws IOException {
		InputStream is = getClass().getResourceAsStream(path);
		Assert.assertNotNull("Resource not found: " + path, is);
		Properties properties = new Properties();
		properties.load(is);
		is.close();
		return properties;
	}

	private Configuration loadProductionConfiguration() {
		return new AnnotationConfiguration().addPackage(
				"com.wss.lsl.test.driven.part2.entity").addAnnotatedClass(
				Person.class);
	}

}
