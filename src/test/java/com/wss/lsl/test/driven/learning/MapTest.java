package com.wss.lsl.test.driven.learning;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class MapTest {

	/**
	 * 测试类型安全性
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testTypeSafe() {
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", "weishengshui");
		params.put("password", "123456");
		String sql = "select * from crm_user where user_name=:username and password=:password";
		sqlMap.put("sql", sql);
		sqlMap.put("params", params);

		Map<String, Object> params2 = (Map<String, Object>) sqlMap
				.get("params");
		System.out.println(params2);
		assertTrue(params == params2);

	}

	@Test
	public void test2() {
		A a = new A(new Date());
		String json = JSON.toJSONString(a);

		@SuppressWarnings("unchecked")
		Map<String, String> map = JSON.parseObject(json, Map.class);
		Object s = map.get("date").toString();
		System.out.println("2" + s);
		String date = (map.get("date")).toString();
		System.out.println(date);
	}

	static class A {
		private Date date;

		public A(Date date) {
			super();
			this.date = date;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

	}

	@Test
	public void test3() {
		Map<String, String> dataMap = new ConcurrentHashMap<String, String>();
		for (int i = 0; i < 100; i++) {
			dataMap.put(i + "", i + "");
		}

		for (String key : dataMap.keySet()) {
			dataMap.remove(key);
		}
	}
	
	@Test
	public void test4(){
		Map<String, String> map = new TreeMap<>();
		map.put("5", "1");
		map.put("4", "1");
		map.put("3", "1");
		map.put("2", "1");
		map.put("1", "1");
		System.out.println(map);
	}

}
