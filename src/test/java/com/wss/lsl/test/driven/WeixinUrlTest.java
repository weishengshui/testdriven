package com.wss.lsl.test.driven;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.wss.lsl.test.driven.common.SignUtil;

/**
 * 生成微信url
 * 
 * @author sean
 *
 */
public class WeixinUrlTest {
	
	// 补丁授权url
	private static final String patch_url="http://patch.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
	
	// 开发授权url
	private static final String dev_url="http://dev.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 测试授权url
	private static final String test_url="http://test.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=1&originalId=%2$s&type=1&sign=%3$s";
	
	// 用户测试授权url
	private static final String usertest_url="http://usertest.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
	
	// 预测试授权url
	private static final String pre_url="http://pre.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
	
	// 生产授权url
	private static final String wx_url="http://wx.qxclub.cn/wechat/wx/authorize.do?"
			+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
	
	// 培训环境授权url
	private static final String demo_url="http://demo.qxclub.cn/wechat/wx/authorize.do?"
				+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
	
	/**
	 * 测试补丁
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testPatch() throws UnsupportedEncodingException {
		String original_id = "gh_7c80f28e26a6";
		String brandCode = "TM";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		StringBuilder returnUrl = new StringBuilder("http://patch.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(patch_url, redirectUrl, original_id, authSign);
		
		System.out.println("补丁环境地址: \n" + finalUrl);
	}
	
	@Test
	public void testPatch2() throws UnsupportedEncodingException {
		String original_id = "gh_651291fbe064";
		
		String pre_url="http://patch.qxclub.cn/wechat/wx/authorize.do?"
				+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
		String oauthTypeBase = "0";
		String type = "0";
		
		String returnUrl = "http://patch.qxclub.cn/wcap/coupon_controllers/claim_coupon_new_index.do?memberRuleId=2c91524a53eabf7c0153eea9551a000d&couponRuleCode=DTD16000074&color=0";
		// String returnUrl = "http://patch.qxclub.cn/wcap/coupon_controllers/claim_coupon_new_index.do?memberRuleId=2c91524a53eabf7c0153eac231590004&couponRuleCode=DTD16000072&color=0";
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign3(redirectUrl, original_id, oauthTypeBase, type);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(pre_url, redirectUrl, original_id, authSign);
		
		System.out.println("补丁试环境地址2: \n" + finalUrl);
	}
	
	@Test
	public void testWx2() throws UnsupportedEncodingException {
		String original_id = "gh_acbd41532bc0";
		
		String pre_url="http://wx.qxclub.cn/wechat/wx/authorize.do?"
				+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
		String oauthTypeBase = "0";
		String type = "0";
		
		String returnUrl = "http://wx.qxclub.cn/wcap/coupon_controllers/claim_coupon_new_index.do?memberRuleId=8a7088b153851a600153f47b6cef116f&couponRuleCode=KTD16001990&color=0";
		// String returnUrl = "http://patch.qxclub.cn/wcap/coupon_controllers/claim_coupon_new_index.do?memberRuleId=2c91524a53eabf7c0153eac231590004&couponRuleCode=DTD16000072&color=0";
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign3(redirectUrl, original_id, oauthTypeBase, type);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(pre_url, redirectUrl, original_id, authSign);
		
		System.out.println("生产环境地址2: \n" + finalUrl);
	}
	
	private String getAuthSign(String redirectUrl, String originalId){
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("redirectUrl", redirectUrl);
		treeMap.put("oauthTypeBase", "0");
		treeMap.put("originalId", originalId);
		treeMap.put("type", "0");
		
		String sign = SignUtil.createSign(treeMap, originalId);
		return sign;
	}
	
	private String getAuthSign3(String redirectUrl, String originalId, String oauthTypeBase, String type){
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("redirectUrl", redirectUrl);
		treeMap.put("oauthTypeBase", oauthTypeBase);
		treeMap.put("originalId", originalId);
		treeMap.put("type", type);
		
		String sign = SignUtil.createSign(treeMap, originalId);
		return sign;
	}
	
	private String getAuthSign2(String redirectUrl, String originalId){
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("redirectUrl", redirectUrl);
		treeMap.put("oauthTypeBase", "1");
		treeMap.put("originalId", originalId);
		treeMap.put("type", "1");
		
		String sign = SignUtil.createSign(treeMap, originalId);
		return sign;
	}
	
	/**
	 * 测试开发
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testDev() throws UnsupportedEncodingException {
		String original_id = "gh_5fdb1e85433c";
		String brandCode = "MA";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		StringBuilder returnUrl = new StringBuilder("http://dev.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(dev_url, redirectUrl, original_id, authSign);
		
		System.out.println("开发环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试培训环境 
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void testDemo() throws UnsupportedEncodingException {
		String original_id = "gh_4eec2c0da696";
		String brandCode = "MA";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		StringBuilder returnUrl = new StringBuilder("http://demo.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(demo_url, redirectUrl, original_id, authSign);
		
		System.out.println("培训环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试“测试”
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testTest() throws UnsupportedEncodingException {
		String original_id = "gh_b3825b7a38c5";
		String brandCode = "TM";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		// 411B1DA58A265BE231ED723CADDF131D
		System.out.println("sign: "+ params.get("sign"));
		
		StringBuilder returnUrl = new StringBuilder("http://test.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign2(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(test_url, redirectUrl, original_id, authSign);
		
		System.out.println("测试环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试“用户测试”
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testUserTest() throws UnsupportedEncodingException {
		String original_id = "gh_c348e222ab0f";
		String brandCode = "TM";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		StringBuilder returnUrl = new StringBuilder("http://usertest.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(usertest_url, redirectUrl, original_id, authSign);
		
		System.out.println("用户测试环境地址: \n" + finalUrl);
	}
	
	/**
	 * 测试“预测试”
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testPreTest() throws UnsupportedEncodingException {
		String original_id = "gh_a1a0a310bc42";
		String brandCode = "JP";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		StringBuilder returnUrl = new StringBuilder("http://pre.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(pre_url, redirectUrl, original_id, authSign);
		
		System.out.println("预测试环境地址: \n" + finalUrl);
	}
	
	@Test
	public void testPre2() throws UnsupportedEncodingException {
		String original_id = "gh_a1a0a310bc42";
		
		String pre_url="http://pre.qxclub.cn/wechat/wx/authorize.do?"
				+"redirectUrl=%1$s&oauthTypeBase=0&originalId=%2$s&type=0&sign=%3$s";
		String oauthTypeBase = "0";
		String type = "0";
		
		// String returnUrl = "http://pre.qxclub.cn/wcap/coupon_controllers/claim_coupon_new_index.do?memberRuleId=ff80808153bcb1b40153cbe5cfe70003";
		String returnUrl = "http://pre.qxclub.cn/wcap/coupon_controllers/claim_coupon_new_index.do?memberRuleId=ff80808153d113960153eae6fb84001b&couponRuleCode=CTD16000160&color=1";
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign3(redirectUrl, original_id, oauthTypeBase, type);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(pre_url, redirectUrl, original_id, authSign);
		
		System.out.println("预测试环境地址2: \n" + finalUrl);
	}
	
	// 添加发送模版签名
	private SortedMap<String, String> addSign(String brandCode, String key){

		String welcomeTitle = "【这是测试】勇敢的少年，恭喜你报名成功！";
		String activityAddress = "武汉大学樱花大道";
		String activityTime = "2016年3月20日";
		String activitySubject = "挑战吉尼斯世界纪录【最长的跳房子游戏图案】";
		String templateType = "APPLY_SUCCESS_NOTICE";
		String remark = "点击【详情】了解更多，天美意与你不见不散！";
		
		SortedMap<String, String> params = new TreeMap<String, String>();
		// openid 不做签名，考虑到跳转方式，在跳转之前获取不到openid
		params.put("brandCode", brandCode);
		params.put("welcomeTitle", welcomeTitle);
		params.put("activityAddress", activityAddress);
		params.put("activitySubject", activitySubject);
		params.put("activityTime", activityTime);
		params.put("templateType", templateType);
		params.put("remark", remark);
		
		String sign = SignUtil.createSign(params, key);
		params.put("sign", sign);
		
		return params;
	}
	
	/**
	 * 测试生产
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void testWx() throws UnsupportedEncodingException{
		String original_id = "gh_e0614ad15746";
		String brandCode = "TM";
		SortedMap<String, String> params = addSign(brandCode, original_id);
		
		StringBuilder returnUrl = new StringBuilder("http://wx.qxclub.cn/portal/wechat_template/send_tempplate_message.do?");
		for(Map.Entry<String, String> param : params.entrySet()){
			String paramName = param.getKey();
			String paramValue = param.getValue();
			paramValue = URLEncoder.encode(paramValue, "UTF-8");
			returnUrl.append(paramName).append("=").append(paramValue).append("&");
		}
		
		String redirectUrl = returnUrl.toString();
		System.out.println("redirectUrl\n"+redirectUrl);
		
		String authSign  = getAuthSign(redirectUrl, original_id);
		redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8");
		
		String finalUrl = String.format(wx_url, redirectUrl, original_id, authSign);
		
		System.out.println("正式环境地址: \n" + finalUrl);
	}

}
