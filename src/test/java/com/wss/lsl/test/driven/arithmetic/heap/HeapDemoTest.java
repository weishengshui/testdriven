package com.wss.lsl.test.driven.arithmetic.heap;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class HeapDemoTest {

	private Random random = new Random();
	private int size;
	private int arrayCount;
	private int[][] heapDatas;
	private HeapDemo[] heapDemos;

	@Before
	public void tearDownAfterClass() {
		size = 5 + random.nextInt(100);
		// size = 10;
		arrayCount = 1000000;
		heapDatas = new int[arrayCount][size + 1];
		heapDemos = new HeapDemo[arrayCount];

		for (int i = 0; i < arrayCount; i++) {
			for (int j = 1; j <= size; j++) {
				heapDatas[i][j] = random.nextInt(10000);
			}

			heapDemos[i] = new HeapDemo(heapDatas[i]);
		}
	}

	@Test
	public void testSiftup() {
		for (int i = 0; i < arrayCount; i++) {
			for (int j = 1; j <= size; j++) {
				heapDemos[i].siftup(j);
			}
			heapDemos[i].verify();
		}
	}

	@Test
	public void testSiftdown() {
		testSiftup();

		for (int i = 0; i < arrayCount; i++) {
			for (int j = 1; j <= size; j++) {
				heapDemos[i].siftdown(random.nextInt(10000));
				heapDemos[i].verify();
			}
		}
	}

}
