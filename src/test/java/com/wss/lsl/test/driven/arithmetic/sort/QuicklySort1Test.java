package com.wss.lsl.test.driven.arithmetic.sort;

import org.junit.Before;
import org.junit.Test;

/**
 * 测试快排1
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public class QuicklySort1Test extends SortBaseTest {

	@Before
	public void before() {
		super.before();
		for (int i = 0; i < arrayCount; i++) {
			sort[i] = new QuicklySort1<Integer>(data[i]);
		}
	}

	@Test
	public void testSort() {
		for (int i = 0; i < arrayCount; i++) {
			sort[i].sort();
			sort[i].validate();
		}
	}
}
