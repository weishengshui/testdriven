package com.wss.lsl.test.driven.date20211227;

import java.util.Stack;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 汉诺塔。
 * 
 * <pre>
 * 盘子摆放规则：大盘子在下，小盘子在上
 * A柱子上有盘子
 * 将A柱子上的盘子移到B柱子上，摆放规则维持不变
 * T柱子用于临时摆放盘子，同时要满足摆放规则
 * </pre>
 * 
 * @author weiss
 * @since 2021年12月27日
 */
@Data
@AllArgsConstructor
public class Hanoi {

	private Stack<Integer> pillarA;
	private Stack<Integer> pillarB;
	private Stack<Integer> pillarT;
	
	
	public void movePillarA2B() {
		
		// check
		if (pillarA.isEmpty() 
				|| !pillarB.isEmpty()
				|| !pillarT.isEmpty()) {
			throw new IllegalArgumentException();
		}
		int n = pillarA.size();
		xx(n, pillarA, pillarB, pillarT);
	}
	
	private void xx(int n, Stack<Integer> A, Stack<Integer> B, Stack<Integer> T) {
		if (n == 1) {
			B.push(A.pop());
			return;
		}
		
		int pre = n - 1;
		// 把前(n-1)个移到T柱上
		xx(pre, A, T, B);
		
		// 把最后1个移到B柱上
		B.push(A.pop());
		
		// 把前(n-1)个移到B柱上
		xx(pre, T, B, A);
	}
}
