package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class IntSetListTest {

	private IntSetList intSetList;

	@Before
	public void setUp() {
		intSetList = new IntSetList();
	}

	@Test
	public void testInsert() {
		// 前置条件检查
		assertNull(intSetList.getList());

		// 插入有序的几个数
		int[] ints = new int[] { 1, 2, 3, 3, 4, 5 };
		for (int val : ints) {
			intSetList.insert(val);
		}
		assertEquals(ints.length, intSetList.size());
		Node res = intSetList.getList();
		int i = 0;
		while (res != null) {
			assertEquals(ints[i], res.val);
			i++;
			res = res.next;
		}

		// 插入无序的几个数
		setUp();
		ints = new int[] { 1, 5, 10, -1, 0, 2, 3, 3, 4, 5 };
		int[] ints2 = new int[] { -1, 0, 1, 2, 3, 3, 4, 5, 5, 10 };
		for (int val : ints) {
			intSetList.insert(val);
		}
		res = intSetList.getList();
		i = 0;
		while (res != null) {
			assertEquals(ints2[i], res.val);
			i++;
			res = res.next;
		}

		// 查看排序10W个数要多久
		// 排序10W个数 76s
		// 排序1W个数 1s
		// 排序2W个数 3s
		// 排序4W个数 9s
		// 数据准备
		setUp();
		Random random = new Random();
		final int count = 10000;
		ints = new int[count];
		i = 0;
		while (i < count) {
			ints[i] = random.nextInt();
			i++;
		}
		// set 会排除重复的数据
		List<Integer> sortedInts = new ArrayList<Integer>();
		i = 0;
		while (i < count) {
			sortedInts.add(ints[i]);
			i++;
		}
		Collections.sort(sortedInts);
		long begin = System.currentTimeMillis();
		for (int val : ints) {
			intSetList.insert(val);
		}
		System.out.println("sorted " + count + " number time: "
				+ (System.currentTimeMillis() - begin) / 1000 + " s");
		assertEquals(count, intSetList.size());
		res = intSetList.getList();
		i = 0;
		while (res != null) {
			assertEquals(sortedInts.get(i).intValue(), res.val);
			i++;
			res = res.next;
		}
	}

}
