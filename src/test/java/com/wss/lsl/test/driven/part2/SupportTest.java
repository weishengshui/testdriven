package com.wss.lsl.test.driven.part2;

import org.easymock.EasyMock;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.dao.UserDao;
import com.wss.lsl.test.driven.part2.entity.User;

public class SupportTest extends EasyMockSupport {
	
	private UserDao firstUserDao;
	private UserDao secondUserDao;
	private ClassUnderTest classUnderTest;
	private User user;
	
	@Before
	public void setUp() {
		classUnderTest = new ClassUnderTest();
		user = new User();
	}
	
	@Test
	public void testStrictType() {
		firstUserDao = createMock(UserDao.class);
		classUnderTest.setStrictUserDao(firstUserDao);
		
		EasyMock.expect(firstUserDao.save(EasyMock.isA(User.class))).andReturn(1);
		replayAll();
		
		classUnderTest.strictSave(user);
		verifyAll();
	}
	
	@Test
	public void testNiceType() {
		secondUserDao = createNiceMock(UserDao.class);
		classUnderTest.setNiceUserDao(secondUserDao);
		
		replayAll();
		classUnderTest.niceSave(user);
		
		verifyAll();
		
	}
	
}
