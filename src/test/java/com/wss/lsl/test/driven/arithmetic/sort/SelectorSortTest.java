package com.wss.lsl.test.driven.arithmetic.sort;

import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SelectorSortTest {

	private Random random;
	private int count;
	private int[] data;
	private int[] sortedData;

	@Before
	public void before() {
		random = new Random();
		count = 100000;

		data = new int[count];
		sortedData = new int[count];
		for (int i = 0; i < count; i++) {
			data[i] = random.nextInt(10000);
			sortedData[i] = data[i];
		}
		Arrays.sort(sortedData);
	}

	@Test
	public void testSort() {
		data = SelectorSort.sort(data);
		System.out.println(Arrays.toString(data));
		Assert.assertArrayEquals(sortedData, data);
	}

}
