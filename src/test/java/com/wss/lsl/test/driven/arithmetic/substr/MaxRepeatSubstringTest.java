package com.wss.lsl.test.driven.arithmetic.substr;

import org.junit.Before;
import org.junit.Test;

public class MaxRepeatSubstringTest {

	private MaxRepeatSubstring mrs;

	@Before
	public void before() {
		mrs = new MaxRepeatSubstring("banana123456789123456789".toCharArray());
	}

	@Test
	public void test() {
		mrs.report();
		mrs.printMaxSubstr();
	}

}
