package com.wss.lsl.test.driven.date20220109;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * 
  *  子数组的可等于k的个数
 * 
 * @author weiss
 *
 */
public class SubArraySumKTest {

	private int[] nums;
	private int k;

	@Before
	public void setUp() {
		k = 9;

		nums = new int[] { 5, 2, 7, 0, 0, 8, 7, 1, 1, 2, 5, 6 };
	}

	@Test
	public void test1() {

		int count = getKCount(nums, k);
		assertEquals(5, count);
		
		count = getKCount(nums, 0);
		assertEquals(3, count);
	}

	private int getKCount(int[] nums, int k) {

		int kcount = 0;
		int sumj = 0;
		Map<Integer, Integer> sumCountMap = new HashMap<>();
		for (int j = 0; j < nums.length; j++) {
			sumj += nums[j];
			if (sumj == k) {
				kcount++;
			}

			int sumi = sumj - k;
			if (sumCountMap.containsKey(sumi)) {
				kcount += sumCountMap.get(sumi);
			}
			sumCountMap.merge(sumj, 1, (i1, i2) -> i1 + i2);
		}

		return kcount;
	}
}
