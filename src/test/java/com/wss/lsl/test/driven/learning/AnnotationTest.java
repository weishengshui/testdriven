package com.wss.lsl.test.driven.learning;

import static org.junit.Assert.fail;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.Test;

public class AnnotationTest {

	@Target(ElementType.ANNOTATION_TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	@interface Constraints {
		boolean allowNull() default true;

		boolean primaryKey() default false;

		boolean unique() default false;
	}
	
	// 无法直接替代@Constraints(unique = true)
	@interface Uniqueness {
		Constraints constraints() default @Constraints(unique = true);
	}

	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	@interface SqlInteger {
		String name() default "";

		Constraints constraints() default @Constraints;
	}

	class Table {

		@SqlInteger
		private int id;
		@SqlInteger(constraints = @Constraints(unique = true))
		private int age;
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
