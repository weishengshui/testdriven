package com.wss.lsl.test.driven.arithmetic.heap;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class HeapSortTest {

	private Random random = new Random();
	private int size;
	private int arrayCount;
	private int[][] heapDatas;
	private HeapSort[] heapSorts;

	@Before
	public void tearDownAfterClass() {
		size = 5 + random.nextInt(100);
		// size = 10;
		arrayCount = 100;
		heapDatas = new int[arrayCount][size + 1];
		heapSorts = new HeapSort[arrayCount];

		for (int i = 0; i < arrayCount; i++) {
			for (int j = 1; j <= size; j++) {
				heapDatas[i][j] = random.nextInt(10000);
			}

			heapSorts[i] = new HeapSort(heapDatas[i]);
		}
	}

	@Test
	public void testSort() {
		for (int i = 0; i < arrayCount; i++) {
			heapSorts[i].sort();
			heapSorts[i].validateSort();
			// System.out.println(heapSorts[i]);
		}
	}

	@Test
	public void testSiftup() {
		for (int i = 0; i < arrayCount; i++) {
			for (int j = 2; j <= size; j++) {
				heapSorts[i].siftup(j);
			}
			heapSorts[i].verifyHeap();
		}
	}

}
