package com.wss.lsl.test.driven.visitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.annotation.ScopedProxyMode;

import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;
import com.ql.util.express.rule.Condition;

public class EvaluatorTest {

	private Evaluator evaluator = new Evaluator();

	@Test
	public void testVisitor() {

		Node add = new Add(new Number(3), new Number(4));
		double result = evaluator.visitor(add);
		Assert.assertTrue(Double.compare(7.0d, result) == 0);

		Negate negate = new Negate(add);
		result = evaluator.visitor(negate);
		Assert.assertTrue(Double.compare(-7.0d, result) == 0);
	}

	@Test
	public void test2() {
		// 测试： 1 + 2 * (3 - 4) / 5
		Node t1 = new Sub(new Number(3), new Number(4));
		Node t2 = new Mul(new Number(2), t1);
		Node t3 = new Div(t2, new Number(5));
		Node t4 = new Add(new Number(1), t3);

		double result = evaluator.visitor(t4);
		Assert.assertTrue(Double.compare(0.6d, result) == 0);
	}

	@Test
	public void test3() throws Exception {
		// 四则运算有括号
		// 如何解析表达式

		String express = "(1 +2 * (3 - 4) / 5) + (1+2)";
		ExpressRunner runner = new ExpressRunner(true, false);
		DefaultContext<String, Object> context = new DefaultContext<String, Object>();
//		context.put("a",1);
//		context.put("b",2);
//		context.put("c",3);
		Object r = runner.execute(express, context, null, true, false);
		System.out.println(r);
		Condition condition = runner.parseContition("(变量A + 变量B * (变量C - 4) / 5) + (1+2)");
		System.out.println(condition);

		try {
			condition = runner.parseContition("1+2)");
			Assert.fail(); // 无效的表达式不应该执行到这里
			System.out.println(condition);
		} catch (Exception e) {
			// ignore
		}

		boolean valid = runner.checkSyntax("1/0.0");
		Assert.assertTrue(valid);
		try {
			runner.execute("1/0.0", context, null, true, false);
			Assert.fail(); // 无效的表达式不应该执行到这里
		} catch (Exception e) {
			// ignore
		}
	}

	@Test
	public void testParseRoundBrackets() throws FileNotFoundException, IOException {
		// 测试用例
//		()()()
//		(())()()()()()
//		)(
//		(()
//
//		true
//		true
//		false
//		false
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(new File("D:\\tmp\\newcoder\\RoundBrackets.txt"))))) {
			List<Boolean> result = new ArrayList<>(); // 程序校验的结果
			List<Boolean> expected = new ArrayList<>(); // 期望的结果
			String str = null;
			boolean expectedBegin = false;
			while ((str = br.readLine()) != null) {
				if (expectedBegin) {
					expected.add(Boolean.valueOf(str));
				} else if ("".equals(str)) {
					expectedBegin = true;
				} else {
					result.add(checkRoundBracketsValid(str));
				}
			}

			Assert.assertTrue(result.equals(expected));
		}
	}

	/**
	 * 检查圆括号是否闭合有效
	 * 
	 * @param str
	 * @return
	 */
	private Boolean checkRoundBracketsValid(String str) {

		Boolean result = Boolean.TRUE;
		Stack<Character> leftRoundBrackets = new Stack<>();
		for (int i = 0, length = str.length(); i < length; i++) {
			char c = str.charAt(i);
			if (c == '(') { // 左括号，入栈
				leftRoundBrackets.push(c);
			} else if (leftRoundBrackets.isEmpty()) {
				result = Boolean.FALSE;
				break;
			} else { // 遇到右括号，将左括号出栈
				leftRoundBrackets.pop();
			}
		}
		if (leftRoundBrackets.isEmpty() == false) {// 最后比较完了，还有左括号
			result = Boolean.FALSE;
		}
		return result;
	}

	@Test
	public void test4() {
		String[] strs = { "()(){}[]", "({[]}){}", "{()()[]" };

		// 读取最长的有效闭合
		Stack<Character> leftRoundBrackets = new Stack<>();
		for (String str : strs) {
			leftRoundBrackets.clear();
			int preMaxCloseLen = 0;
			int currentMaxCloseLen = 0;
			for (int i = 0, length = str.length(); i < length; i++) {
				char c = str.charAt(i);
				boolean isLeft = c == '(' || c == '[' || c == '{';
				if (isLeft) { // 把左括号压入栈
					leftRoundBrackets.push(c);
				} else if (leftRoundBrackets.isEmpty()) {
					// 遇到了无效闭合，新的闭合长度需要重新算
					preMaxCloseLen = Math.max(preMaxCloseLen, currentMaxCloseLen);
					currentMaxCloseLen = 0;
				} else {
					char last = leftRoundBrackets.pop();
					boolean isClosed = (last == '(' && c == ')') || (last == '[' && c == ']')
							|| (last == '{' && c == '}');
					if (isClosed) {// 找到了一对闭合
						currentMaxCloseLen += 2;
					} else {
						// 遇到了无效闭合，新的闭合长度需要重新算
						preMaxCloseLen = Math.max(preMaxCloseLen, currentMaxCloseLen);
						currentMaxCloseLen = 0;
					}
				}
			}
			System.out.println(Math.max(preMaxCloseLen, currentMaxCloseLen));
		}
	}

	@Test
	public void test5() {

		byte b = 1;
		short sh = 1;
		int i = 1;
		char c = '1';
		long l = 1;
		double d = 1.0;
		float f = 1.0f;
		String s = "1";
		ScopedProxyMode mode = ScopedProxyMode.DEFAULT;
		mode.ordinal();
		testSwitch(b, sh, i, c, l, d, f, s, mode);
	}

	// switch 底层实现是通过int实现的。
	// 枚举类型是通过ordinal()返回的整数值比价的。
	// java 7 开始支持string，case通过hashCode()比较，hashCode相同是通过equals()比较。
	private void testSwitch(byte b, short sh, int i, char c, long l, double d, float f, String s,
			ScopedProxyMode mode) {
		switch (b) {
		case 1:
			break;
		}
		switch (sh) {
		case 1:
			break;
		}
		switch (i) {
		case 1:
			break;
		}
		switch (c) {
		case 1:
			break;
		}
//		switch (l) { // long不支持
//		case 1:
//			break;
//		}
//		switch (d) {
//		case 1:
//			break;
//		}
//		switch (f) {
//		case 1:
//			break;
//		}

		switch (s) {
		case "1":
			break;
		}
		switch (mode) {
		case DEFAULT:
			break;
		default:
			break;
		}

//		Long ll = 1L;
//		switch (ll) {
//		
//		}
	}

}
