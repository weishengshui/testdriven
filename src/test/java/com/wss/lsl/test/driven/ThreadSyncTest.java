package com.wss.lsl.test.driven;

public class ThreadSyncTest {
  
    private static final  OutName out = new OutName();
     
    public static void main(String[] args) {
         
            new Thread(new Runnable() {
 
                @Override
                public void run() {
                    out.printName("liutao");
                }
            }).start();
 
            new Thread(new Runnable() {
 
                @Override
                public void run() {
                    out.printName("chuanzhi");
 
                }
            }).start();
 
    }
 
    static class OutName {
 
        public synchronized  void printName(String name) {
            while (true) {
                //synchronized (this) {
                    for (int i = 0; i < name.length(); i++) {
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.print(name.charAt(i));
                    }
                    System.out.println("------------------->" + Thread.currentThread().getName());
                //}
            }
        }
    }
}
