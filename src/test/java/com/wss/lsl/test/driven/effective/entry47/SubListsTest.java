package com.wss.lsl.test.driven.effective.entry47;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SubListsTest {

	private long begin;

	@Before
	public void setUp() {
		begin = System.currentTimeMillis();
	}

	@After
	public void after() {
		System.out.println(System.currentTimeMillis() - begin);
	}

	@Test
	public void testOf() {
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");

		SubLists.of(list).forEach(System.out::println);
	}

	@Test
	public void test1() {
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");

		of(list).forEach(System.out::println);
	}

	public static <E> Stream<List<E>> of(List<E> list) {
		return IntStream.range(0, list.size()).mapToObj(start -> IntStream
				.rangeClosed(start + (int) Math.signum(start), list.size()).mapToObj(end -> list.subList(start, end)))
				.flatMap(t -> t);
	}

	@Test
	public void test2() {
		// Math.signum()负数、0、正数对应的返回值：-1、0、1
		System.out.println((int) Math.signum(0) == 0); // 0
		System.out.println((int) Math.signum(1) == 1); // 1
		System.out.println((int) Math.signum(2) == 1); // 1
	}

	@Test
	public void test3() {
		Double.toHexString(0.1);
		double a1 = 0.1;
		double a2 = 0.2;
		double a = a1 + a2;
		double b = 0.3;
		System.out.println(a == b);
		System.out.println(Double.compare(a, b) == 1); // a > b
		System.out.println(Double.toHexString(0.1));
	}

	@Test
	public void test4() {
		Assert.assertEquals(0, 1 >> 1);
		Assert.assertEquals(0, 2 & 1);
		Assert.assertEquals(1, 3 & 1);
	}

	/**
	 * 适配。流转化可迭代对象
	 * 
	 * @param s 流对象
	 * @return
	 */
	public static <E> Iterable<E> iterableOf(Stream<E> s) {
		return s::iterator;
	}

	/**
	 * 适配。可迭代对象转化为流
	 * 
	 * @param i
	 * @return
	 */
	public static <E> Stream<E> streamOf(Iterable<E> i) {
		return StreamSupport.stream(i.spliterator(), false);
	}

	@Test
	public void testToArray() {
		List<String> list = new ArrayList<>(3);
		list.add("1");
		list.add("2");
		list.add("3");

		String[] array = { "", "", "", "4", "5" }; // 超过List的size，第size(从0开始) 元素设为null
		list.toArray(array);
		Assert.assertTrue(array[list.size()] == null); // 第size(从0开始) 元素设为null
		Assert.assertTrue(array[list.size() - 1].equals(list.get(list.size() - 1))); // 数组和列表的最后一个元素是相同的
		Assert.assertTrue(array[list.size() + 1].equals("5")); // 超过size个的元素值不变
	}

	@Test
	public void test5() {
		LocalDate date1 = LocalDate.now();

		// get the second saturday of this month
		LocalDate firstInYear = LocalDate.of(date1.getYear(), date1.getMonth(), 1);
		LocalDate secondSaturday = firstInYear.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY))
				.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
		System.out.println("Second Saturday on : " + secondSaturday);

		ZoneId zoneId = ZoneId.systemDefault();
		System.out.println(zoneId);
	}

	@Test
	public void test6() throws IOException {
		String s = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		do {
			s = br.readLine();
			System.out.println(s);
		} while ("".equals(s) == false);
	}

	@Test
	public void test7() {

//		Scanner in = new Scanner(System.in);
//		String str = in.nextLine();

		String s = "hello world";
		Pattern p = Pattern.compile("^(\\S+\\s*)+$");
		Matcher m = p.matcher(s);
		if (m.find()) {
			System.out.println(m.group(m.groupCount()).trim().length());
		}
	}

	@Test
	public void test8() {
		String s = "ABCDEAAFxxxxAaa";
		String c = "A";
		Pattern p = Pattern.compile("(?i)" + c); // 不区分大小写
		Matcher m = p.matcher(s);

		int count = 0;
		while (m.find()) {
			count++;
		}
		System.out.println(count);
	}

	private String[] zeros = { "0", "00", "000", "0000", "00000", "000000", "0000000" };
	private final int charCount = 8;

	@Test
	public void test9() {
		String[] strs = { "abc", "123456789" };
		StringBuilder sb = new StringBuilder();
		for (String s : strs) {
			if (s.length() <= charCount) {
				appendZeros(s, sb);
			} else {
				for (int i = 0, length = (s.length() / charCount) * charCount; i < length; i += charCount) {
					appendZeros(s.substring(i, i + charCount), sb);
				}
				int mod = s.length() % charCount;
				if (mod > 0) { // 最后还剩余一段
					appendZeros(s.substring(s.length() - mod), sb);
				}
			}

		}
		// sb.setLength(sb.length() - 1); // 去掉最后一个换行符。注意细节
		System.out.print(sb);
	}

	private void appendZeros(String temp, StringBuilder sb) {
		sb.append(temp);
		if (temp.length() < charCount) {
			sb.append(zeros[charCount - temp.length() - 1]);
		}
		sb.append("\n");
	}

	@Test
	public void test10() {
		String input = "A";
		StringBuilder sb = new StringBuilder();
		sb.append(Long.parseLong(input, 16)).append("\n");

		System.out.print(sb);
	}

	@Test
	public void test11() {
		long input = 180;

		System.out.println(getResult(input));
	}

	public String getResult(long input) {

		StringBuilder sb = new StringBuilder();
		BigInteger bigInteger = BigInteger.valueOf(input);
		if (bigInteger.isProbablePrime(10)) {
			return sb.append(input).append(" ").toString();
		}

		long i = 1;
		while (input > 1) {
			BigInteger prime = nextPrime(i);
			while (input > 1 && input % prime.longValue() == 0) {
				sb.append(prime.longValue()).append(" ");
				input /= prime.longValue();
			}
			i = prime.longValue();
		}

		return sb.toString();

	}

	private BigInteger nextPrime(long i) {

		while (i <= Long.MAX_VALUE) {
			i += 1;
			BigInteger bi = BigInteger.valueOf(i);
			if (bi.isProbablePrime(10)) {
				return bi;
			}
		}
		return null;
	}

	@Test
	public void test12() {
		String str = "5.4";
		double d = Double.valueOf(str);
		long l = (long) d;
		if (str.contains(".")) {
			char c = str.charAt(str.indexOf(".") + 1);
			if (c >= '5') {
				l += 1;
			}
		}
		// l = d > 0.5 ? l + 1 : l;
		System.out.println(l);
	}

	@Test
	public void test13() {
		Map<Integer, Integer> map = new TreeMap<>();
		int[] keys = { 0, 0, 1, 3 };
		int[] values = { 1, 2, 2, 4 };
		for (int i = 0, length = keys.length; i < length; i++) {
			int key = keys[i];
			int value = values[i];
			map.compute(key, (t, u) -> u == null ? value : u + value);
		}
		map.forEach((k, v) -> {
			System.out.println(String.format("%d %d", k, v));
		});
	}

	@Test
	public void test14() throws FileNotFoundException {
		Scanner in = new Scanner(new File("D:\\tmp\\newcoder\\merge_table.txt"));

		int n = 0;
//		while (true) { // 假设索引是有顺序的
//			n = in.nextInt();
//
//			int[] keys = new int[n];
//			int[] values = new int[n];
//			int lastIndex = 0;
//			for (int i = 0; i < n; i++) {
//				int key = in.nextInt();
//				int value = in.nextInt();
//				if (keys[lastIndex] == key) {
//					values[lastIndex] += value;
//				} else {
//					lastIndex++;
//					keys[lastIndex] = key;
//					values[lastIndex] = value;
//				}
//			}
//			for (int i = 0; i <= lastIndex; i++) {
//				System.out.println(String.format("%d %d", keys[i], values[i]));
//			}
//
//			if (in.hasNext() == false) {
//				break;
//			}
//		}

		Map<Integer, Integer> map = new TreeMap<>();
		while (true) {
			n = in.nextInt();
			map.clear();

			for (int i = 0; i < n; i++) {
				int key = in.nextInt();
				int value = in.nextInt();
				map.compute(key, (t, u) -> u == null ? value : u + value);
			}
			map.forEach((k, v) -> {
				System.out.println(String.format("%d %d", k, v));
			});

			if (in.hasNext() == false) {
				break;
			}
		}

		in.close();
	}

	@Test
	public void test15() throws FileNotFoundException {
		boolean[] flags = new boolean[10];
		StringBuilder sb = new StringBuilder(10);
		Scanner in = new Scanner(new File("D:\\tmp\\newcoder\\distinct_int.txt"));

		while (in.hasNext()) {
			String input = in.nextLine();
			for (int i = input.length() - 1; 0 <= i; i--) {
				char c = input.charAt(i);
				int index = c - '0';
				if (flags[index] == false) {
					sb.append(c);
					flags[index] = true;
				}
			}

			System.out.println(sb);
			sb.setLength(0);
		}

		in.close();
	}

	@Test
	public void test16() throws FileNotFoundException {
		byte count = 0;
		boolean[] flags = new boolean[128];
		Scanner in = new Scanner(new File("D:\\tmp\\newcoder\\chars_count.txt"));
		while (in.hasNext()) {
			String s = in.nextLine();
			count = 0;
			for (int i = 0, length = s.length(); i < length; i++) {
				char c = s.charAt(i);
				if (flags[c] == false) {
					flags[c] = true;
					count++;
				}
			}
			if (count < 0) {
				System.out.println(128);
			} else {
				System.out.println(count);
			}
		}

		in.close();
		// System.out.println(Math.abs(b));
	}

	@Test
	public void test17() throws FileNotFoundException {
		Scanner in = new Scanner(new File("E:\\tmp\\newcoder\\contains_char.txt"));
		while (in.hasNext()) {
			String i1 = in.nextLine();
			String i2 = in.nextLine();
			boolean matched = true;

			for (char c = 'A'; c <= 'Z'; c++) {
				if (i2.indexOf(c) != -1 && i1.indexOf(c) == -1) {
					matched = false;
					break;
				}
			}

			System.out.println(matched);
		}

		in.close();
	}

	@Test
	public void test18() throws FileNotFoundException {

		long begin = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		try (Scanner in = new Scanner(new File("D:\\tmp\\newcoder\\digit_reverse.txt"));) {
			String input = in.nextLine();
			for (int i = input.length() - 1; 0 <= i; i--) {
				sb.append(input.charAt(i));
			}
			System.out.println(sb);
		}
		System.out.println(System.currentTimeMillis() - begin);
	}

	@Test
	public void test19() throws IOException {

		try (BufferedInputStream bis = new BufferedInputStream(
				new FileInputStream(new File("D:\\tmp\\newcoder\\string_reverse.txt")), 1024)) {
			int n = bis.available();
			StringBuilder sb = new StringBuilder(n);
			int i = bis.read();
			while (i != -1 && i != '\n') {
				sb.insert(0, (char) i);

				i = bis.read();
			}
			System.out.println(sb);
		}
	}

	@Test
	public void test20() throws FileNotFoundException, IOException {
		try (BufferedInputStream bis = new BufferedInputStream(
				new FileInputStream(new File("D:\\tmp\\newcoder\\sentence_reverse.txt")), 1024)) {
			int n = bis.available();
			StringBuilder sb = new StringBuilder(n);
			int i = bis.read();
			int letterCount = -1;
			while (i != -1 && i != '\n') {
				letterCount = i == ' ' || letterCount == -1 ? 0 : letterCount + 1;
				sb.insert(letterCount, (char) i);

				if (i == ' ') {
					letterCount = -1;
				}
				i = bis.read();
			}
			System.out.println(sb);
		}
	}

	@Test
	public void test21() {
		Integer i = 1;
		Integer j = 1;
		Assert.assertTrue(i == 1);
		Assert.assertTrue(i == j);

		i = 300;
		j = 300;
		Assert.assertTrue(i == 300);
		Assert.assertFalse(i == j);
	}

	@Test
	public void test22() {
		Map<String, String> map = new HashMap<String, String>(1);
		map.put("1", "1"); // 哈希表容量扩大了1倍，是2
		map.put("2", "2"); // 哈希表容量变成了4
	}

	@Test
	public void test23() throws FileNotFoundException, IOException {

		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(new File("D:\\tmp\\newcoder\\string_concat.txt"))), 1024)) {

			final int count = Integer.valueOf(br.readLine());
			int ii = 0;

			String[] input = new String[count];
			while (ii < count) {
				input[ii] = br.readLine();
				ii++;
			}

			StringBuilder sb = new StringBuilder(count * 50);
			for (int i = 0, length = input.length; i < length; i++) {
				String min = input[i];
				int minIndex = i;
				for (int j = i + 1; j < length; j++) {
					if (input[j].compareTo(min) < 0) {
						min = input[j];
						minIndex = j;
					}
				}
				if (minIndex > i) {
					input[minIndex] = input[i];
					input[i] = min;
				}
				sb.append(min).append("\n");
			}
			sb.setLength(sb.length() - 1);
			System.out.println(sb.toString());
		}
	}

	// 读取整数中二进制1的个数
	@Test
	public void test24() throws FileNotFoundException, IOException {
		try (Scanner in = new Scanner(new File("D:\\tmp\\newcoder\\int_1count.txt"))) {
			// String str = in.nextLine();
			String str = Integer.toHexString(in.nextInt());
			byte count = (byte) 0;
			for (byte i = 0, length = (byte) str.length(); i < length; i++) {
				switch (str.charAt(i)) {
				case '1':
				case '2':
				case '4':
				case '8':
					count += 1;
					break;
				case '3':
				case '5':
				case '6':
				case '9':
				case 'a':
				case 'c':
					count += 2;
					break;
				case '7':
				case 'b':
				case 'd':
				case 'e':
					count += 3;
					break;
				case 'f':
					count += 4;
				}
			}
			Assert.assertTrue(31 == count);
		}
	}

	// 读取整数中二进制1的个数。本方法速度更快
	@Test
	public void test24_2() throws FileNotFoundException, IOException {

		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(new File("D:\\tmp\\newcoder\\int_1count.txt"))), 1024)) {
			String str = br.readLine();
			str = Integer.toBinaryString(Integer.parseInt(str));
			int count = 0;
			for (int i = 0, length = str.length(); i < length; i++) {
				if (str.charAt(i) == '1') {
					count++;
				}
			}
			Assert.assertEquals(31, count);
		}
	}
}
