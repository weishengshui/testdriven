package com.wss.lsl.test.driven.effective.entry47;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.Test;

public class PowerSetTest {

	@Test
	public void testOf() {
		Set<String> s = new HashSet<>();
		s.add("a");
		s.add("b");
		s.add("c");

		Collection<Set<String>> list = PowerSet.of(s);
		for (Set<String> st : list) {
			System.out.println(st);
		}
	}

	@Test
	public void test1() {
		Stream.of("A", "B").flatMap(t -> Stream.of(t, t)).forEach(System.out::println);
	}

}
