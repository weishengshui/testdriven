package com.wss.lsl.test.driven.arithmetic;

import org.junit.Assert;
import org.junit.Test;

/**
 * 逆序对次数测试
 * 
 * @author weiss
 *
 */
public class ReverseTupleTest {

	@Test
	public void testCount() {
		int[] data = { 2, 3, 8, 6, 1 };
		Assert.assertEquals(5, ReverseTuple.count(data, 0, data.length - 1));

		// 逆序排列的数组，逆序对最多。
		int n = 10;
		data = new int[n];
		int begin = 100;
		for (int i = 0; i < n; i++) {
			data[i] = begin - i;
		}
		int count = (n - 1) * n / 2;
		Assert.assertEquals(count, ReverseTuple.count(data, 0, data.length - 1));
	}

}
