package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.common.time.SystemTime;
import com.wss.lsl.test.driven.part2.common.time.TimeSource;

public class SystemTimeAbstractionTest {

	@After
	public void tearDown() {
		SystemTime.reset();
	}

	@Test
	public void clockReturnsValidMilliseconds() {
		long before = System.currentTimeMillis();
		long clock = SystemTime.asMillis();
		long after = System.currentTimeMillis();
		assertBetween(before, clock, after);

	}

	@Test
	public void clockReturnsFakedTimeInMilliseconds() {
		final long fakeTime = 1234567890l;
		SystemTime.setTimeSource(new TimeSource() {

			@Override
			public long millis() {
				return fakeTime;
			}
		});
		long clock = SystemTime.asMillis();
		assertEquals("Should return fake time", fakeTime, clock);
	}

	private void assertBetween(long before, long clock, long after) {
		assertTrue("Should've return something between " + before + " and "
				+ after + " (instead of " + clock + ")", before <= clock
				&& clock <= after);
	}

}
