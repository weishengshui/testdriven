package com.wss.lsl.test.driven.arithmetic;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TwiceLinear {

	public int twiceLinear(int n) {

		List<Integer> nums = new ArrayList<>();
		nums.add(1);
		int y = 0;
		int z = 0;
		while (nums.size() <= n) {
			int yy = 2 * nums.get(y) + 1;
			int zz = 3 * nums.get(z) + 1;
			if (yy < zz) {
				nums.add(yy);
				y++;
			} else if (yy > zz) {
				nums.add(zz);
				z++;
			} else {
				y++;
			}
		}

		return nums.get(nums.size() - 1);
	}

	@Test
	public void test() {
		System.out.println(twiceLinear(10));
		System.out.println(twiceLinear(20));
		System.out.println(twiceLinear(30));
		System.out.println(twiceLinear(50));
		System.out.println(twiceLinear(100));
		System.out.println(twiceLinear(500));
		System.out.println(twiceLinear(1000));
		System.out.println(twiceLinear(2000));
		System.out.println(twiceLinear(6000));
		System.out.println(twiceLinear(60000));
		System.out.println(twiceLinear(14688));
		// 14688
	}

}
