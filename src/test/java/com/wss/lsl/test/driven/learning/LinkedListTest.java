package com.wss.lsl.test.driven.learning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.junit.Assert;
import org.junit.Test;

public class LinkedListTest {

	/**
	 * LinkedList 双向链表
	 */
	@Test
	public void test() {
		List<String> linkedList = new LinkedList<String>();
		linkedList.add("111");
	}

	@Test
	public void test2() {
		List<String> list = Collections.unmodifiableList(new ArrayList<String>(Arrays.asList("1", "2", "3", "4")));

		String a = list.get(0);
		a = "3";
		System.out.println(a);
		System.out.println(list);

		System.out.println("ListIterator---------------------------------------------------");
		ListIterator<String> lit = list.listIterator(2); // 索引从0开始
		while (lit.hasNext()) {
			System.out.print(lit.next() + " ");
		}
		System.out.println("ListIterator---------------------------------------------------");
	}

	@Test
	public void test3() {
		List<String> list = new ArrayList<>();
		Iterator<String> it = list.iterator();
		// 返回迭代器视图，就不能再对list进行修改。否则会抛异常ConcurrentModificationException
		// list.add("1");

		int size = 0;
		while (it.hasNext()) {
			it.next();
			size++;
		}

		list.add("1"); // 这里可以，但是不影响迭代器
		Assert.assertFalse(size == list.size());
	}

}
