package com.wss.lsl.test.driven.spring.factorybean;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import com.wss.lsl.test.driven.BaseTest;
import com.wss.lsl.test.driven.spring.factorybean.model.User;

/**
 * FactoryBean测试。获取bean和factory本身
 * 
 * @author weiss
 *
 */
public class FactoryBeanImplForUserServiceTest extends BaseTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() throws Exception {
		String userId = "1";

		// 获取bean
		UserService userService = (UserService) applicationContext.getBean("userService");
		User user = userService.queryUserById(userId);
		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getRole());

		// 获取factoryBean本身：在bean名字前面加"&"符号
		FactoryBeanImplForUserService factoryBeanImplForUserService = (FactoryBeanImplForUserService) applicationContext
				.getBean("&userService");
		Assert.assertNotNull(factoryBeanImplForUserService);

		UserService userService2 = factoryBeanImplForUserService.getObject();
		user = userService2.queryUserById(userId);
		Assert.assertNotNull(user);
		Assert.assertNotNull(user.getRole());
	}

	@Test
	public void testProfile() {
		UserService userService = (UserService) applicationContext.getBean("userService");
		System.out.println(userService.toString());
	}

	@Test
	public void test2() {
		ExecutorService executorService = Executors.newCachedThreadPool();
		executorService.submit(this::println);
		// executorService.submit(System.out::println); // 编译错误，因为
		// System.out.println()方法的重载导致的
	}

	private void println() {

	}

}
