package com.wss.lsl.test.driven.part2;

import com.wss.lsl.test.driven.common.Constants;

public class LoginPageTest extends MyJspTestCase {

	/**
	 * jsp页面出现中文就报异常
	 * 
	 * @throws Exception
	 */
	public void testLoginPageFormFieldsPresent() throws Exception {
		get("/login.jsp");
		form().shouldHaveField(Constants.USERNAME_PARAMETER);
		form().shouldHaveField(Constants.PASSWORD_PARAMETER);
		form().shouldHaveSubmitButton("login");
	}
	
	public void testPreviousUsernameIsRetained() throws Exception {
		setRequestAttribute(Constants.USERNAME_PARAMETER, "bob");
		get("/login.jsp");
		form().field(Constants.USERNAME_PARAMETER).shouldHaveValue("bob");
	}
}
