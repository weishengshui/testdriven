package com.wss.lsl.test.driven.date20211226;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

/**
 * 排序二叉树测试
 * 
 * @author weiss
 *
 */
public class SortedBtreeTest {

	private SortedBtree root;
	private Random random = new Random();
	int count = 1000;
	List<Integer> numbers;
	int secondMax;

	@Before
	public void setUp() {
		
		count = 1000;
		numbers = new ArrayList<>(count);
		while (count-- > 0) {
			numbers.add(random.nextInt());
		}

		// 初始化排序二叉树
		root = new SortedBtree();
		root.setValue(numbers.get(0));
		for (int i = 1; i < numbers.size(); i++) {
			root.initSortedBtree(root, numbers.get(i));
		}
		
		numbers.sort((o1, o2)->-o1.compareTo(o2));
		secondMax = numbers.get(1);
		System.out.println(secondMax);
	}

	@Test
	public void test() {
		
		int runTimes = 1000000;
		while(runTimes-- > 0) {
			setUp();
			SortedBtree secondMaxNode = root.secondMaxNode();
			boolean result = secondMax == secondMaxNode.getValue();
			if (!result) {
				System.out.println(numbers);
				System.out.println(secondMaxNode.getValue());
			}
			assertTrue(result);
		}
	}

}
