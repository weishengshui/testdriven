package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import org.junit.Test;

public class OrderProcessorTest {

	@Test
	public void testOrderProcessorWithMockObject() {
		float initialBalance = 100.0f;
		float listPrice = 30.0f;
		float discount = 10.0f;
		float expectedBalance = initialBalance
				- (listPrice * (1 - discount / 100));
		Customer customer = new Customer(initialBalance);
		Product product = new Product("TDD in Action", listPrice);
		OrderProcessor op = new OrderProcessor();
		PricingService pricingService = new PricingServiceTestDouble(discount);
		op.setPricingService(pricingService);
		op.process(new Order(customer, product));
		assertEquals(expectedBalance, customer.getBalance(), 0.001f);
	}

}
