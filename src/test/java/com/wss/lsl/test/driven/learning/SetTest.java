package com.wss.lsl.test.driven.learning;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class SetTest {

	private Set<String> set = new HashSet<String>();

	@Before
	public void before() {
		set.add("1");
		set.add("2");
		set.add("3");
		set.add("4");
	}

	@Test(expected = ConcurrentModificationException.class)
	public void testForRemove() {
		for (String s : set) {
			set.remove(s);
		}
	}

	@Test(expected = ConcurrentModificationException.class)
	public void testForRemove2() {
		synchronized (this) {
			for (String s : set) {
				set.remove(s);
			}
		}
	}

	@Test(expected = ConcurrentModificationException.class)
	public void testListForRemove() {
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		for (String s : list) {
			list.remove(s);
		}
	}

}
