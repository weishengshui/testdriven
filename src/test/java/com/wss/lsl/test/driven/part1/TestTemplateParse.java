package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class TestTemplateParse {

	@Test
	public void emptyTemplateRenderAsEmptyString() {
		List<Segment> segments = parse("");
		assertSegments(segments, new PlainText(""));
	}

	@Test
	public void templateWithOnlyPlanText() {
		List<Segment> segments = parse("plan text");
		assertSegments(segments, new PlainText("plan text"));
	}

	@Test
	public void parseMultipleVariables() {
		List<Segment> segments = parse("${a}:${b}:${c}");
		assertSegments(segments, new Variable("a"), new PlainText(":"),
				new Variable("b"), new PlainText(":"), new Variable("c"));
	}

	private void assertSegments(List<? extends Object> actual,
			Object... expected) {

		assertEquals("Number of segments", expected.length, actual.size());
		assertEquals(Arrays.asList(expected), actual);
	}

	private List<Segment> parse(String templateText) {
		return new TemplateParse().parseSegments(templateText);
	}
}
