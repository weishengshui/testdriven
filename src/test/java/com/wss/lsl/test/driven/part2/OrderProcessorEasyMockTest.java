package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import org.easymock.EasyMock;
import org.junit.Test;

public class OrderProcessorEasyMockTest {

	@Test
	public void testOrderProcessorWithEasyMock() {
		float initialBalance = 100.0f;
		float listPrice = 30.0f;
		float discount = 10.0f;
		float expectedBalance = initialBalance
				- (listPrice * (1 - discount / 100));
		Customer customer = new Customer(initialBalance);
		Product product = new Product("TDD in Action", listPrice);
		OrderProcessor op = new OrderProcessor();

		// 为PricingService创建动态模拟对象
		PricingService mock = EasyMock.createMock(PricingService.class);
		EasyMock.expect(mock.getDiscountPercentage(customer, product))
				.andReturn(discount);
		EasyMock.replay(mock);

		// 将模拟对象传递给待测对象
		op.setPricingService(mock);
		op.process(new Order(customer, product));
		assertEquals(expectedBalance, customer.getBalance(), 0.001f);

		// 让模拟对象自行验证
		EasyMock.verify(mock);
	}

}
