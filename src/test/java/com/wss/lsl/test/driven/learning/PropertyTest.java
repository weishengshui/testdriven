package com.wss.lsl.test.driven.learning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.Test;

public class PropertyTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test() throws FileNotFoundException, IOException {
		Properties p = new Properties();
		p.setProperty("name", "wss");
		p.setProperty("age", "28");
		
		p.storeToXML(new FileOutputStream(new File("/tmp/pro_xml.xml")), null);
		
		p.storeToXML(new FileOutputStream(new File("/tmp/pro_xml_comment.xml")), "这是注释");
	}

}
