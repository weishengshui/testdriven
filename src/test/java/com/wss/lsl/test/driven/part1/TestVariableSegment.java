package com.wss.lsl.test.driven.part1;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.wss.lsl.test.driven.exception.MissingValueException;

/**
 * 测试变量片段
 * 
 * @author Administrator
 * 
 */
public class TestVariableSegment {

	private Map<String, String> variables;

	@Before
	public void setUp() {
		variables = new HashMap<String, String>();
	}

	@Test
	public void variableEvaluateAsIteValue() {
		variables.put("one", "1");
		assertEquals("1", new Variable("one").evaluate(variables));
	}

	@Test
	public void missingVariableRaisesException() {
		String name = "myvar";
		try {
			new Variable(name).evaluate(variables);
			fail("Missing variable value should raise an exception");
		} catch (MissingValueException e) {
			assertEquals("No value for ${" + name + "}", e.getMessage());
		}

	}
}
