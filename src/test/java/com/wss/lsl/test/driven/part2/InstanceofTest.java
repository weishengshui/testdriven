package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import org.junit.Test;

import com.wss.lsl.test.driven.part2.entity.Person;

public class InstanceofTest {

	@Test
	public void testObjectNull() {
		Person person = null;
		assertFalse(person instanceof Person);
		assertNull(person);
	}

}
