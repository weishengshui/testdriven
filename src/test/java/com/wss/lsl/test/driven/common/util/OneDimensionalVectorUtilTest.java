package com.wss.lsl.test.driven.common.util;

import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OneDimensionalVectorUtilTest {

	private Logger LOG = LoggerFactory.getLogger(getClass());

	private static final int TEST_ARRAY_COUNT = 10000;// 测试数组的数量
	private static final int TEST_ARRAY_SIZE = 1000;// 测试数组的大小

	private String str;
	private Random random = new Random();

	@Before
	public void before() {
		str = RandomStringUtils.randomAscii(10);
		for (int i = 1; i < str.length(); i++) {
			char[] chars = str.toCharArray();
			loopMove(chars, i);
			chars = str.toCharArray();
			recursionMove(chars, i);
		}
	}
	
	private void assertResult(char[] temp, int i) {
		char[] expecteds = (str.substring(i) + str.substring(0, i))
				.toCharArray();
		Assert.assertArrayEquals(i + " is not ok", expecteds, temp);
	}

	@Test
	public void testLoopMove() {

		// 循环1W个char数组
		int loopCount = TEST_ARRAY_COUNT;
		int count = 0;
		while (loopCount-- > 0) {
			LOG.info("loopCount={}", loopCount);
			str = RandomStringUtils.randomAscii(2 + random
					.nextInt(TEST_ARRAY_SIZE));
			for (int temp = 1; temp < 10; temp++) {
				LOG.info("count={}", ++count);
				char[] chars = str.toCharArray();
				int i = random.nextInt(chars.length - 1) + 1;
				loopMove(chars, i);
			}
		}
	}

	private void loopMove(char[] chars, int i) {
		char[] temp = OneDimensionalVectorUtil.loopMove(chars, i);

		assertResult(temp, i);
	}

	@Test
	public void testRecursionMove() {

		// 循环1W个char数组
		int loopCount = TEST_ARRAY_COUNT;
		int count = 0;
		while (loopCount-- > 0) {
			LOG.info("recursionMove={}", loopCount);
			str = RandomStringUtils.randomAscii(2 + random
					.nextInt(TEST_ARRAY_SIZE));
			for (int temp = 1; temp < 10; temp++) {
				LOG.info("count={}", ++count);
				char[] chars = str.toCharArray();
				int i = random.nextInt(chars.length - 1) + 1;
				recursionMove(chars, i);
			}
		}
	}

	private void recursionMove(char[] chars, int i) {
		char[] temp = OneDimensionalVectorUtil.recursionMove(chars, i,
				chars.length);

		assertResult(temp, i);
	}

	@Test
	public void testReversalMove() {
		// 循环1W个char数组
		int loopCount = TEST_ARRAY_COUNT;
		int count = 0;
		while (loopCount-- > 0) {
			LOG.info("recursionMove={}", loopCount);
			str = RandomStringUtils.randomAscii(2 + random
					.nextInt(TEST_ARRAY_SIZE));
			for (int temp = 1; temp < 10; temp++) {
				LOG.info("count={}", ++count);
				char[] chars = str.toCharArray();
				int i = random.nextInt(chars.length - 1) + 1;
				reversalMove(chars, i);
			}
		}
	}
	
	private void reversalMove(char[] chars, int i) {
		char[] temp = OneDimensionalVectorUtil.reversalMove(chars, i);

		assertResult(temp, i);
	}

}
