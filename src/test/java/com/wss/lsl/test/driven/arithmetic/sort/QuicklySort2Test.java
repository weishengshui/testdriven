package com.wss.lsl.test.driven.arithmetic.sort;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

/**
 * 测试快排2
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public class QuicklySort2Test extends SortBaseTest {

	@Before
	public void before() {
		super.before();
		for (int i = 0; i < arrayCount; i++) {
			sort[i] = new QuicklySort2<Integer>(data[i]);
		}
	}

	@Test
	public void testSort() {
		for (int i = 0; i < arrayCount; i++) {
			sort[i].sort();
			sort[i].validate();
			System.out.println(i);
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
