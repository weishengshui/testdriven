package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.mockobjects.sql.MockSingleRowResultSet;
import com.wss.lsl.test.driven.part2.entity.Person;
import com.wss.lsl.test.driven.part2.mapper.PersonRowMapper;

public class PersonRowMapperTest {

	
	@Test
	public void testMappingRow() throws SQLException {
		Person expected = new Person("John", "Doe");
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("first_name", expected.getFirstname());
		data.put("last_name", expected.getLastname());
		MockSingleRowResultSet rs = new MockSingleRowResultSet();
		rs.addExpectedNamedValues(data);
		
		assertEquals(expected, new PersonRowMapper().mapRow(rs, 1));
	}

}
