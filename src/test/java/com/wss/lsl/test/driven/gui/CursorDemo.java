package com.wss.lsl.test.driven.gui;

import java.awt.Cursor;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class CursorDemo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton b1 = new JButton("button1");
	
	public CursorDemo() {
		super("自定义光标图形demo");
		setSize(300, 400);
		// b1.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		b1.setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		setLayout(new FlowLayout());
		
		add(b1);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new CursorDemo();
			}
		});
	}
	
}
