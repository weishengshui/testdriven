package com.wss.lsl.test.driven.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.junit.Assert;
import org.junit.Test;

public class ThreadLocalTest {

	@Test
	public void testThreadLocal() throws InterruptedException,
			BrokenBarrierException {
		int numberOfThread = 100;
		CyclicBarrier entryBarrier = new CyclicBarrier(1 + numberOfThread);
		CyclicBarrier exitBarrier = new CyclicBarrier(1 + numberOfThread);

		SomeServiceImpl someServiceImpl = new SomeServiceImpl();
		for (int i = 0; i < numberOfThread; i++) {
			new Thread(new CrmSaleJob(someServiceImpl, "" + i, entryBarrier,
					exitBarrier)).start();
		}

		entryBarrier.await();
		exitBarrier.await();

	}

}

/**
 * 模拟service， 其中有ThreadLocal变量
 * 
 * @author Administrator
 * 
 */
class SomeServiceImpl {

	private ThreadLocal<Map<String, String>> parameters = new ThreadLocal<Map<String, String>>();

	public void setParameters(Map<String, String> parameters) {
		this.parameters.set(parameters);
	}

	public void print() {
		for (Map.Entry<String, String> entry : parameters.get().entrySet()) {
			System.out.println(entry.getKey() + "=" + entry.getValue());
		}
	}

	public String get(String key) {
		return parameters.get().get(key);
	}

	public Map<String, String> get() {
		return parameters.get();
	}

}

class CrmSaleJob implements Runnable {

	private Map<String, String> parameters;
	private SomeServiceImpl someServiceImpl;
	private String value;
	private CyclicBarrier entryBarrier;
	private CyclicBarrier exitBarrier;

	public CrmSaleJob(SomeServiceImpl someServiceImpl, String value,
			CyclicBarrier entryBarrier, CyclicBarrier exitBarrier) {
		super();
		this.someServiceImpl = someServiceImpl;
		this.value = value;
		this.entryBarrier = entryBarrier;
		this.exitBarrier = exitBarrier;
		this.parameters = new HashMap<String, String>();
		this.parameters.put("time", this.value);
	}

	@Override
	public void run() {
		try {
			entryBarrier.await();
			someServiceImpl.setParameters(parameters);
			Assert.assertEquals(this.value, someServiceImpl.get("time"));
			Assert.assertEquals(1, someServiceImpl.get().size());
			exitBarrier.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}

	public Map<String, String> get() {
		return someServiceImpl.get();
	}

}
