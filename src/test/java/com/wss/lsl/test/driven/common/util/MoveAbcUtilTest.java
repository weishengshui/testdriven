package com.wss.lsl.test.driven.common.util;

import java.util.Arrays;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoveAbcUtilTest {

	private Logger LOG = LoggerFactory.getLogger(getClass());

	private static final int TEST_ARRAY_COUNT = 10000;// 测试数组的数量
	private static final int TEST_ARRAY_SIZE = 1000;// 测试数组的大小

	private String str;
	private Random random = new Random();

	@Before
	public void before() {
		// str = RandomStringUtils.randomAscii(10);
		// int a, b;
		// int half = str.length() / 2;
		// for (int i = 1, length = str.length(); i < length; i++) {
		// LOG.info("before i={}", i);
		// char[] chars = str.toCharArray();
		// a = 1 + random.nextInt(half);
		// b = a + random.nextInt(half);
		// if (b == a) {
		// b = a + 1;
		// }
		// char[] temp = MoveAbcUtil.loopMove(chars, a, b);
		// assertResult(temp, a, b);
		// }
	}

	// 测试循环移动中间部分
	@Test
	public void testLoopMoveMiddle() {

		int size = 10;
		char[] chars = new char[size];
		for (int i = 0; i < size; i++) {
			chars[i] = (i + "").toCharArray()[0];
		}

		int begin = 2, i = 5, n = 6;
		MoveAbcUtil.loopMove(chars, begin, i, n);
		System.out.println(String.format("%s", Arrays.toString(chars)));
	}

	// 测试递归移动
	@Test
	public void testRecursionMove() {

		// 循环1W个char数组
		int loopCount = TEST_ARRAY_COUNT;
		int count = 0;
		while (loopCount-- > 0) {
			LOG.info("loopCount={}", loopCount);
			str = RandomStringUtils.randomAscii(3 + random
					.nextInt(TEST_ARRAY_SIZE));
			int a, b;
			int half = str.length() / 2;
			for (int i = 1; i < 10; i++) {
				LOG.info("count={}", ++count);
				char[] chars = str.toCharArray();
				a = 1 + random.nextInt(half);
				b = a + random.nextInt(half);
				if (b == a) {
					b = a + 1;
				}
				char[] temp = MoveAbcUtil.recursionMove(chars, a, b);
				assertResult(temp, a, b);
			}
		}

	}

	// 测试递归移动法
	@Test
	public void testRecursionMove2() {
		int size = 10;
		char[] chars = new char[size];
		for (int i = 0; i < size; i++) {
			chars[i] = (i + "").toCharArray()[0];
		}

		int a = 2, b = 8;
		int left = a, n = size;
		MoveAbcUtil.recursionMove2(chars, left, n);
		System.out.println(String.format("%s", Arrays.toString(chars)));

		left = b - a;
		n = size - a;
		MoveAbcUtil.recursionMove2(chars, left, n);
		System.out.println(String.format("%s", Arrays.toString(chars)));
	}

	// 测试循环移动
	@Test
	public void testLoopMove() {

		// 循环1W个char数组
		int loopCount = TEST_ARRAY_COUNT;
		int count = 0;
		while (loopCount-- > 0) {
			LOG.info("loopCount={}", loopCount);
			str = RandomStringUtils.randomAscii(3 + random
					.nextInt(TEST_ARRAY_SIZE));
			int a, b;
			int half = str.length() / 2;
			for (int i = 1; i < 10; i++) {
				LOG.info("count={}", ++count);
				char[] chars = str.toCharArray();
				a = 1 + random.nextInt(half);
				b = a + random.nextInt(half);
				if (b == a) {
					b = a + 1;
				}
				char[] temp = MoveAbcUtil.loopMove(chars, a, b);
				assertResult(temp, a, b);
			}
		}
	}

	// 测试反转法
	@Test
	public void testReversalMove() {
		// 循环1W个char数组
		int loopCount = TEST_ARRAY_COUNT;
		int count = 0;
		while (loopCount-- > 0) {
			LOG.info("loopCount={}", loopCount);
			str = RandomStringUtils.randomAscii(3 + random
					.nextInt(TEST_ARRAY_SIZE));
			int a, b;
			int half = str.length() / 2;
			for (int i = 1; i < 10; i++) {
				LOG.info("count={}", ++count);
				char[] chars = str.toCharArray();
				a = 1 + random.nextInt(half);
				b = a + random.nextInt(half);
				if (b == a) {
					b = a + 1;
				}
				char[] temp = MoveAbcUtil.reversalMove(chars, a, b);
				assertResult(temp, a, b);
			}
		}
	}

	private void assertResult(char[] temp, int a, int b) {
		char[] expecteds = (str.substring(b) + str.substring(a, b) + str
				.substring(0, a)).toCharArray();
		Assert.assertArrayEquals(
				String.format("a=%d and b=%d is not ok", a, b), expecteds, temp);
	}

}
