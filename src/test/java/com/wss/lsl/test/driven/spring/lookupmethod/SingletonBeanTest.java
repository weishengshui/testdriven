package com.wss.lsl.test.driven.spring.lookupmethod;

import org.junit.AfterClass;
import org.junit.Test;

import com.wss.lsl.test.driven.BaseTest;

public class SingletonBeanTest extends BaseTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testPrintLookupMethod() {

		SingletonBean singletonBean = applicationContext.getBean(SingletonBean.class);
		System.out.println(singletonBean);

		singletonBean = applicationContext.getBean(SingletonBean.class);
		System.out.println(singletonBean);

		singletonBean.printLookupMethod();
		singletonBean.printLookupMethod();
	}

}
