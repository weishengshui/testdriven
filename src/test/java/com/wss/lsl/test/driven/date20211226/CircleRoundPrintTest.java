package com.wss.lsl.test.driven.date20211226;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

/**
 * 回旋打印二叉树。奇数层节点，从左往右；偶数层节点，从右往左。
 * 
 * @author weiss
 *
 */
public class CircleRoundPrintTest {
	
	private SortedBtree root;
	private Random random = new Random();
	int count = 1000;
	List<Integer> numbers;

	@Before
	public void setUp() {
		
		count = 20;
		numbers = new ArrayList<>(count);
		while (count-- > 0) {
			numbers.add(random.nextInt(100));
		}

		// 初始化排序二叉树
		root = new SortedBtree();
		root.setValue(numbers.get(0));
		for (int i = 1; i < numbers.size(); i++) {
			root.initSortedBtree(root, numbers.get(i));
		}
	}
	
	@Test
	public void test() {
		
		root.circleRoundPrint();
		
		System.out.println();
		System.out.println("原序列数字==========");
		System.out.println(numbers);
	}

}
