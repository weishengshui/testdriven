package com.wss.lsl.test.driven.redis;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

public class RedisTemplateTest {

	private RedisTemplate redisTemplate;
	private JedisPool jedisPool;

	private static final int THREAD_COUNT = 50;
	// 线程池模拟竞争
	private ExecutorService executorService = Executors
			.newFixedThreadPool(THREAD_COUNT);

	@Before
	public void setup() throws Exception {
		// String host = "192.168.1.150";
		// int port = 6379;

		String host = "172.17.210.80";
		int port = 6369;

		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(500);

		jedisPool = new JedisPool(poolConfig, host, port);

		redisTemplate = new RedisTemplate(jedisPool);
	}
	
	@Test
	public void testCopySortSetRangeToSet() {

		String sortSetKey = "sort1", setKey = "s2";
		int min = 4, max = Integer.MAX_VALUE;
		redisTemplate.copySortSetRangeToSet(sortSetKey, min, max, setKey);
	}

	@After
	public void teardown() throws Exception {
		System.out.println("teardown");
	}

	@Test
	public void testAcquireLock() {

		String key = "locked_key";
		// 每次设定唯一的值
		int lockTime = 100;
		int waitTime = 101;
		String value = redisTemplate.acquireLock(key, lockTime, waitTime);
		// 校验锁定成功
		Assert.assertTrue(value != null);
		// redisTemplate.releaseLock(key, value);
		redisTemplate.releaseLockWithLua(key, value);

		waitTime = 5;
		value = redisTemplate.acquireLock(key, lockTime, waitTime);
		// 校验锁定成功
		Assert.assertTrue(value != null);
		// redisTemplate.releaseLock(key, value);
		redisTemplate.releaseLockWithLua(key, value);
	}

	@Test
	public void testReleaseLock() throws Exception {

		final CyclicBarrier cb = new CyclicBarrier(THREAD_COUNT + 1);
		final Random random = new Random();
		final AtomicInteger ai = new AtomicInteger();

		// 50个线程模拟竞争条件
		for (int i = 0; i < THREAD_COUNT; i++) {
			executorService.execute(new Runnable() {
				@Override
				public void run() {
					String key = "locked_key";
					String value = null;
					int lockTime = 10;
					int waitTime = 11;
					try {
						cb.await();

						value = redisTemplate.acquireLock(key, lockTime,
								waitTime);

						System.out.println(ai.getAndIncrement());
						if (value == null) {
							// 没有获取到分布式锁
							return;
						}
						System.out.println("线程："
								+ Thread.currentThread().getName()
								+ " 获取到了分布式锁 ");
						System.out.println(new Date());

						// 模拟业务执行时间
						TimeUnit.MILLISECONDS.sleep(5000 + random
								.nextInt(10000));

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						if (value != null) {
							redisTemplate.releaseLockWithLua(key, value);
							System.out.println("线程："
									+ Thread.currentThread().getName()
									+ " 释放了分布式锁 ");
							System.out.println(new Date());
						}
					}
				}
			});
		}

		cb.await();

		System.in.read();
	}

	/**
	 * 比较流水线和普通方式执行命令的效率
	 * 
	 * @author wei.ss
	 */
	@Test
	public void testPipeline() {
		Jedis jedis = jedisPool.getResource();

		long begin = System.currentTimeMillis();
		Pipeline pipeline = jedis.pipelined();
		int count = 10000;

		//
		for (int i = 0; i < count; i++) {
			pipeline.set(i + "", "" + i);
		}
		System.out.println((System.currentTimeMillis() - begin));
		pipeline.sync();
		// System.out.println(response);

		begin = System.currentTimeMillis();
		for (int i = 0; i < count; i++) {
			jedis.set(i + "", "" + i);
		}
		System.out.println((System.currentTimeMillis() - begin));

		// pipeline.exec();
		// pipeline.sync();
	}

}
