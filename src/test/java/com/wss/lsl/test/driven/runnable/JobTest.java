package com.wss.lsl.test.driven.runnable;

import org.junit.Before;
import org.junit.Test;

public class JobTest {

	private Job job;

	@Before
	public void before() {
		job = new Job();
	}

	@Test
	public void testRun() {
		int threadCount = 10;
		job.run(threadCount);
	}

}
