package com.wss.lsl.test.driven.common;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixUtilTest {

	private int n = 4;

	private int[][] matrix;

	@Before
	public void setUp() {
		matrix = new int[n][n];
	}

	/**
	 * 测试矩阵都是正数时
	 */
	@Test
	public void testCalcMaxSumOfSubMatrixByAllPositiveNumber() {
		int index = 1;
		for (int i = 0, rowLength = matrix.length; i < rowLength; i++) {
			for (int j = 0, cellLength = matrix[i].length; j < cellLength; j++) {
				matrix[i][j] = index++;
			}
		}
		int result = MatrixUtil.calcMaxSumOfSubMatrix(matrix);

		assertEquals((n * n + 1) * (n * n) / 2, result);
	}

}
