package com.wss.lsl.test.driven.util;

import java.io.IOException;

import com.pdfcrowd.Pdfcrowd;

public class PdfcrowdUtil {

	public static void main(String[] args) throws IOException, Pdfcrowd.Error {
		String url = "https://treport.1hykj.com/persionalBeforeLoanReport?id=8a2831786d24c24e016d24c24e9b0000";
		try {
			// create the API client instance
			Pdfcrowd.HtmlToPdfClient client = new Pdfcrowd.HtmlToPdfClient("demo", "ce544b6ea52a5621fb9d55f8b542d14d");
			client.setPageWidth("35cm");
			
			// run the conversion and write the result to a file
			client.convertUrlToFile(url, "d:\\example.pdf");
		} catch (Pdfcrowd.Error why) {
			// report the error
			System.err.println("Pdfcrowd Error: " + why);

			// handle the exception here or rethrow and handle it at a higher level
			throw why;
		} catch (IOException why) {
			// report the error
			System.err.println("IO Error: " + why.getMessage());

			// handle the exception here or rethrow and handle it at a higher level
			throw why;
		}

		System.out.println("Done!");
	}
}
