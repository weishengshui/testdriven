package com.wss.lsl.test.driven.serial;



import java.io.*;

public class A implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int age;
	
	public static int age2 = 4;
	
	public A(){
		
	}
	
	public A(int age){
		this.age = age;
	}
	
	public void setAge(int age){
		this.age = age;
	}
	
	public int getAge(){
		return this.age;
	}
	
}
