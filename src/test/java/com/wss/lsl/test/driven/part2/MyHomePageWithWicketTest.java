package com.wss.lsl.test.driven.part2;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import com.wss.lsl.test.driven.part2.page.LoginPage;
import com.wss.lsl.test.driven.part2.page.MyHomePage;

public class MyHomePageWithWicketTest {

	@Test
	public void homePageHasWelcomeText() {

		WicketTester tester = new WicketTester();
		tester.startPage(MyHomePage.class);

		// 检查渲染错误
		tester.assertRenderedPage(MyHomePage.class);
		tester.assertNoErrorMessage();

		// 判断页面内容
		tester.assertContains("Welcome to the home page");
	}

	@Test
	public void homePaheHasLinkToLoginPage() {
		WicketTester tester = new WicketTester();
		tester.startPage(MyHomePage.class);

		tester.assertRenderedPage(MyHomePage.class);
		tester.assertNoErrorMessage();

		tester.assertLabel("welcomeMessage", "Welcome to the home page");
		tester.assertBookmarkablePageLink("linkToLoginPage", LoginPage.class,
				new PageParameters());
	}
	
	@Test
	public void interactingWithComponents() {
		WicketTester tester = new WicketTester();
		tester.startPage(MyHomePage.class);
		tester.assertRenderedPage(MyHomePage.class);
		tester.clickLink("linkToLoginPage");
		tester.assertRenderedPage(LoginPage.class);
	}

}
