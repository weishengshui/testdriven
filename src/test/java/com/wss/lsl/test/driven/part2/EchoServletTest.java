package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

public class EchoServletTest {

	@Test
	public void testEchoingServletWithMultipleVaules() throws ServletException,
			IOException {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("name", "张三");
		request.addParameter("age", "36");
		request.addParameter("sex", "男");
		MockHttpServletResponse response = new MockHttpServletResponse();

		EchoServlet echo = new EchoServlet();
		echo.doGet(request, response);
		String[] lines = response.getContentAsString().split("\n");
		assertEquals("Expected as many lines as we have parameter values", 3,
				lines.length);
		assertEquals("name=张三", lines[0]);
		assertEquals("age=36", lines[1]);
		assertEquals("sex=男", lines[2]);
	}

}
