package com.wss.lsl.test.driven.part2;

import org.junit.Test;

import com.wss.lsl.test.driven.common.Constants;

public class LoginTemplateTest extends VelocityTestCase {

	@Override
	protected String getWebRoot() {
		return "src/main/webapp/WEB-INF/views/vtl";
	}

	@Test
	public void previousUsernameIsRetained() throws Exception {
		String previousUsername = "bob";
		setAttribute(Constants.USERNAME_PARAMETER, previousUsername);
		render("login.vtl");
		assertFormFieldValue(Constants.USERNAME_PARAMETER, previousUsername);

		// 测试中文
		previousUsername = "水哥";
		setAttribute(Constants.USERNAME_PARAMETER, previousUsername);
		render("login.vtl");
		assertFormFieldValue(Constants.USERNAME_PARAMETER, previousUsername);
	}
}
