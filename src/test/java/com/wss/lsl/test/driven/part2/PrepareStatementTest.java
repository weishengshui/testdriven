package com.wss.lsl.test.driven.part2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.Assert;

/**
 * 
 * @author sean
 * 
 */
public class PrepareStatementTest extends DbUnitIntegrationTestCase {

	public void testNameParameterAndPositionParameter()
			throws ClassNotFoundException, SQLException {
		Connection connection = getJdbcConnection();
		String sql = "insert into crm_user(first_name, last_name) values(?, ?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		
		preparedStatement.setString(1, "zhangsan");
		preparedStatement.setString(2, "lisi");
		
		Assert.assertEquals(1, preparedStatement.executeUpdate());
	}
}
