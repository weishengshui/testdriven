package com.wss.lsl.test.driven.learning;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

/**
 * 日期测试
 * 
 * @author wei.ss
 * @date 2017年6月1日
 * @copyright wonhigh.cn
 */
public class DateTest {
	/**
	 * 常量：一天的开始(00:00:00:000)
	 */
	public static final int DATE_TIME_BEGIN = 1;
	/**
	 * 常量：一天的结束(23:59:59:999)
	 */
	public static final int DATE_TIME_END = 2; 
	/**
	 * 本周
	 */
	public static final int DATE_TYPE_THIS_WEEK = 1;
	/**
	 * 上周
	 */
	public static final int DATE_TYPE_LAST_WEEK = 2;
	/**
	 * 本月
	 */
	public static final int DATE_TYPE_THIS_MONTH = 3;
	/**
	 * 上月
	 */
	public static final int DATE_TYPE_LAST_MONTH = 4;
	
	@Test
	public void test() {
		System.out.println(getDayBeginOfWeek());
		System.out.println(getDayBeginOfLastWeek());
		System.out.println(getDayEndOfLastWeek());
	}
	
	@Test
	public void test2() {
		System.out.println(getDate(DATE_TYPE_THIS_WEEK, DATE_TIME_BEGIN));
		System.out.println(getDate(DATE_TYPE_THIS_WEEK, DATE_TIME_END));
		
		System.out.println();
		System.out.println(getDate(DATE_TYPE_LAST_WEEK, DATE_TIME_BEGIN));
		System.out.println(getDate(DATE_TYPE_LAST_WEEK, DATE_TIME_END));
		
		System.out.println();
		System.out.println(getDate(DATE_TYPE_THIS_MONTH, DATE_TIME_BEGIN));
		System.out.println(getDate(DATE_TYPE_THIS_MONTH, DATE_TIME_END));
		
		System.out.println();
		System.out.println(getDate(DATE_TYPE_LAST_MONTH, DATE_TIME_BEGIN));
		System.out.println(getDate(DATE_TYPE_LAST_MONTH, DATE_TIME_END));
	}

	// 本周开始时间
	private Date getDayBeginOfWeek() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return c.getTime();
	}

	// 上周开始时间
	private Date getDayBeginOfLastWeek() {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.add(Calendar.DAY_OF_MONTH, -7);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return c.getTime();
	}

	// 上周结束时间
	private Date getDayEndOfLastWeek() {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.add(Calendar.DAY_OF_MONTH, -7);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return c.getTime();
	}
	
	public static Date getDate(int dateType, int timeType){
		Calendar c = Calendar.getInstance();
		c.setTime(getNextDay(new Date(), 0, timeType));
		switch (dateType) {
		case DATE_TYPE_THIS_WEEK:
			c.setFirstDayOfWeek(Calendar.MONDAY);
			if (timeType == DATE_TIME_BEGIN) { // 本周开始
				c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			} else if (timeType == DATE_TIME_END) { // 本周结束
				c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			}
			break;
		case DATE_TYPE_LAST_WEEK:
			c.setFirstDayOfWeek(Calendar.MONDAY);
			c.add(Calendar.DAY_OF_MONTH, -7);
			if (timeType == DATE_TIME_BEGIN) { // 上周开始
				c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			} else if (timeType == DATE_TIME_END) {// 上周结束
				 c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			}
			break;
		case DATE_TYPE_THIS_MONTH:
			if (timeType == DATE_TIME_BEGIN) { // 当月开始
				c.set(Calendar.DAY_OF_MONTH, 1);
			} else if (timeType == DATE_TIME_END) { // 当月结束
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			break;
		case DATE_TYPE_LAST_MONTH:
			c.add(Calendar.MONTH, -1);
			if (timeType == DATE_TIME_BEGIN) { // 上月开始
		        c.set(Calendar.DAY_OF_MONTH, 1);
			} else if (timeType == DATE_TIME_END) { // 上月结束
				c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			break;
		default:
			break;
		}

		return c.getTime();
	}
	
	public static Date getNextDay(Date date, int n, int timeType) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, n);
		if (timeType == DATE_TIME_BEGIN) {
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		} else if (timeType == DATE_TIME_END) {
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 999);
		}
		return cal.getTime();
	}
	
	@Test
	public void test3(){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -90);
		System.out.println(cal.getTime());
	}

}
