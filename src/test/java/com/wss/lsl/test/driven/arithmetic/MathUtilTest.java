package com.wss.lsl.test.driven.arithmetic;

import org.junit.Assert;
import org.junit.Test;

/**
 * 数学算术工具测试
 * 
 * @author sean
 *
 */
public class MathUtilTest {

	@Test
	public void testExp() {

		// 测试非法参数
		int x = 10, n = -1;
		try {
			// 非法参数，要抛出异常
			MathUtil.exp(x, n);
			Assert.fail();
		} catch (Exception e) {
			// ingore
		}

		// 测试临界值
		n = 0;
		Assert.assertEquals(1, MathUtil.exp(x, n));

		// 测试奇数次幂
		n = 5;
		Assert.assertEquals(Math.pow(x, n), MathUtil.exp(x, n), 0);

		// 测试偶数次幂
		n = 2;
		Assert.assertEquals(Math.pow(x, n), MathUtil.exp(x, n), 0);
		
		n = 6;
		Assert.assertEquals(Math.pow(x, n), MathUtil.exp(x, n), 0);
		
		n = 8;
		Assert.assertEquals(Math.pow(x, n), MathUtil.exp(x, n), 0);
		
		n = 9;
		Assert.assertEquals(Math.pow(x, n), MathUtil.exp(x, n), 0);
		
		x=2; n = 30;
		Assert.assertEquals(Math.pow(x, n), MathUtil.exp(x, n), 0);
	}

}
