package com.wss.lsl.test.driven.learning;

@ExtractInterface("IMultiply")
public class Multiply {

	public int multiply(int x, int y) {

		return x * y;
	}
}
