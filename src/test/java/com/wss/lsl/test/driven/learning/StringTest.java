package com.wss.lsl.test.driven.learning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.wss.lsl.test.driven.util.UUIDHexGenerator;

public class StringTest {

	private final ExecutorService executors = Executors
			.newSingleThreadExecutor();

	@Test
	public void test() {
		String s = "?";
		assertTrue("?".equals(s));
		assertFalse(Pattern.matches("[0-9]+", s));
		s = "00025555";
		assertTrue(Pattern.matches("[0-9]+", s));
		s = "3400.220025";
		assertTrue(Pattern.matches("[0-9]+.?[0-9]*", s));
		s = "3400";
		assertTrue(Pattern.matches("[0-9]+.?[0-9]*", s));
	}

	@Test
	public void testFormat() {
		String nickName = "水哥";
		String message = "%1$s，你好";
		Assert.assertEquals("水哥，你好", String.format(message, nickName));

		nickName = "水哥\"";
		Assert.assertEquals("水哥\"，你好", String.format(message, nickName));

		nickName = "水哥\"\"";
		Assert.assertEquals("水哥\"\"，你好", String.format(message, nickName));

		nickName = "水哥\'";
		Assert.assertEquals("水哥\'，你好", String.format(message, nickName));

		nickName = "水哥\'\'";
		Assert.assertEquals("水哥\'\'，你好", String.format(message, nickName));

		nickName = null;
		Assert.assertEquals("null，你好", String.format(message, nickName));
	}

	@Test
	public void test2() {
		int count = 3;
		while (count-- > 0) {
			System.out.println(count);
		}
	}

	@Test
	public void test3() {
		System.out.println(new HashMap<String, String>().entrySet());
	}

	@Test
	public void test4() {
		String xxx = "xxx&shopOrgCode=%1$s";
		xxx = xxx.replace("&shopOrgCode=%1$s", "");
		Assert.assertTrue("xxx".equals(xxx));
	}

	@Test
	public void test5() {
		String x = "sasasa";
		x = String.format(x, "sasas");
		Assert.assertTrue("sasasa".equals(x));
	}

	@Test
	public void test6() {
		String a = "1|2|3";
		Assert.assertArrayEquals(new String[] { "1", "2", "3" }, a.split("\\|"));
	}

	@Test
	public void test7() {
		String a = "\n";
		Assert.assertTrue(a.length() == 1);
	}

	@Test
	public void test8() {
		String a = "\\";
		System.out.println(a.replaceAll("\\\\", "\\\\\\\\"));
	}

	@Test
	public void test9() {
		String a = "%%2Fwx\\%%2Fauth.do\\%%3Fid\\%%3D%1$s";
		System.out.println(String.format(a, "1233"));

		final String WECHAT_MENU_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%1$s&redirect_uri=%2$s/"
				+ "%%2Fwx%%2Fauth.do%%3Fid%%3D"
				+ "%3$s"
				+ "%%26type%%3D"
				+ "%4$s"
				+ "&response_type=code&scope=snsapi_base&state=123#wechat_redirect";
		System.out.println(String.format(WECHAT_MENU_URL, "1", "2", "3", "4"));
	}

	@Ignore
	@Test
	public void test10() {
		String str = "12345";
		str = str.replaceAll("[\001]", "");
		Assert.assertEquals("2345", str);
	}

	@Test
	public void test11() {
		String str = "..12345";
		str = str.replaceAll("\\.", ""); // 替换点
		Assert.assertEquals("12345", str);
	}

	@Test
	public void test12() {
		String a = "asas asa  sasasa  qwqwsasayuxzc asasasa asasa";
		String[] array = a.split("\\s+");
		Assert.assertEquals(6, array.length);
	}

	@Test
	public void test13() throws Exception {
		// URDEncoder.encode()之后的字符串，不区分大小写
		String str = "%e6%b0%b4%e5%93%a5";
		String expected = "水哥";
		System.out.println(URLEncoder.encode(expected, "UTF-8"));

		System.out.println(URLDecoder.decode(str, "UTF-8"));
	}

	public void testFetchOpenId() {
		String fromKey = "fromOpenId=";
		String sourceKey = "sourceOpenId=";

		String homePageUrl = "we_display/w_index.do?openid=12122&shopOrgCode=wqwqsaa&"
				+ "brandCode=asasas&followerShopOrgCode=sazxczczc&fromOpenId=sasdasad&"
				+ "sourceOpenId=saaszxz&parShareID=saazxzxz";
		String fromopenid = fetchOpenId(homePageUrl, fromKey);
		String sourceOpenid = fetchOpenId(homePageUrl, sourceKey);
		Assert.assertEquals("sasdasad", fromopenid);
		Assert.assertEquals("saaszxz", sourceOpenid);

		homePageUrl = "we_display/w_index.do?openid=12122&shopOrgCode=wqwqsaa&"
				+ "brandCode=asasas&followerShopOrgCode=sazxczczc&fromOpenId=sasdasad&"
				+ "&parShareID=saazxzxzsourceOpenId=saaszxz";
		fromopenid = fetchOpenId(homePageUrl, fromKey);
		sourceOpenid = fetchOpenId(homePageUrl, sourceKey);
		Assert.assertEquals("sasdasad", fromopenid);
		Assert.assertEquals("saaszxz", sourceOpenid);

		String productDetailUrl = "we_display/productDetail.do?openid=12122&shopOrgCode=wqwqsaa&"
				+ "brandCode=asasas&followerShopOrgCode=sazxczczc&fromOpenId=12121sasdasad&"
				+ "&parShareID=saazxzxzsourceOpenId=saaszxzsasass33";
		fromopenid = fetchOpenId(productDetailUrl, fromKey);
		sourceOpenid = fetchOpenId(productDetailUrl, sourceKey);
		Assert.assertEquals("12121sasdasad", fromopenid);
		Assert.assertEquals("saaszxzsasass33", sourceOpenid);
	}

	/**
	 * 提取openID
	 * 
	 * @param key
	 * @return
	 */
	private String fetchOpenId(String url, String key) {

		int subStrBegin = url.indexOf(key) + key.length();
		int subStrEnd = url.indexOf("&", subStrBegin);
		if (subStrEnd == -1) {
			subStrEnd = url.length();
		}

		String openId = url.substring(subStrBegin, subStrEnd);

		return openId;
	}

	@Test
	public void testUrl() {
		String url = "http://www.baidu.com/sasa/?asa";

		String domain = getDomainByUrl(url);
		Assert.assertEquals("www.baidu.com", domain);
	}

	// 提取url中的域名
	public String getDomainByUrl(String url) {
		String domain = url.split("//")[1];

		int index = domain.indexOf("/");
		if (index != -1) {
			domain = domain.substring(0, index);
		}

		return domain;
	}

	static abstract class BaseClass {
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	static class SubClass extends BaseClass {
		private String cardNo;

		public String getCardNo() {
			return cardNo;
		}

		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}
	}

	@Test
	public void test14() {
		SubClass subClass = new SubClass();
		subClass.setCardNo("CardNo");
		subClass.setName("Name");

		System.out.println(JSON.toJSONString(subClass));

		BaseClass baseClass = subClass;

		System.out.println(JSON.toJSONString(baseClass));
	}

	static class A implements Serializable {
		/**
		 * TODO:变量描述
		 */
		private static final long serialVersionUID = 1L;
		private String id;
		// private String name;
		@JSONField(format = "yyyy-MM-dd HH:mm")
		private Date sendTime;

		private Date birthday;

		@JSONField(name = "card_no", serialize = true)
		private String cardNo;

		public A(String id) {
			super();
			this.id = id;
		}

		public A() {
		}

		// public A(String id, String name) {
		// super();
		// this.id = id;
		// this.name = name;
		// }

		public Date getSendTime() {
			return sendTime;
		}

		public void setSendTime(Date sendTime) {
			this.sendTime = sendTime;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Date getBirthday() {
			return birthday;
		}

		public void setBirthday(Date birthday) {
			this.birthday = birthday;
		}

		// public String getName() {
		// return name;
		// }
		//
		// public void setName(String name) {
		// this.name = name;
		// }

	}

	@Test
	public void test15() throws Exception {
		A a = new A("zhangsan");

		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				new File("d:/serial/a")));
		oos.writeObject(a);
		oos.close();
	}

	@Test
	public void test16() throws Exception {

		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				new File("d:/serial/a")));

		A a = (A) ois.readObject();
		ois.close();
		System.out.println(JSON.toJSONString(a));
	}

	@Test
	public void test17() {
		byte[] data = { 1, 2, 3, 4 };
		System.out.println(Arrays.toString(data));
	}

	@Test
	public void test18() {
		A a = new A();
		System.out.println(JSON.toJSONString(a));
	}

	@Test
	public void test19() {
		JSONObject jo = new JSONObject();
		jo.put("age", "22");

		Assert.assertEquals(22, jo.getIntValue("age"));
	}

	@Test
	public void test20() {
		String json = "{\"sendTime\":\"2016-12-12 12:00\"}";
		A a = JSON.parseObject(json, A.class);
		String temp = JSON.toJSONString(a);
		Assert.assertEquals(json, temp);
	}

	@Test
	public void test21() {
		String stratrgyNo = null;
		int count = 1000;
		while (count-- > 0) {
			stratrgyNo = RandomStringUtils.random(4, true, true);
			System.out.println(stratrgyNo);
		}
	}

	@Test
	public void test22() {
		String d = String.format("%04d", 1);
		Assert.assertEquals("0001", d);

		d = String.format("%04d", 2);
		Assert.assertEquals("0002", d);

		d = String.format("%04d", 10);
		Assert.assertEquals("0010", d);

		d = String.format("%04d", 9999);
		Assert.assertEquals("9999", d);
	}

	@Test
	public void test23() {
		System.out.println((1 % 2 + 3));
	}

	@Test
	public void test24() {
		char c = (char) 0x14;
		System.out.println(c);

		BigDecimal bg = new BigDecimal(0.5);
		BigDecimal bg2 = bg.setScale(0, BigDecimal.ROUND_HALF_UP);
		System.out.println(bg2.doubleValue());
	}

	@Test
	public void test26() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		// bos.write(123);
		Assert.assertEquals(0, bos.size());
	}

	@Test
	public void test25() {

		for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; i++) {
			addByte((byte) i);
		}
	}

	@Test
	public void test27() {
		List<String> list = new ArrayList<String>();
		int count = 100;
		for (int i = 0; i < count; i++) {
			list.add("" + i);
			if (i % 10 == 0) {
				list.clear();
			}
		}
	}

	@Test
	public void test28() {
		JSONObject jo = new JSONObject();
		jo.put("date", new Date().getTime());

		long sss = 1222222l;
		new Date(sss);
		System.out.println(jo.getString("date"));
	}

	@Test
	public void test29() {
		Calendar cal = Calendar.getInstance();
		System.out.println(cal.get(Calendar.MONTH));
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);
		int lastMonth = cal.get(Calendar.MONTH);
		System.out.println(lastMonth);

		int year = 2017;
		int month = 0;
		String monthFirstDay = getMonthFirstDay(year, month);
		String monthLastDay = getMonthFirstDay(year, month + 1);
		System.out.println(monthFirstDay);
		System.out.println(monthLastDay);
	}

	@Test
	public void test30() {
		A a = new A();
		Date now = new Date();
		a.setBirthday(now);
		JSONObject jo = JSON.parseObject(JSON.toJSONString(a));
		String birthday = jo.getString("birthday");
		System.out.println(birthday);
		System.out.println(now.getTime());
	}

	@Test
	public void test31() {
		System.out.println((6 + 6 % 6));
	}

	@Test
	public void test32() {
		BigDecimal a = new BigDecimal(2.5);
		BigDecimal b = new BigDecimal(2.0);
		a.setScale(2);
		b.setScale(2);
		System.out.println(a.divide(b));

		b = new BigDecimal(1);
		System.out.println(b.doubleValue());
		System.out.println(b.intValue());
	}

	@Test
	public void test33() {
		int count = 10;
		ExecutorService executors = Executors.newSingleThreadExecutor();
		while (count-- > 0) {
			executors.execute(new Runnable() {

				@Override
				public void run() {
					try {
						TimeUnit.SECONDS.sleep(2);
						System.out.println(Thread.currentThread().getName());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		executors.shutdown();
		try {
			System.in.read();
		} catch (IOException e) {
		}
		System.out.println("Done!");
	}

	@Test
	public void testGetLastDate() {
		Date now = new Date();
		Date beginDate = getLastDate(now, 1);
		Date endDate = getLastDate(now, 2);

		System.out.println(String.format("now=%s, beginDate=%s, endDate=%s",
				now, beginDate, endDate));
	}

	private Date getLastDate(Date date, int type) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		if (type == 1) {
			// 取开始时间
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) - 1);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 999);
		} else if (type == 2) {
			// 取结束时间
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) - 30);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		}

		return cal.getTime();
	}

	@Test
	public void testGetAvgCount() {

		int peopleCount = 1, dayCount = 30;
		Assert.assertTrue(0 == getAvgCount(peopleCount, dayCount));

		peopleCount = 14;
		dayCount = 30;
		Assert.assertTrue(0 == getAvgCount(peopleCount, dayCount));

		peopleCount = 15;
		dayCount = 30;
		Assert.assertTrue(1 == getAvgCount(peopleCount, dayCount));

		peopleCount = 29;
		dayCount = 30;
		Assert.assertTrue(1 == getAvgCount(peopleCount, dayCount));

		peopleCount = 30;
		dayCount = 30;
		Assert.assertTrue(1 == getAvgCount(peopleCount, dayCount));

		peopleCount = 31;
		dayCount = 30;
		Assert.assertTrue(1 == getAvgCount(peopleCount, dayCount));

		peopleCount = 44;
		dayCount = 30;
		Assert.assertTrue(1 == getAvgCount(peopleCount, dayCount));

		peopleCount = 45;
		dayCount = 30;
		Assert.assertTrue(2 == getAvgCount(peopleCount, dayCount));
	}

	// 获取平均数：四舍五入
	private int getAvgCount(int peopleCount, int dayCount) {

		BigDecimal avg = new BigDecimal(((double) peopleCount)
				/ ((double) dayCount)).setScale(0, BigDecimal.ROUND_HALF_UP);

		System.out.println(String.format(
				"peopleCount=%d, dayCount=%d, avg=%d, double=%s", peopleCount,
				dayCount, avg.intValue(), avg.doubleValue()));

		return avg.intValue();
	}

	/**
	 * 获取某年在某月的最后一天
	 * 
	 * @author lv.yp
	 * @date 2017-01-07
	 * @param year
	 * @param month
	 * @return
	 */
	private String getMonthFirstDay(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getMinimum(Calendar.DATE));
		return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
	}

	private void addByte(byte addedByte) {

		System.out.println(addedByte);

		String xml1 = "<xml><!-- 这是注释 -->";
		String xml2 = "xxxxxx";
		String xml3 = "</xml>";
		ByteArrayInputStream bis;

		Document document;
		String CHARSET_UTF_8 = "UTF-8";
		try {
			byte[] byte1 = xml1.getBytes(CHARSET_UTF_8);
			byte[] byte2 = xml2.getBytes(CHARSET_UTF_8);
			byte[] byte3 = xml3.getBytes(CHARSET_UTF_8);
			byte[] data = new byte[byte1.length + byte2.length + byte3.length
					+ 1];
			copyByteArray(byte1, 0, byte1.length, data, 0);
			copyByteArray(byte2, 0, byte2.length, data, byte1.length);
			data[byte1.length + byte2.length] = addedByte;
			// data[byte1.length + byte2.length + 1] = (byte)0x15;

			copyByteArray(byte3, 0, byte3.length, data, byte1.length
					+ byte2.length + 1);

			byte b;
			// 过滤特殊字符
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			for (int i = 0; i < data.length; i++) {
				b = data[i];
				if (0x00 <= b && b < 0x20 && b != 0x09 && b != 0x0a
						&& b != 0x0d) {
					System.out.println("非法字符" + b);
					continue;
				}
				bos.write(b);
			}

			bis = new ByteArrayInputStream(bos.toByteArray());
			document = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder().parse(bis);
			System.out.println(document.toString());
		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(e.getMessage());
		}

		System.out.println("Done!");

	}

	private void copyByteArray(byte[] src, int begin, int length, byte[] dest,
			int begin2) {
		for (int i = 0; i < length; i++) {
			dest[begin2 + i] = src[begin + i];
		}
	}

	@Test
	public void test50() {
		long b = 31536000000l;
		int a = (int) b;
		Assert.assertFalse(a == b);
	}

	@Test
	public void test51() {
		Map<String, Object> conf = new HashMap<String, Object>();

		String[] allowFiles = { ".jpg", ".png" };
		conf.put("maxSize", 1l);
		conf.put("allowFiles", allowFiles);

		long maxSize = (Long) conf.get("maxSize");
		Assert.assertEquals(1, maxSize);

		allowFiles = (String[]) conf.get("allowFiles");
		Assert.assertTrue(".jpg".equals(allowFiles[0]));
	}

	@Test
	public void test52() {
		String a = "]";
		System.out.println(a.substring(0, a.length() - 1) + "xxx");
	}

	@Test
	public void test53() {
		SecureRandom random = new SecureRandom();
		byte[] seed = random.generateSeed(128);
		System.out.println(seed);
	}

	@Test
	public void test54() {
		int count = 10;
		while (count-- > 0) {
			final int c = count;
			executors.execute(new Runnable() {

				@Override
				public void run() {
					try {
						TimeUnit.SECONDS.sleep(1);
						System.out.println("run:" + c);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			System.out.println(count);
		}

		System.out.println("Done!");
		executors.shutdown();
		try {
			executors.awaitTermination(100, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test55() {
		StringBuilder sb = new StringBuilder();
		sb.append(new Date());
		sb.append("123");
		System.out.println(sb.toString());
	}

	@Test
	public void test56() {

		Random random = new Random();
		int row = 10, col = 10;

		int length = 1;
		double deep = Math.log(length) / Math.log(2);
		System.out.println(deep);
		length = 2;
		deep = Math.log(length) / Math.log(2);
		System.out.println(deep);
		length = 3;
		deep = Math.log(length) / Math.log(2);
		System.out.println(deep);
		System.out.println(Math.pow(2, 0));

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out
						.print(String.format("%10d\t", random.nextInt(10000)));
			}
			System.out.println();
		}
	}

	/**
	 * 测试布尔运算
	 * 
	 * @author wei.ss
	 */
	@Test
	public void test57() {
		boolean a = true;
		boolean b = true;
		Assert.assertTrue(!(a ^ b));

		a = false;
		b = false;
		Assert.assertTrue(!(a ^ b));

		a = true;
		b = false;
		Assert.assertFalse(!(a ^ b));

		a = false;
		b = true;
		Assert.assertFalse(!(a ^ b));
	}

	@Test
	public void test58() {
		String str = "https://precdn.qxclub.cn/file/filedownload/7D670F6C54C04B6F13158345C92A40B90980D1C1438ABCA20D6D50DCA4310D7591E9F7557CE04E128246EE1E6320EA5D27095856376FF99D34CC00D4F5F52456";
		System.out.println(str.replace("/download/", "/filedownload/"));
	}

	@Test
	public void test59() throws Exception {
		String format = "INSERT INTO marketing_whole_result "
				+ " ( whole_result_id, purpose_user_id, brand_code,"
				+ " consumption_count, re_consumption_count, order_average_price,"
				+ " order_sale_amount, brand_sale_amount, stat_date,"
				+ " create_user, update_user, create_time,"
				+ " update_time, is_valid, new_register_count,"
				+ " avg_register_count, last_avg_register_count)"
				+ " VALUES('%1$s', '%2$s', '%3$s', " + " 0, 0, 0,"
				+ " 0, 0, '%4$s',"
				+ " 'strategy-process', 'strategy-process', '%4$s',"
				+ " '%4$s', 1, 0," + " 0, 0);";

		FileReader fr = new FileReader(new File("e:\\tmp\\str_process.txt"));
		BufferedReader br = new BufferedReader(fr);

		String line = null;
		String id = null;
		String[] records = null;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if ("".equals(line)) {
				continue;
			}
			id = UUIDHexGenerator.generate();
			records = line.split("\\s");
			if (records.length != 5) {
				continue;
			}
			sb.append(
					String.format(format, id, records[1].trim(),
							records[4].trim(),
							(records[2].trim() + " " + records[3].trim())))
					.append("\n");
			System.out.println(Arrays.toString(records));
		}

		System.out.println(sb.toString());

		br.close();
		fr.close();
	}

	@Test
	public void test60() throws Exception {
		String format = "INSERT INTO company_button_role "
				+ " ( id, btn_code, role_code,"
				+ " create_user, create_time, update_user,"
				+ " update_time, is_valid)"
				+ " VALUES('%1$s', '%2$s', '%3$s', "
				+ " 'wei.ss', now(), 'wei.ss'," + " now(), 1);";

		String line = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(new File("e:\\tmp\\added_button_role.txt")), "UTF-8"));
		Set<String> addedButtens = new HashSet<String>();
		while((line = br.readLine()) != null) {
			line = line.trim();
			if("".equals(line) || line.startsWith("--")) {
				continue;
			}
			addedButtens.add(line);
		}
		br.close();
		
		FileReader fr = new FileReader(new File("e:\\tmp\\button_role.txt"));
		br = new BufferedReader(fr);

		String id = null;
		String[] records = null;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			line = line.trim();
			if ("".equals(line) || line.startsWith("--")) {
				continue;
			}
			id = UUIDHexGenerator.generate();
			records = line.split("\\s+");
			if (records.length != 2) {
				continue;
			}
			if(addedButtens.contains(records[0].trim())) {
				continue;
			}
			sb.append(
					String.format(format, id, records[0].trim(),
							records[1].trim())).append("\n");
			// System.out.println(Arrays.toString(records));
		}

		System.out.println(sb.toString());

		br.close();
		fr.close();
	}
	
	@Test
	public void test61(){
		String productUrl = null;
		Assert.assertNull(getSingleAnalysePicUrl16(productUrl));
		
		productUrl = "";
		Assert.assertEquals(productUrl, getSingleAnalysePicUrl16(productUrl));
		
		productUrl = " ";
		Assert.assertEquals(productUrl, getSingleAnalysePicUrl16(productUrl));
		
		productUrl = " xx";
		Assert.assertEquals(productUrl, getSingleAnalysePicUrl16(productUrl));
		
		productUrl = "xx  ";
		Assert.assertEquals(productUrl, getSingleAnalysePicUrl16(productUrl));
		
		productUrl = "xx xx";
		Assert.assertEquals(productUrl, getSingleAnalysePicUrl16(productUrl));
		
		productUrl = " xx.";
		Assert.assertEquals(productUrl, getSingleAnalysePicUrl16(productUrl));
		
		productUrl = "/2017/MDM/AD/AP4362.jpg";
		Assert.assertEquals("/2017/MDM/AD/AP4362_172x172.jpg", getSingleAnalysePicUrl16(productUrl));
	}
	private String getSingleAnalysePicUrl16(String productUrl) {
		if (StringUtils.isEmpty(productUrl) || !productUrl.contains(".") || productUrl.endsWith(".")) {
			return productUrl;
		}
		
		StringBuilder picUrl = new StringBuilder();
		int index = productUrl.lastIndexOf(".");
		picUrl.append(productUrl.substring(0, index));
		picUrl.append("_172x172");
		picUrl.append(productUrl.substring(index));
		
		return picUrl.toString();
	}
	
	@Test
	public void test62(){
		Integer a = new Integer(1);
		Integer b = new Integer(1);
		System.out.println(a == b);
	}
}
