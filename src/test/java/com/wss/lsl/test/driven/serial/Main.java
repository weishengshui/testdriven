package com.wss.lsl.test.driven.serial;


import java.io.*;

public class Main{
	
	public static void main(String[] args) throws Exception {
		
		String fileName = "test.out";
		
		System.out.println("write begin");
		
		A.age2 = 10;
		A a = new A(22);
		System.out.println("a="+a);
		
		System.out.println("write a age=" + a.getAge());
		FileOutputStream bos = new FileOutputStream(fileName);
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(a);	
		oos.flush();
		
		a.setAge(20);
		System.out.println("a set age=" + a.getAge());
		oos.writeObject(a);
		oos.flush();
		oos.writeObject(A.class);
		oos.close();
		
		System.out.println("write end");
	}
}