package com.wss.lsl.test.driven.arithmetic.sort;

import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InsertionSortTest {

	private Random random;
	private int count;
	private int[] data;
	private int[] sortedData;

	@Before
	public void before() {
		random = new Random();
		count = 1000;

		data = new int[count];
		sortedData = new int[count];
		for (int i = 0; i < count; i++) {
			data[i] = random.nextInt(10000);
			sortedData[i] = data[i];
		}
		Arrays.sort(sortedData);
	}

	@Test
	public void testSort() {

		data = InsertionSort.sort(data);
		// System.out.println(Arrays.toString(data));
		Assert.assertArrayEquals(sortedData, data);
	}

	@Test
	public void testSort2() {

		InsertionSort.recursion(data, 1, 1);
		// System.out.println(Arrays.toString(data));
		Assert.assertArrayEquals(sortedData, data);
	}

}
