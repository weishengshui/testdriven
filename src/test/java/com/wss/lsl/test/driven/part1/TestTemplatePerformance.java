package com.wss.lsl.test.driven.part1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * 测试模版的性能
 * 
 * @author Administrator
 * 
 */
public class TestTemplatePerformance {

	private Template template;

	@Before
	public void setUp() {
		StringBuilder sb = new StringBuilder();
		sb.append("${one} partnered with ${two} to bring ${three} Ubuntu ${four} to Google ${five} Engine. ");
		sb.append("Google Compute Engine ${six} ${seven} ${eight} in ${nine} 2013. Since then, it added ");
		sb.append("${ten} for ${eleven}, Debian, Red Hat ${twelve} Linux, SUSE and ${thirteen} ${fourteen} Server. ");
		sb.append("Though Debian and Ubuntu ${fifteen} are ${sixteen}, many ${seventeen} prefer to work on ");
		sb.append("Ubuntu. ${eighteen} to ${nineteen}, Ubuntu powers 85% of Linux workloads running on public ");
		sb.append("clouds. Ubuntu is a popular choice of Linux distribution on ${twenty} EC2, Microsoft Azure and HP ");
		sb.append("Cloud and Joyent.");
		template = new Template(sb.toString());
		template.set("one", "Google");
		template.set("two", "Canonical");
		template.set("three", "official");
		template.set("four", "images");
		template.set("five", "Compute");
		template.set("six", "became");
		template.set("seven", "generally");
		template.set("eight", "available");
		template.set("nine", "December");
		template.set("ten", "support");
		template.set("eleven", "CoreOS");
		template.set("twelve", "Enterprise");
		template.set("thirteen", "Microsoft");
		template.set("fourteen", "Windows");
		template.set("fifteen", "distributions");
		template.set("sixteen", "compatible");
		template.set("seventeen", "developers");
		template.set("eighteen", "According");
		template.set("nineteen", "Canonical");
		template.set("twenty", "Amazon");

	}

	@Test
	public void templateWith100WordsAnd20Variables() {
		long expected = 200l;
		long time = System.currentTimeMillis();
		template.evaluate();
		time = System.currentTimeMillis() - time;
		assertTrue("Rendering the template took " + time
				+ "ms while the target ws " + expected + "ms", time <= expected);
	}
}
