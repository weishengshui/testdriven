package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import com.wss.lsl.test.driven.common.Constants;
import com.wss.lsl.test.driven.part2.controller.LoginController;
import com.wss.lsl.test.driven.part2.service.AuthenticationService;
import com.wss.lsl.test.driven.part2.service.FakeAuthenticationService;

public class LoginControllerTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private LoginController loginController;
	private AuthenticationService authenticationService;

	@Before
	public void setUp() {
		authenticationService = new FakeAuthenticationService();
		authenticationService.addUser(Constants.VALID_USERNAME, Constants.CORRECT_PASSWORD);

		loginController = new LoginController();
		loginController.setAuthenticationService(authenticationService);

		request = new MockHttpServletRequest("GET", "/login");
		response = new MockHttpServletResponse();
	}

	@Test
	public void wrongPasswordShouldRedirectToErrorPage() throws Exception {
		request.addParameter(Constants.USERNAME_PARAMETER, Constants.VALID_USERNAME);
		request.addParameter(Constants.PASSWORD_PARAMETER, "wrong_password");

		ModelAndView modelAndView = loginController.handleRequest(request,
				response);
		assertEquals("wrongpassword", modelAndView.getViewName());
	}

	@Test
	public void validLoginForwardsToFrontPage() throws Exception {

		request.addParameter(Constants.USERNAME_PARAMETER, Constants.VALID_USERNAME);
		request.addParameter(Constants.PASSWORD_PARAMETER, Constants.CORRECT_PASSWORD);
		ModelAndView modelAndView = loginController.handleRequest(request,
				response);

		assertEquals("frontpage", modelAndView.getViewName());
	}
}
