package com.wss.lsl.test.driven.part2;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.easymock.EasyMock;
import org.junit.Test;

import com.mockobjects.sql.MockMultiRowResultSet;
import com.wss.lsl.test.driven.part2.dao.impl.JdbcPersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;

public class JdbcPersonDaoTest {

	@Test
	public void testFindByLastname() throws SQLException {

		String lastname = "Smith";
		// 录制过程
		DataSource dataSource = EasyMock.createMock(DataSource.class);
		Connection connection = EasyMock.createMock(Connection.class);
		EasyMock.expect(dataSource.getConnection()).andReturn(connection);
		String sql = "select * from people where last_name = ?";
		PreparedStatement stmt = EasyMock.createMock(PreparedStatement.class);
		EasyMock.expect(connection.prepareStatement(sql)).andReturn(stmt);
		stmt.setString(1, lastname);

		MockMultiRowResultSet resultSet = new MockMultiRowResultSet();
		String[] columnNames = new String[] { "first_name", "last_name" };
		resultSet.setupColumnNames(columnNames);
		List<Person> smiths = createListOfPersonWithLastname(lastname);
		resultSet.setupRows(asResultSetArray(smiths));

		EasyMock.expect(stmt.executeQuery()).andReturn(resultSet);

		// 释放资源
		resultSet.setExpectedCloseCalls(1);
		stmt.close();
		connection.close();

		// 开始回放
		EasyMock.replay(dataSource, connection, stmt);
		
		JdbcPersonDao dao = new JdbcPersonDao();
		dao.setDataSource(dataSource);

		List<Person> persons = dao.findByLastname(lastname);
		assertEquals(smiths, persons);
		EasyMock.verify(dataSource, connection, stmt);
		resultSet.verify();
	}

	private Object[][] asResultSetArray(List<Person> persons) {

		Object[][] records = new Object[persons.size()][2];
		for (int i = 0, length = records.length; i < length; i++) {
			Person person = persons.get(i);
			records[i] = new Object[] { person.getFirstname(),
					person.getLastname() };
		}
		return records;
	}

	private List<Person> createListOfPersonWithLastname(String lastname) {

		List<Person> expected = new ArrayList<Person>();
		expected.add(new Person("Alice", lastname));
		expected.add(new Person("Billy", lastname));
		expected.add(new Person("Clark", lastname));

		return expected;
	}

}
