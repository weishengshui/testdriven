package com.wss.lsl.test.driven;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BaseTest {

	protected AnnotationConfigApplicationContext applicationContext;

	@Before
	public void before() {
		// applicationContext = new
		// AnnotationConfigApplicationContext(TestdrivenSpringConfig.class);

		// 编程方式激活profile
		applicationContext = new AnnotationConfigApplicationContext();
		applicationContext.getEnvironment().setActiveProfiles("pro");
		applicationContext.register(TestdrivenSpringConfig.class);
		applicationContext.refresh();

		// applicationContext.getBeanFactory().registerScope("", null); // 注册自定义的scope
		// 另一种注册自定义的scope的方式，使用配置类CustomScopeConfigurer
	}

	@After
	public void after() {
		applicationContext.close();
	}

	@Test
	public void t_() {
	}
}
