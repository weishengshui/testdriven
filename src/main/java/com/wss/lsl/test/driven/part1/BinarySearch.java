package com.wss.lsl.test.driven.part1;

/**
 * 二分查找：不考虑数组中元素为null的情况
 * 
 * @author Administrator
 * 
 */
public class BinarySearch<T extends Comparable<T>> {

	/**
	 * 查找到就返回数组下标，否则返回-1
	 * 
	 * @param data
	 * @param item
	 * @return
	 */
	public int search(T[] data, T item) {
		int lengh = data.length;
		int i = 0, j = lengh - 1;
		int index = -1;
		if(item == null) {
			return index;
		}
		while (true) {
			// 说明没有找到
			if (i > j) {
				break;
			}
			int temp = (i + j) / 2;
			if (data[temp].equals(item)) {
				index = temp;
				break;
			}
			if (data[temp].compareTo(item) > 0) {
				j = temp - 1;
			} else {
				i = temp + 1;
			}
		}
		return index;
	}
}
