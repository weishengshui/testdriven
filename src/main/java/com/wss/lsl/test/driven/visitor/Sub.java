package com.wss.lsl.test.driven.visitor;

/**
 * 加法
 * 
 * @author weiss
 *
 */
public class Sub extends BinaryOperator {

	public Sub(Node left, Node right) {
		super(left, right);
	}
}
