package com.wss.lsl.test.driven.exception;

public class MissingValueException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MissingValueException() {
	}
	
	public MissingValueException(String msg) {
		super(msg);
	}
	
	public MissingValueException(Throwable t) {
		super(t);
	}

}
