package com.wss.lsl.test.driven.visitor;

public class Evaluator extends NodeVisitor {

	{
		nameToFunctionMap.put("visit_Number", this::visit_Number);
		nameToFunctionMap.put("visit_Negate", this::visit_Negate);
		nameToFunctionMap.put("visit_Add", this::visit_Add);
		nameToFunctionMap.put("visit_Sub", this::visit_Sub);
		nameToFunctionMap.put("visit_Mul", this::visit_Mul);
		nameToFunctionMap.put("visit_Div", this::visit_Div);
	}

	private Double visit_Negate(Node node) {
		Negate negate = (Negate) node;
		return -visitor(negate.getOperand());
	}

	private Double visit_Add(Node node) {
		Add t = (Add) node;
		return visitor(t.getLeft()) + visitor(t.getRight());
	}
	
	private Double visit_Sub(Node node) {
		Sub t = (Sub) node;
		return visitor(t.getLeft()) - visitor(t.getRight());
	}
	
	private Double visit_Mul(Node node) {
		Mul t = (Mul) node;
		return visitor(t.getLeft()) * visitor(t.getRight());
	}
	
	private Double visit_Div(Node node) {
		Div t = (Div) node;
		return visitor(t.getLeft()) / visitor(t.getRight());
	}

	private Double visit_Number(Node node) {
		Number number = (Number) node;
		return number.getValue();
	}
}
