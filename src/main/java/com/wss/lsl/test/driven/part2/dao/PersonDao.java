package com.wss.lsl.test.driven.part2.dao;

import java.util.List;

import com.wss.lsl.test.driven.part2.entity.Person;

public interface PersonDao {

	Person find(Integer id);

	void save(Person person) throws Exception;

	void update(Person person);

	void delete(Person person);

	List<Person> findAll();

	List<Person> findByLastname(String lastname);
}
