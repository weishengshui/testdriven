package com.wss.lsl.test.driven.part1;

import java.util.Map;

import com.wss.lsl.test.driven.exception.MissingValueException;

public class Variable implements Segment {

	private String name;

	public Variable(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		return name.equals(((Variable) obj).name);
	}

	@Override
	public String evaluate(Map<String, String> variables) {
		if (!variables.containsKey(name)) {
			throw new MissingValueException("No value for ${" + name + "}");
		}
		return variables.get(name);
	}
}
