package com.wss.lsl.test.driven.visitor;

/**
 * 二元计算
 * 
 * @author weiss
 *
 */
public abstract class BinaryOperator extends Node {

	private Node left;
	private Node right;

	public BinaryOperator(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
	}

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}

}
