package com.wss.lsl.test.driven.spring.lifecycle;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.Lifecycle;
import org.springframework.stereotype.Service;

@Service("lifecycleBean2")
public class LifecycleBean implements Lifecycle, BeanNameAware {

	private volatile boolean running;
	private String name;

	// 获取自己的bean名字
	@Override
	public void setBeanName(String name) {
		this.name = name;
	}

	public void printName() {
		System.out.println("====================" + this.name);
	}

	// 不会自动调start()方法
	@Override
	public void start() {
		System.out.println("LifecycleBean start");
		running = true;

	}

	@Override
	public void stop() {
		System.out.println("LifecycleBean stop");
		running = false;
	}

	@Override
	public boolean isRunning() {
		System.out.println("LifecycleBean isRunning");
		return running;
	}

}
