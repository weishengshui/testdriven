package com.wss.lsl.test.driven.visitor;

/**
 * 加法
 * 
 * @author weiss
 *
 */
public class Add extends BinaryOperator {

	public Add(Node left, Node right) {
		super(left, right);
	}
}
