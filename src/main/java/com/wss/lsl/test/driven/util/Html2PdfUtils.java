//package com.wss.lsl.test.driven.util;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.nio.charset.StandardCharsets;
//
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.FontProvider;
//import com.itextpdf.text.pdf.PdfWriter;
//import com.itextpdf.tool.xml.XMLWorkerFontProvider;
//import com.itextpdf.tool.xml.XMLWorkerHelper;
//
///**
// * html转化为pdf
// * 
// * @author weiss
// *
// */
//public class Html2PdfUtils {
//
//	public static void toPdf(String url, OutputStream pdfOut) {
//
//		try (CloseableHttpClient client = HttpClients.createDefault();) {
//			HttpGet request = new HttpGet(url);
//			CloseableHttpResponse response = client.execute(request);
//			int statusCode = response.getStatusLine().getStatusCode();
//			if (statusCode == 200) {
//				InputStream is = response.getEntity().getContent();
//				html2Pdf(is, pdfOut);
//			} else {
//				throw new RuntimeException("下载html失败");
//			}
//		} catch (Exception e) {
//			throw new RuntimeException("转换pdf发生异常", e);
//		}
//	}
//
//	public static void html2Pdf(InputStream htmlIs, OutputStream pdfOut) throws IOException, DocumentException {
//		Document document = new Document();
//		// step 2
//		PdfWriter writer = PdfWriter.getInstance(document, pdfOut);
//		// step 3
//		document.open();
//		// step 4
////		 XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlIs,
////		 StandardCharsets.UTF_8);
//
////		String resourcesRootPath = "https://treport.1hykj.com";
//		String resourcesRootPath = "http://127.0.0.1:9010";
//		String fontPath = "D:\\tmp\\8.6\\html2pdf\\element-icons.535877f5.ttf";
//		FontProvider fontProvider = new XMLWorkerFontProvider(fontPath);
//		InputStream inCssFile = null;
//		XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlIs, inCssFile, StandardCharsets.UTF_8,
//				fontProvider, resourcesRootPath);
//		// step 5
//		document.close();
//
//		System.out.println("PDF Created!");
//	}
//
//}
