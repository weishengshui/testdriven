package com.wss.lsl.test.driven.effective.entry47;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SubLists {

	public static <E> Stream<List<E>> of(List<E> list) {

		return Stream.concat(Stream.of(Collections.emptyList()), prefix(list).flatMap(SubLists::suffix));
	}

	public static <E> Stream<List<E>> prefix(List<E> list) {
		return IntStream.rangeClosed(1, list.size()).mapToObj(end -> list.subList(0, end));
	}

	public static <E> Stream<List<E>> suffix(List<E> list) {
		return IntStream.range(0, list.size()).mapToObj(start -> list.subList(start, list.size()));
	}
}
