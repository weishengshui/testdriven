package com.wss.lsl.test.driven.common;

/**
 * 常量表
 * 
 * @author Administrator
 * 
 */
public interface Constants {

	/**
	 * 有效用户名
	 */
	public static final String VALID_USERNAME = "validusername";
	/**
	 * 有效密码
	 */
	public static final String CORRECT_PASSWORD = "correctpassword";
	/**
	 * 用户名参数名称
	 */
	public static final String USERNAME_PARAMETER = "j_username";
	/**
	 * 密码参数名称
	 */
	public static final String PASSWORD_PARAMETER = "j_password";
}
