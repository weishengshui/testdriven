package com.wss.lsl.test.driven.part2.service;

/**
 * 验证登录接口
 * 
 * @author Administrator
 *
 */
public interface AuthenticationService {
	
	void addUser(String username, String password);
	
	boolean isValidLogin(String username, String password);
}
