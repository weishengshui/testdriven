package com.wss.lsl.test.driven.sql;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.wss.lsl.test.driven.util.UUIDHexGenerator;

/**
 * 替换sql脚本的主键
 * 
 * @author weiss
 *
 */
public class ReplacePrimarkKey {

	public static void main(String[] args) throws IOException {
		if (args == null || args.length == 0) {
			throw new IllegalArgumentException("没有指定脚本路径");
		}

		String sqlPath = args[0];
		File file = new File(sqlPath);
		String sqlContent = FileUtils.readFileToString(file, "UTF-8");

		Pattern pattern = Pattern.compile("VALUES \\('([^']*)'");
		Matcher matcher = pattern.matcher(sqlContent);
		int index = 0;
		while (matcher.find()) {
			String id = matcher.group(1);
			sqlContent = sqlContent.replaceAll(id, UUIDHexGenerator.generate());
			// System.out.println(id);
			System.out.println(++index);
		}

		System.out.println("Done");
		FileUtils.write(new File("d:\\replaced_id.sql"), sqlContent, "UTF-8");
	}
}
