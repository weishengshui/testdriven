package com.wss.lsl.test.driven.runnable.producecustomer;

import java.util.concurrent.TimeUnit;

/**
 * 模拟消费者
 * 
 * @author Administrator
 *
 */
public class C implements Runnable {

	private M m;

	public C(M m) {
		super();
		this.m = m;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (m.buf.get() <= 0) {
						wait();
					}
				}

				// System.out.println("get");
				synchronized (m.p) {
					m.buf.addAndGet(-1);
					System.out.println("c " + m.buf.get());
					m.p.notify();
				}


				TimeUnit.MILLISECONDS.sleep(m.random.nextInt(200));
			}
		} catch (InterruptedException e) {
			System.out.println("interrupted c");
		}
	}
}
