package com.wss.lsl.test.driven.arithmetic.search;

/**
 * 二分搜索：数组实现
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public abstract class BinArraySearch<T extends Comparable<T>> extends
		BinSearch<T> {

	protected T[] data;

	public BinArraySearch(T[] data) {
		super();
		this.data = data;
	}

	/**
	 * 查找元素
	 * 
	 * @param e
	 * @return 找到返回下标，没找到返回-1
	 * @author wei.ss
	 */
	public abstract int search(T e);

	/**
	 * 打印元素列表
	 * 
	 * @author wei.ss
	 */
	public void report() {
		for (int i = 0; i < data.length; i++) {
			System.out.print(data[i] + " ");
		}
	}

	@Override
	public void validate(T e, int p) {

		int p2 = -1;
		for (int i = 0; i < data.length; i++) {
			if (e.equals(data[i])) {
				p2 = i;
				break;
			}
		}

		if (p2 != p) {
			throw new RuntimeException(String.format("元素的位置不对:e=%s,p=%s", e, p));
		}
	}
}
