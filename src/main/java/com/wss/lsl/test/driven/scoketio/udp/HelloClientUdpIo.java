package com.wss.lsl.test.driven.scoketio.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * 接收服务端的内容
 * 
 * @author wei.ss
 *
 */
public class HelloClientUdpIo {

	public static void main(String[] args) throws Exception {
		DatagramSocket ds = new DatagramSocket(9000);

		byte[] data = new byte[1024];
		DatagramPacket dp = new DatagramPacket(data, data.length);
		ds.receive(dp);
		data = dp.getData();
		System.out.println(new String(data, 0, dp.getLength(), "UTF-8"));
		ds.close();
	}

}
