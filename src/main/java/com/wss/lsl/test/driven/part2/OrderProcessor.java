package com.wss.lsl.test.driven.part2;

public class OrderProcessor {

	private PricingService pricingService;

	public void setPricingService(PricingService pricingService) {
		this.pricingService = pricingService;
	}

	public void process(Order order) {

		Customer customer = order.getCustomer();
		Product product = order.getProduct();
		float discount = pricingService.getDiscountPercentage(
				order.getCustomer(), order.getProduct());
		float balance = customer.getBalance()
				- (product.getListPrice() * (1 - discount / 100));
		customer.setBalance(balance);
	}

}
