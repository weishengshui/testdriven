package com.wss.lsl.test.driven.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * ST店员二维码处理
 * 
 * @author wei.ss
 * @date 2018年1月22日
 * @copyright wonhigh.cn
 */
public class StAssistantQrcode2 {
	// 边框的高度和宽度
	private static final int BORDER_H = 471;
	private static final int BORDER_W = 156;
	// 二维码定位
	private static final int QR_BORDER_W = 145;
	private static final int QR_BORDER_H = 166;

	// 单张图片宽度、高度
	private static final int IMAGE_W = 638;
	private static final int IMAGE_H = 1005;

	// 二维码大小
	private static final int QRCODE_SIZE = 353;

	private static final int ROW_COUNT = 4;
	private static final int COL_COUNT = 5;

	// 一张纸承载工牌的数量
	private static final int PAGE_SIZE = ROW_COUNT * COL_COUNT;

	public static void main(String[] args) throws Exception {

		List<Customer> customers = getCustomerList(23);

		List<File> list = mergeImage(customers);
		System.out.println(list);

		System.out.println("Done!");
	}

	// 合成图片
	public static List<File> mergeImage(List<Customer> customers)
			throws Exception {

		int pageCount = customers.size() / PAGE_SIZE;
		pageCount = customers.size() % PAGE_SIZE == 0 ? pageCount
				: pageCount + 1;
		List<File> files = new ArrayList<File>(pageCount * 2);
		List<Customer> tempCustomers = null;
		int toIndex = 0;
		int fromIndex = 0;
		for (int i = 0; i < pageCount; i++) {
			fromIndex = i * PAGE_SIZE;
			toIndex = fromIndex + PAGE_SIZE;
			if (toIndex > customers.size()) {
				toIndex = customers.size();
			}
			tempCustomers = customers.subList(fromIndex, toIndex);

			// 合成正面
			files.add(frontage(tempCustomers, i + 1));
			// 合成反面
			files.add(mergeReverse(tempCustomers, i + 1));
		}

		return files;
	}

	// 合成反面
	public static File mergeReverse(List<Customer> customers, int pageNo)
			throws Exception {
		String image2 = "E:\\tmp\\st2\\st_22.jpg";
		String dest = "E:\\tmp\\st2\\st_22_第" + pageNo + "页--反面.jpg";
		File image3 = new File(dest);

		BufferedImage i1 = zoomImage(QRCODE_SIZE);
		BufferedImage i2 = getImage(image2);

		// 这种方式更优
		Graphics2D g = i2.createGraphics();
		// 消除线条锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		int index = 0;
		Customer customer = null;
		outer: for (int i = 0; i < ROW_COUNT; i++) {
			// 反面的顺序反过来
			for (int j = COL_COUNT - 1; j >= 0; j--) {
				index = i * (COL_COUNT) + (COL_COUNT - j - 1);
				if (index >= customers.size()) {
					break outer;
				}

				customer = customers.get(index);
				System.out.println("打印反面：" + customer.name + " i=" + i + " j="
						+ j);
				g.drawImage(i1, BORDER_W + (IMAGE_W * j) + QR_BORDER_W,
						BORDER_H + IMAGE_H * i + QR_BORDER_H, i1.getWidth(),
						i1.getWidth(), Color.WHITE, null);

				// 标记，要注释掉
				Font font = new Font("微软雅黑", Font.PLAIN, 85);
				font = new Font("微软雅黑", Font.PLAIN, 85);
				g.setFont(font);
				g.setColor(Color.BLUE);
				g.drawString(customer.name, BORDER_W + (IMAGE_W * j) + 78,
						472 + 1004 * i + 115);
			}

		}

		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_ATOP, 1.0f));
		g.dispose();

		ImageIO.write(i2, dest.substring(dest.lastIndexOf(".") + 1), image3);

		return image3;
	}

	// 合成正面
	private static File frontage(List<Customer> customers, int pageNo)
			throws IOException {
		String image2 = "E:\\tmp\\st2\\st_11.jpg";
		String dest = "E:\\tmp\\st2\\st_22_第" + pageNo + "页--正面.jpg";
		File image3 = new File(dest);

		BufferedImage i2 = getImage(image2);

		// 这种方式更优
		Graphics2D g = i2.createGraphics();
		// 消除线条锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// String name = "宁晓荣";
		// String saleNo = "171096107";
		// String shopName = "龙华龙观东路天虹";

		int x = 78, y = 435;
		int borderH = 472;
		int imageH = 1004;
		Font font = null;
		Color color = null;
		color = new Color(0xd5, 0xc8, 0xbe);

		int index = 0;
		Customer customer = null;
		outer: for (int i = 0; i < ROW_COUNT; i++) {
			// 反面的顺序反过来
			for (int j = 0; j < COL_COUNT; j++) {
				index = i * (COL_COUNT) + j;
				if (index >= customers.size()) {
					break outer;
				}
				customer = customers.get(index);
				System.out.println("打印正面：" + customer.name + " i=" + i + " j="
						+ j);

				font = new Font("微软雅黑", Font.PLAIN, 85);
				g.setFont(font);
				g.setColor(color);
				g.drawString(customer.name, BORDER_W + (IMAGE_W * j) + x,
						borderH + imageH * i + y);

				// 字体大小是名字的0.8
				font = new Font("微软雅黑", Font.BOLD, 68);
				g.setFont(font);
				g.setColor(color);
				g.drawString(customer.saleNo, BORDER_W + (IMAGE_W * j) + x,
						borderH + imageH * i + y + 90);

				// 字体大小是名字的0.35
				font = new Font("微软雅黑", Font.PLAIN, 30);
				g.setFont(font);
				g.setColor(color);
				g.drawString(customer.shopName, BORDER_W + (IMAGE_W * j) + x,
						borderH + imageH * i + y + 109 + 40);
			}
		}

		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_ATOP, 1.0f));
		g.dispose();
		ImageIO.write(i2, dest.substring(dest.lastIndexOf(".") + 1), image3);

		return image3;
	}

	private static BufferedImage getImage(String path) throws IOException {
		File file = new File(path);

		return ImageIO.read(file);
	}

	public static List<Customer> getCustomerList(int count) {
		List<Customer> list = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			list.add(new Customer("宁晓荣" + i));
		}

		return list;
	}

	private static class Customer {
		private String name = "宁晓荣";
		private String saleNo = "171096107";
		private String shopName = "龙华龙观东路天虹";

		public Customer(String name) {
			super();
			this.name = name;
		}
	}

	/*
	 * 图片按比率缩放 size为文件大小
	 */
	public static BufferedImage zoomImage(Integer size) throws Exception {
		InputStream is = StAssistantQrcode2.class
				.getResourceAsStream("/st.jpg");
		// String dest = "e:/tmp/st_little.jpg";
		// File destFile = new File(dest);

		BufferedImage bufImg = ImageIO.read(is);
		int width = bufImg.getWidth();

		Double rate = ((double) (size)) / width; // 获取长宽缩放比例

		System.out.println(rate);
		Image Itemp = bufImg.getScaledInstance(bufImg.getWidth(),
				bufImg.getHeight(), BufferedImage.SCALE_SMOOTH);

		AffineTransformOp ato = new AffineTransformOp(
				AffineTransform.getScaleInstance(rate, rate), null);
		Itemp = ato.filter(bufImg, null);
		return (BufferedImage) Itemp;
		// try {
		// ImageIO.write((BufferedImage) Itemp,
		// dest.substring(dest.lastIndexOf(".") + 1), destFile);
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
	}

}
