package com.wss.lsl.test.driven.part1;

public class Node {
	
	public int val;
	public Node next;
	
	public Node(int val, Node next) {
		this.val = val;
		this.next = next;
	}
}
