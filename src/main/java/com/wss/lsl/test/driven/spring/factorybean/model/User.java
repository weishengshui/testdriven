package com.wss.lsl.test.driven.spring.factorybean.model;

public class User {

	private String id;
	private String roleId;
	private String userName;

	// 非数据库字段
	private Role role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", roleId=" + roleId + ", userName=" + userName + ", role=" + role + "]";
	}

}
