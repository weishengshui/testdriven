package com.wss.lsl.test.driven.arithmetic.sort;

/**
 * 抽象基类
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public abstract class AbstractQuicklySort<T extends Comparable<T>> implements
		Sort<T> {

	protected T[] data;

	public AbstractQuicklySort(T[] data) {
		super();
		this.data = data;
	}

	@Override
	public void validate() {
		for (int i = 0; i < data.length - 1; i++) {
			if (data[i].compareTo(data[i + 1]) > 0) {
				throw new RuntimeException(String.format("数组不是有序的：%s=%s,%s=%s",
						i, data[i], i + 1, data[i + 1]));
			}
		}
	}

	protected void swap(int i, int j) {
		T e = data[i];
		data[i] = data[j];
		data[j] = e;
	}
}
