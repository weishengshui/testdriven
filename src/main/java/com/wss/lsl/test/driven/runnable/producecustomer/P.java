package com.wss.lsl.test.driven.runnable.producecustomer;

import java.util.concurrent.TimeUnit;

/**
 * 模拟生产者
 * 
 * @author Administrator
 *
 */
public class P implements Runnable {

	private M m;
	private int count;

	public P(M m) {
		super();
		this.m = m;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (m.buf.get() >= 10) {
						wait();
					}
				}

				if (count++ >= 100) {
					System.out.println("to be end");
					m.executorService.shutdownNow();
				}

				// System.out.println("put " + count);
				synchronized (m.c) {
					m.buf.addAndGet(1);
					System.out.println("p " + m.buf.get());
					m.c.notify();
				}


				TimeUnit.MILLISECONDS.sleep(m.random.nextInt(100));
			}
		} catch (InterruptedException e) {
			System.out.println("Interrupted p");
		}
	}
}
