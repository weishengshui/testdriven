package com.wss.lsl.test.driven.arithmetic.search;

/**
 * 二分搜索第一个
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public class BinArraySearchFirst<T extends Comparable<T>> extends
		BinArraySearch<T> {

	public BinArraySearchFirst(T[] data) {
		super(data);
	}

	public int search(T e) {
		int l = -1, u = data.length;
		int m;
		while (l + 1 < u) {
			m = (l + u) / 2;
			if (data[m].compareTo(e) < 0) {
				l = m;
			} else {
				u = m;
			}
		}

		int p = u;
		if (p < data.length && data[p].equals(e)) {
			return p;
		}

		return -1;

	}

}
