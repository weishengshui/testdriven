package com.wss.lsl.test.driven.spring.staticmethodbean;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class StaticConfigation {

	@Bean
	public static DemoMemberService demoMemberService() {
		return new DemoMemberService();
	}
}
