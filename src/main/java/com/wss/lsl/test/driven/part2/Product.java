package com.wss.lsl.test.driven.part2;

public class Product {

	private String name;
	private float listPrice;

	public Product(String name, float listPrice) {
		this.name = name;
		this.listPrice = listPrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getListPrice() {
		return listPrice;
	}

	public void setListPrice(float listPrice) {
		this.listPrice = listPrice;
	}

}
