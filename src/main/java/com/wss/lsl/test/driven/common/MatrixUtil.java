package com.wss.lsl.test.driven.common;

public class MatrixUtil {

	public static int calcMaxSumOfSubMatrix(int[][] matrix) {

		int maxEndingHere = 0;
		int maxSofar = 0;
		for (int i = 0, rowLength = matrix.length; i < rowLength; i++) {
			for (int j = 0, cellLength = matrix[i].length; j < cellLength; j++) {
				maxEndingHere = Math.max(maxEndingHere + matrix[i][j], 0);
				maxSofar = Math.max(maxSofar, maxEndingHere);
			}
		}
		return maxSofar;
	}
}
