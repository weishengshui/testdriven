package com.wss.lsl.test.driven.runnable.producecustomer;

import java.util.concurrent.TimeUnit;

public class Cheaf implements Runnable {

	Restaurant restaurant;
	int count;

	public Cheaf(Restaurant restaurant) {
		super();
		this.restaurant = restaurant;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (restaurant.meal != null) {
						wait();
					}
				}

				if (count++ >= 10) {
					System.out.println("out of food");
					restaurant.executorService.shutdownNow();
				}

				System.out.println("order up!  " + count);

				synchronized (restaurant.waitPerson) {
					restaurant.meal = new Meal();
					restaurant.waitPerson.notifyAll();
				}

				TimeUnit.MILLISECONDS.sleep(100);
			}
		} catch (InterruptedException e) {
			System.out.println("interrupted cheaf");
		}
	}
}
