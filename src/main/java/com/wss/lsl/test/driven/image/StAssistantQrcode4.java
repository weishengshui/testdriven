//package com.wss.lsl.test.driven.image;
//
//import java.awt.Graphics2D;
//import java.awt.Image;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.FileOutputStream;
//
//import javax.imageio.ImageIO;
//import javax.imageio.ImageReader;
//import javax.imageio.metadata.IIOMetadata;
//import javax.imageio.stream.FileImageInputStream;
//
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//
///**
// * 图片分辨率demo
// * 
// * @author wei.ss
// * @date 2018年2月5日
// * @copyright wonhigh.cn
// */
//public class StAssistantQrcode4 {
//
//	public static void main(String[] args) throws Exception {
////		for(int type = 1; type <= 13; type++) {
////			System.out.println("type:" + type);
////			try {
////			} catch (Exception e) {
////				e.printStackTrace();
////			}
////		}
//		mergeImage(1);
//	}
//	
//	public static void mergeImage(int type) throws Exception {
//		File infile = new File("E:\\tmp\\st4\\st_22.jpg");
//		File outfile = new File("E:\\tmp\\st4\\st_22_222_"+type+".jpg");
//
//		ImageReader reader = ImageIO.getImageReadersByFormatName("jpeg").next();
//		reader.setInput(new FileImageInputStream(infile), true, false);
//		IIOMetadata data = reader.getImageMetadata(0);
//		BufferedImage image = reader.read(0);
//
//		int w = image.getWidth(), h = image.getHeight();
//		Image rescaled = image.getScaledInstance(w, h,
//				Image.SCALE_AREA_AVERAGING);
//		BufferedImage output = toBufferedImage(rescaled,
//				type);
//
//		Element tree = (Element) data.getAsTree("javax_imageio_jpeg_image_1.0");
//		Element jfif = (Element) tree.getElementsByTagName("app0JFIF").item(0);
//		for (int i = 0; i < jfif.getAttributes().getLength(); i++) {
//			Node attribute = jfif.getAttributes().item(i);
//			System.out.println(attribute.getNodeName() + "="
//					+ attribute.getNodeValue());
//		}
//		FileOutputStream fos = new FileOutputStream(outfile);
//		JPEGImageEncoder jpegEncoder = JPEGCodec.createJPEGEncoder(fos);
//		JPEGEncodeParam jpegEncodeParam = jpegEncoder
//				.getDefaultJPEGEncodeParam(output);
//		jpegEncodeParam.setDensityUnit(JPEGEncodeParam.DENSITY_UNIT_DOTS_INCH);
//		jpegEncodeParam.setXDensity(300);
//		jpegEncodeParam.setYDensity(300);
//		jpegEncoder.encode(output, jpegEncodeParam);
//		fos.close();
//	}
//
//	public static BufferedImage toBufferedImage(Image image, int type) {
//		int w = image.getWidth(null);
//		int h = image.getHeight(null);
//		System.out.println(w);
//		System.out.println(h);
//		BufferedImage result = new BufferedImage(w, h, type);
//		Graphics2D g = result.createGraphics();
//		g.drawImage(image, 0, 0, null);
//		g.dispose();
//		return result;
//	}
//
//}
