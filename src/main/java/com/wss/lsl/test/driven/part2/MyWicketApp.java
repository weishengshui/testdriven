package com.wss.lsl.test.driven.part2;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

import com.wss.lsl.test.driven.part2.page.MyHomePage;

public class MyWicketApp extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return MyHomePage.class;
	}

}
