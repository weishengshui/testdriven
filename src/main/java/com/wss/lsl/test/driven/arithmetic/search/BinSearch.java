package com.wss.lsl.test.driven.arithmetic.search;

/**
 * 二分搜索
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public abstract class BinSearch<T extends Comparable<T>> {

	/**
	 * 查找元素
	 * 
	 * @param e
	 * @return 找到返回下标，没找到返回-1
	 * @author wei.ss
	 */
	public abstract int search(T e);

	/**
	 * 打印元素列表
	 * 
	 * @author wei.ss
	 */
	public abstract void report();

	/**
	 * 验证查找的元素下标对不对
	 * 
	 * @param e
	 * @param p
	 * @author wei.ss
	 */
	public abstract void validate(T e, int p);
}
