package com.wss.lsl.test.driven.part2.common.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import com.wss.lsl.test.driven.part2.common.time.SystemTime;

public class HttpRequestLogFormatter {

	public static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public String format(HttpServletRequest request, int httpStatusCode,
			int contentLength) {

		StringBuilder log = new StringBuilder();
		log.append(request.getRemoteAddr());
		log.append(" ");
		log.append(request.getRemoteUser());
		log.append(" [");
		log.append(DATE_FORMAT.format(SystemTime.asDate()));
		log.append("] \"");
		log.append(request.getMethod());
		log.append(" ");
		log.append(request.getRequestURI());
		log.append(" ");
		log.append(request.getProtocol());
		log.append("\" ");
		log.append(httpStatusCode);
		log.append(" ");
		log.append(contentLength);

		return log.toString();
	}
}
