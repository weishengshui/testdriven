package com.wss.lsl.test.driven.part2.page;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

public class MyHomePage extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyHomePage() {
		add(new Label("welcomeMessage", "Welcome to the home page"));
		add(new BookmarkablePageLink<WebPage>("linkToLoginPage", LoginPage.class));
	}

}
