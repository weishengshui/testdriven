package com.wss.lsl.test.driven.spring.lookupmethod;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("prototypeBeanB")
@Scope("prototype")
public class PrototypeBeanB {

}
