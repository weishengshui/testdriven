package com.wss.lsl.test.driven.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;

import javax.imageio.ImageIO;

/**
 * 二维码缩放
 * 
 * @author wei.ss
 * @date 2018年3月1日
 * @copyright wonhigh.cn
 */
public class QrcodeScaling {

	public static void main(String[] args) throws Exception {
		int[] sizeArray = { 8, 12, 15, 30, 50 };// 各种尺寸的二维码
		int[] fontSizes = { 12, 17, 20, 40, 60 };// 字体大小
		int[] lineSpacing = { 2, 10, 12, 40, 60 };// 行距

		BufferedImage i2 = null;
		File imageFile = null;

		BufferedImage dest = null;
		int index = 0;
		Font font = new Font("微软雅黑", Font.PLAIN, fontSizes[index]);
		for (int i : sizeArray) {
			imageFile = new File("E:\\tmp\\sceneqrcode\\dest_" + i + ".jpg");
			i2 = zoomImage(i);

			// 加文字
			int newWidth = i2.getWidth() + 30 * 2;
			int newheight = i2.getHeight() + (3 * fontSizes[index] / 2 + 30)
					* 2;
			dest = new BufferedImage(newWidth, newheight,
					BufferedImage.TYPE_BYTE_GRAY);
			Graphics2D g = dest.createGraphics();
			g.setBackground(Color.WHITE);
			g.clearRect(0, 0, newWidth, newheight);
			g.drawImage(i2, 30, 30 + fontSizes[index], i2.getWidth(),
					i2.getHeight(), Color.WHITE, null);
			g.setFont(new Font("微软雅黑", Font.PLAIN, fontSizes[index]));
			g.setColor(Color.BLACK);
			// 活动名称
			g.drawString("活动名称活动名称活动名称活动名称活动名称", 30, 30 + lineSpacing[index]);
			// 二维码有效期
			g.drawString("二维码有效期：", 10, i2.getHeight()
					+ (3 * fontSizes[index] / 2 + 30));
			g.drawString(
					"2018.01.01 00:00——2018.02.01 23:59",
					10,
					i2.getHeight() + (3 * fontSizes[index] / 2 + 30)
							+ font.getSize() + lineSpacing[index]);

			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,
					1.0f));
			g.dispose();

			ImageIO.write(dest, "jpg", imageFile);

			index++;
		}

		System.out.println("Done!");
	}

	/*
	 * 图片大小：单位厘米
	 */
	public static BufferedImage zoomImage(int cmSize) throws Exception {
		InputStream is = QrcodeScaling.class
				.getResourceAsStream("/qrcode/showqrcode15.jpg");
		// String dest = "e:/tmp/st_little.jpg";
		// File destFile = new File(dest);

		BigDecimal pxSize = new BigDecimal((cmSize / 1.5) * 43); // 图片像素大小
		pxSize = pxSize.setScale(0, java.math.BigDecimal.ROUND_UP);

		BufferedImage bufImg = ImageIO.read(is);
		ColorModel colorModel = bufImg.getColorModel();
		System.out.println(colorModel);
		int width = bufImg.getWidth();

		Double rate = ((double) (pxSize.doubleValue())) / width; // 获取长宽缩放比例

		System.out.println(rate);
		Image Itemp = bufImg.getScaledInstance(bufImg.getWidth(),
				bufImg.getHeight(), BufferedImage.SCALE_SMOOTH);

		AffineTransformOp ato = new AffineTransformOp(
				AffineTransform.getScaleInstance(rate, rate), null);
		Itemp = ato.filter(bufImg, null);
		return (BufferedImage) Itemp;
		// try {
		// ImageIO.write((BufferedImage) Itemp,
		// dest.substring(dest.lastIndexOf(".") + 1), destFile);
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
	}
}
