package com.wss.lsl.test.driven.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * ST店员二维码处理
 * 
 * @author wei.ss
 * @date 2018年1月22日
 * @copyright wonhigh.cn
 */
public class StAssistantQrcode {

	public static void main(String[] args) throws Exception {

		String image2 = "E:\\tmp\\st\\st_2.jpg";
		String dest = "E:\\tmp\\st\\st_2_22.jpg";
		File image3 = new File(dest);
		// image3.deleteOnExit();

		BufferedImage i1 = zoomImage(185);
		BufferedImage i2 = getImage(image2);

		int[] i1rgbArray = new int[i1.getWidth() * i1.getHeight()];
		i1rgbArray = i1.getRGB(0, 0, i1.getWidth(), i1.getHeight(), i1rgbArray,
				0, i1.getWidth());
		// i2.setRGB(59, 180, i1.getWidth(), i1.getWidth(), i1rgbArray, 0,
		// i1.getWidth());

		// 这种方式更优
		Graphics2D g = i2.createGraphics();
		// 消除线条锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(i1, 59, 180, i1.getWidth(), i1.getWidth(), Color.WHITE,
				null);
		g.setFont(new Font("微软雅黑", Font.PLAIN, 23));
		g.setColor(Color.BLACK);
		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_ATOP, 1.0f));

		String saleNo = "150745985";
		// String saleNo = "1";
		g.drawString(saleNo, 122, 397 + 18);
		g.dispose();
		try {
			ImageIO.write(i2, dest.substring(dest.lastIndexOf(".") + 1), image3);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// 合成正面
		frontage();

		System.out.println("Done!");
	}

	// 合成正面
	private static void frontage() throws IOException {
		String image2 = "E:\\tmp\\st\\st_1.jpg";
		String dest = "E:\\tmp\\st\\st_1_11.jpg";
		File image3 = new File(dest);

		BufferedImage i2 = getImage(image2);

		// 这种方式更优
		Graphics2D g = i2.createGraphics();
		// 消除线条锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setFont(new Font("微软雅黑", Font.PLAIN, 23));
		g.setColor(Color.BLACK);

		String name = "宁晓荣";
		String saleNo = "171096107";
		String shopName = "龙华龙观东路天虹";

		int x = 97, y = 372;
		drawString(new Font("微软雅黑", Font.PLAIN, 20), x, y, name, g, true);
		drawString(new Font("微软雅黑", Font.PLAIN, 20), x, y + 47, saleNo, g, true);
		drawString(new Font("微软雅黑", Font.PLAIN, 20), x, y + 47 * 2, shopName,
				g, false);

		g.dispose();
		try {
			ImageIO.write(i2, dest.substring(dest.lastIndexOf(".") + 1), image3);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void drawString(Font font, int x, int y, String text,
			Graphics2D g, boolean textCenter) {

		g.setFont(font);
		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_ATOP, 1.0f));
		int strWidth = g.getFontMetrics().stringWidth(text);
		int totalWwidth = 165;
		if (textCenter && strWidth < totalWwidth) {
			x += (totalWwidth - strWidth) / 2;
		}
		g.drawString(text, x, y);
	}

	private static BufferedImage getImage(String path) throws IOException {
		File file = new File(path);

		return ImageIO.read(file);
	}

	/*
	 * 图片按比率缩放 size为文件大小
	 */
	public static BufferedImage zoomImage(Integer size) throws Exception {
		InputStream is = StAssistantQrcode.class.getResourceAsStream("/st.jpg");
		// String dest = "e:/tmp/st_little.jpg";
		// File destFile = new File(dest);

		BufferedImage bufImg = ImageIO.read(is);
		int width = bufImg.getWidth();

		Double rate = ((double) (size)) / width; // 获取长宽缩放比例

		System.out.println(rate);
		Image Itemp = bufImg.getScaledInstance(bufImg.getWidth(),
				bufImg.getHeight(), BufferedImage.SCALE_SMOOTH);

		AffineTransformOp ato = new AffineTransformOp(
				AffineTransform.getScaleInstance(rate, rate), null);
		Itemp = ato.filter(bufImg, null);
		return (BufferedImage) Itemp;
		// try {
		// ImageIO.write((BufferedImage) Itemp,
		// dest.substring(dest.lastIndexOf(".") + 1), destFile);
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
	}

}
