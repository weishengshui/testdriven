package com.wss.lsl.test.driven.part1;

import java.util.Map;

public class PlainText implements Segment {

	private String text;

	public PlainText(String text) {
		this.text = text;
	}

	@Override
	public boolean equals(Object obj) {
		return text.equals(((PlainText) obj).text);
	}

	@Override
	public String evaluate(Map<String, String> variables) {
		return text;
	}
}
