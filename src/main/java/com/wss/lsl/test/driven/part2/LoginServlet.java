package com.wss.lsl.test.driven.part2;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wss.lsl.test.driven.common.Constants;
import com.wss.lsl.test.driven.part2.service.AuthenticationService;

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String username = req.getParameter(Constants.USERNAME_PARAMETER);
		String password = req.getParameter(Constants.PASSWORD_PARAMETER);
		if (getAuthenticationService().isValidLogin(username, password)) {
			req.getSession().setAttribute("username", username);
			resp.sendRedirect("/frontpage");
		} else {
			resp.sendRedirect("/invalidlogin");
		}
	}

	protected AuthenticationService getAuthenticationService() {

		return null;
	}

}
