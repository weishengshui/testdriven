package com.wss.lsl.test.driven.memcached;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import net.spy.memcached.MemcachedClient;

/**
 * test twemproxy memcached.
 *
 */
public class App {
	public static void main(String[] args) throws IOException,
			InterruptedException {

		List<InetSocketAddress> addrs = new ArrayList<InetSocketAddress>(2);
		addrs.add(new InetSocketAddress("192.168.1.150", 22122));

		MemcachedClient mc = new MemcachedClient(addrs);
		String key = "a";
		mc.set(key, 100, 2);
		mc.set("b", 100, 2);
		mc.set("c", 100, 2);
		mc.set("d", 100, 2);
		// key 含有空格测试。
		// key包含一下字符将抛出异常
		// b == ' ' || b == '\n' || b == '\r' || b == 0
		// mc.set("1 d", 100, 2);
		for (int i = 0; i < 10000; i++) {
			mc.set(i + "", 200, i);
			// TimeUnit.SECONDS.sleep(1);
		}
		// mc.s

		System.out.println("Done");
		// mc.shutdown(5, TimeUnit.SECONDS);
		mc.shutdown();
	}
}
