package com.wss.lsl.test.driven.runnable.factory;

import java.util.concurrent.ThreadFactory;

public class PropertyThreadFactory implements ThreadFactory {

	private int property;

	public PropertyThreadFactory() {
		property = Thread.NORM_PRIORITY;
	}

	public void setProperty(int property) {
		this.property = property;
	}

	public PropertyThreadFactory(int property) {
		super();
		this.property = property;
	}

	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r);
		t.setPriority(this.property);
		return t;
	}

	static class A<K extends Runnable> {

		public Thread newThread(K r) {
			// TODO Auto-generated method stub
			return null;
		}
	}

}
