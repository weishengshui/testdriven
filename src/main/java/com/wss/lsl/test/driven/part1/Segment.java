package com.wss.lsl.test.driven.part1;

import java.util.Map;

public interface Segment {

	String evaluate(Map<String, String> variables);
}
