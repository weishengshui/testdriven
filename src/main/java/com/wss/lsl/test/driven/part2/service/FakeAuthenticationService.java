package com.wss.lsl.test.driven.part2.service;

import java.util.HashMap;
import java.util.Map;

/**
 * 验证登录的伪实现
 * 
 * @author Administrator
 * 
 */
public class FakeAuthenticationService implements AuthenticationService {

	private Map<String, String> users = new HashMap<String, String>();

	@Override
	public void addUser(String username, String password) {
		users.put(username, password);
	}

	@Override
	public boolean isValidLogin(String username, String password) {

		return users.containsKey(username)
				&& password.equals(users.get(username));
	}

}
