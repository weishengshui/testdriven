package com.wss.lsl.test.driven.common.dto.rsa;

import java.io.Serializable;

public class RsaKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// 私钥
	public String privateKey;
	// 公钥
	private String publicKey;

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

}
