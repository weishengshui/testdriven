package com.wss.lsl.test.driven.scoketio.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 给客户端发送内容
 * 
 * @author wei.ss
 *
 */
public class HellServerUdpIo {

	public static void main(String[] args) throws Exception {
		DatagramSocket ds = new DatagramSocket(3000);

		byte[] data = "hello world".getBytes("UTF-8");
		DatagramPacket dp = new DatagramPacket(data, data.length,
				InetAddress.getByName("localhost"), 9000);
		ds.send(dp);
		ds.close();
	}
}
