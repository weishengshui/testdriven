package com.wss.lsl.test.driven.part2.dao.impl;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.wss.lsl.test.driven.part2.dao.PersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;
import com.wss.lsl.test.driven.part2.mapper.PersonRowMapper;

public class JdbcTemplatePersonDao extends JdbcDaoSupport implements PersonDao {

	@Override
	public Person find(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Person person) throws SQLException {

		getJdbcTemplate().update(
				"insert into people(first_name, last_name) values(?, ?)",
				new Object[] { person.getFirstname(), person.getLastname() },
				new int[] { Types.VARCHAR, Types.VARCHAR });
	}

	@Override
	public void update(Person person) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Person person) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Person> findAll() {
		return getJdbcTemplate().query("select * from people",
				new PersonRowMapper());
	}

	@Override
	public List<Person> findByLastname(String lastname) {

		return getJdbcTemplate().query(
				"select * from people where last_name = ?",
				new String[] { lastname }, new PersonRowMapper());
	}

}
