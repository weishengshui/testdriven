package com.wss.lsl.test.driven.effective.entry43;

public interface Printer<T> {
	
	
	void print(Message<T> message);
}
