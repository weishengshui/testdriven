package com.wss.lsl.test.driven.part2.common.time;

/**
 * 时间来源接口
 * 
 * @author Administrator
 *
 */
public interface TimeSource {
	
	long millis() ;
}
