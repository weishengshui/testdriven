package com.wss.lsl.test.driven.spring.factorybean;

import org.springframework.stereotype.Service;

import com.wss.lsl.test.driven.spring.factorybean.model.Role;

@Service("roleService")
public class RoleService {

	public Role queryRoleById(String roleId) {

		Role role = new Role();
		role.setId(roleId);
		role.setRoleName("超级管理员");
		return role;
	}
}
