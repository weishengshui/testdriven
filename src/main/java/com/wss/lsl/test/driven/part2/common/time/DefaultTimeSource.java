package com.wss.lsl.test.driven.part2.common.time;

public class DefaultTimeSource implements TimeSource {

	@Override
	public long millis() {
		return System.currentTimeMillis();
	}

}
