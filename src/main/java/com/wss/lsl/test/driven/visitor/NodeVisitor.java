package com.wss.lsl.test.driven.visitor;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


public class NodeVisitor {

	protected final Map<String, Function<Node, Double>> nameToFunctionMap = new HashMap<>();

	public  Double visitor(Node node) {
		String clsName = node.getClass().getSimpleName();
		String methname = "visit_" + clsName;
		Function<Node, Double> f = nameToFunctionMap.get(methname);
		if (f == null) {
			throw new RuntimeException("未处理的节点类型：" + clsName);
		}
		Double result = f.apply(node);
		return result;
	}

}
