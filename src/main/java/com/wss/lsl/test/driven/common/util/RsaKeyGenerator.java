package com.wss.lsl.test.driven.common.util;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAKeyGenParameterSpec;

import com.wss.lsl.test.driven.common.dto.rsa.RsaKey;

/**
 * rsa 密钥 生成器 TODO 没做好
 * 
 * @author sean
 *
 */
public class RsaKeyGenerator {
	
	public static final String SHA1WITHRSA_ALGORITHM = "SHA1withRsa";
	
	@SuppressWarnings("unused")
	public static RsaKey generatorRsaKey() throws Exception {
		RsaKey rsaKey = null;
		
		KeyPairGenerator generator = KeyPairGenerator.getInstance(SHA1WITHRSA_ALGORITHM);
		RSAKeyGenParameterSpec parameterSpec = new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4);
		generator.initialize(parameterSpec);
		
		KeyPair keyPair = generator.generateKeyPair();
		
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();
		byte[] privateEncoded = privateKey.getEncoded();
		byte[] publicEncoded = publicKey.getEncoded();
		
		return rsaKey;
		
	}
	
	public static void main(String[] args) throws Exception {
		RsaKeyGenerator.generatorRsaKey();
	}
}
