package com.wss.lsl.test.driven.arithmetic.sort;

/**
 * 选择排序
 * 
 * 
 * @author weiss
 *
 */
public class SelectorSort {

	public static int[] sort(int[] data) {

		for (int i = 1; i < data.length; i++) {
			int min = data[i - 1];
			int j = i;
			int minIndex = -1;
			while (j < data.length) {
				if (data[j] < min) {
					min = data[j];
					minIndex = j;
				}
				j++;
			}

			if (minIndex > -1) {// 找到了一个更小的？
				data[minIndex] = data[i - 1];
				data[i - 1] = min;
			}
		}

		return data;
	}
}
