package com.wss.lsl.test.driven.part2.common.time;

import java.util.Date;

/**
 * 抽象系统时间api。<br>
 * 替换时间标准api，方便时间类型的测试
 * 
 * @author Administrator
 * 
 */
public class SystemTime {

	private static final TimeSource DEFAULT_TIME_SOURCE = new DefaultTimeSource();

	private static TimeSource timeSource;

	public static long asMillis() {

		return getTimeSource().millis();
	}

	public static Date asDate() {
		return new Date(asMillis());
	}

	public static void setTimeSource(TimeSource timeSource) {
		SystemTime.timeSource = timeSource;
	}

	public static TimeSource getTimeSource() {
		return timeSource == null ? DEFAULT_TIME_SOURCE : timeSource;
	}

	/**
	 * 重置为默认的时间源
	 */
	public static void reset() {
		setTimeSource(null);
	}
}
