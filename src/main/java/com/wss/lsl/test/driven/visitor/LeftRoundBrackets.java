package com.wss.lsl.test.driven.visitor;

/**
 * 左圆括号
 * 
 * @author weiss
 *
 */
public class LeftRoundBrackets extends UnaryOperator implements RoundBrackets {

	public LeftRoundBrackets(Node right) {
		super(right);
	}
}
