package com.wss.lsl.test.driven.part2.dao;

import com.wss.lsl.test.driven.part2.entity.User;

public interface UserDao {
	
	int save(User user);
	
	int update(User user);
	
}
