package com.wss.lsl.test.driven.runnable.producecustomer;

public class WaitPerson implements Runnable {

	Restaurant restaurant;

	public WaitPerson(Restaurant restaurant) {
		super();
		this.restaurant = restaurant;
	}

	@Override
	public void run() {
		try {
			while (!Thread.interrupted()) {
				synchronized (this) {
					while (restaurant.meal == null) {
						wait();
					}
				}

				System.out.println("get food");

				synchronized (restaurant.cheaf) {
					restaurant.meal = null;
					restaurant.cheaf.notifyAll();
				}

			}
		} catch (InterruptedException e) {
			System.out.println("interrupted waitperson");
		}
	}

}
