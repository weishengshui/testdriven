package com.wss.lsl.test.driven.part2;

public class Customer {

	private float balance;

	public Customer(float initialBalance) {
		this.balance = initialBalance;
	}

	public float getBalance() {
		return this.balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

}
