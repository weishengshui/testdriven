package com.wss.lsl.test.driven.arithmetic.sort;

/**
 * 排序接口
 * 
 * @author wei.ss
 * @date 2017年3月10日
 * @copyright wonhigh.cn
 */
public interface Sort<T extends Comparable<T>> {

	/**
	 * 排序，返回排序后的数据
	 * 
	 * @return
	 * @author wei.ss
	 */
	public T[] sort();
	
	/**
	 * 校验排序结果
	 * 
	 * @author wei.ss
	 */
	public void validate();
}
