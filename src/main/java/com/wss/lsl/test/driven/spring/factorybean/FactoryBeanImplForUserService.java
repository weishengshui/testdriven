package com.wss.lsl.test.driven.spring.factorybean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("dev")
@Service("userService")
public class FactoryBeanImplForUserService implements FactoryBean<UserService> {

	@Autowired
	private RoleService roleService;

	@Override
	public UserService getObject() throws Exception {
		UserService userService = new UserService();
		userService.setRoleService(roleService);
		return userService;
	}

	@Override
	public Class<?> getObjectType() {
		return UserService.class;
	}

}
