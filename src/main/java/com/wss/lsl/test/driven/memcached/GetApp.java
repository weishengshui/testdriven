package com.wss.lsl.test.driven.memcached;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import net.spy.memcached.MemcachedClient;

/**
 * Hello world!
 *
 */
public class GetApp {
	public static void main(String[] args) throws IOException {

		List<InetSocketAddress> addrs = new ArrayList<InetSocketAddress>(2);
		addrs.add(new InetSocketAddress("192.168.1.150", 22122));

		MemcachedClient mc = new MemcachedClient(addrs);
		String key = "a";
		Object result = mc.get(key);
		System.out.println(result);

		System.out.println("Done");
		// mc.shutdown(5, TimeUnit.SECONDS);
		mc.shutdown();
	}
}
