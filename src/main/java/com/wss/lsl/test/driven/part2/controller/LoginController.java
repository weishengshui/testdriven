package com.wss.lsl.test.driven.part2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.wss.lsl.test.driven.common.Constants;
import com.wss.lsl.test.driven.part2.service.AuthenticationService;

public class LoginController implements Controller {

	private AuthenticationService authenticationService;

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String username = request
				.getParameter(Constants.USERNAME_PARAMETER);
		String password = request
				.getParameter(Constants.PASSWORD_PARAMETER);
		if (authenticationService.isValidLogin(username, password)) {
			return new ModelAndView("frontpage");
		}
		return new ModelAndView("wrongpassword");
	}

	public void setAuthenticationService(
			AuthenticationService authenticationService) {

		this.authenticationService = authenticationService;
	}

}
