package com.wss.lsl.test.driven.runnable.producecustomer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Restaurant {

	Meal meal;
	Cheaf cheaf = new Cheaf(this);
	WaitPerson waitPerson = new WaitPerson(this);

	ExecutorService executorService = Executors.newCachedThreadPool();

	public Restaurant() {
		executorService.execute(cheaf);
		executorService.execute(waitPerson);
	}
	
	public static void main(String[] args) {
		new Restaurant();
	}
}
