package com.wss.lsl.test.driven.visitor;

/**
 * 一元计算
 * 
 * @author weiss
 *
 */
public abstract class UnaryOperator extends Node {

	private Node operand;

	public UnaryOperator(Node operand) {
		super();
		this.operand = operand;
	}

	public Node getOperand() {
		return operand;
	}

	public void setOperand(Node operand) {
		this.operand = operand;
	}

}
