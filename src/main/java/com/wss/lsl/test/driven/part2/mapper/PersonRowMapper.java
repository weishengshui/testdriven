package com.wss.lsl.test.driven.part2.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wss.lsl.test.driven.part2.entity.Person;

public class PersonRowMapper implements RowMapper<Person> {

	@Override
	public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
		Person person = new Person();
		person.setFirstname(rs.getString("first_name"));
		person.setLastname(rs.getString("last_name"));
		
		return person;
	}

}
