package com.wss.lsl.test.driven.spring.factorybean;

import com.wss.lsl.test.driven.spring.factorybean.model.Role;
import com.wss.lsl.test.driven.spring.factorybean.model.User;

public class UserService {

	private RoleService roleService;

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public User queryUserById(String id) {

		String roleId = "1";
		Role role = roleService.queryRoleById(roleId);

		User user = new User();
		user.setId(id);
		user.setRoleId(roleId);
		user.setUserName("zhangsan");
		user.setRole(role);

		return user;
	}
}
