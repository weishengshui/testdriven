package com.wss.lsl.test.driven.spring.lookupmethod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public abstract class SingletonBean {

	@Autowired
	private PrototypeBeanB prototypeBeanB;

	public void printLookupMethod() {
		System.out.println(createPrototypeBean()); // 注入的是"prototype"，每次调用都不一样
		// 注入的是单例，不是预期的"prototype"。
		// 或者加上配置@ComponentScan(scopedProxy = ScopedProxyMode.TARGET_CLASS)。可使注入的是"prototype"
		System.out.println(prototypeBeanB); 
	}

	@Lookup("prototypeBean")
	public abstract PrototypeBean createPrototypeBean();
}
