package com.wss.lsl.test.driven.visitor;

/**
 * 右圆括号
 * 
 * @author weiss
 *
 */
public class RightRoundBrackets extends UnaryOperator implements RoundBrackets {

	public RightRoundBrackets(Node left) {
		super(left);
	}
}
