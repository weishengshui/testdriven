package com.wss.lsl.test.driven.part2.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import com.wss.lsl.test.driven.part2.dao.PersonDao;
import com.wss.lsl.test.driven.part2.entity.Person;

public class HibernateTemplatePersonDao extends HibernateDaoSupport implements
		PersonDao {

	@Override
	public Person find(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Person person) {
		getHibernateTemplate().save(person);
	}

	@Override
	public void update(Person person) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Person person) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Person> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> findByLastname(String lastname) {
		try {
			String hql = "from Person where lastname = :lastname";
			Session session = getHibernateTemplate().getSessionFactory()
					.openSession();
			Query query = session.createQuery(hql);
			query.setString("lastname", lastname);
			return query.list();
		} catch (HibernateException e) {
			throw new RuntimeException(e);
		}
	}

}
