package com.wss.lsl.test.driven.visitor;

/**
 * 取负
 * 
 * @author weiss
 *
 */
public class Negate extends UnaryOperator {

	public Negate(Node operand) {
		super(operand);
	}
}
