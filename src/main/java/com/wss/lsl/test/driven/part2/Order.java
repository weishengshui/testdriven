package com.wss.lsl.test.driven.part2;

public class Order {

	private Customer customer;
	private Product product;

	public Order(Customer customer, Product product) {
		this.customer = customer;
		this.product = product;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
