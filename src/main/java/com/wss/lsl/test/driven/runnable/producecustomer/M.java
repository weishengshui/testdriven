package com.wss.lsl.test.driven.runnable.producecustomer;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class M {

	AtomicInteger buf = new AtomicInteger(0);
	P p = new P(this);
	C c = new C(this);
	Random random = new Random(47);

	ExecutorService executorService = Executors.newCachedThreadPool();

	public M() {
		executorService.execute(c);
		executorService.execute(p);
	}

	public static void main(String[] args) {
		new M();
	}

}
