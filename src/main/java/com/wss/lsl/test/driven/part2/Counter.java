package com.wss.lsl.test.driven.part2;

public class Counter {

	private int count;

	public Counter() {
		this.count = 0;
	}

	public int value() {
		return this.count;
	}

	public synchronized void increment() {
		this.count++;
	}

}
