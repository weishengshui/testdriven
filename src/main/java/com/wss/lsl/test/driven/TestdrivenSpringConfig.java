package com.wss.lsl.test.driven;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
@ComponentScan(value = "com.wss.lsl.test.driven.spring", scopedProxy = ScopedProxyMode.TARGET_CLASS)
public class TestdrivenSpringConfig {

}
