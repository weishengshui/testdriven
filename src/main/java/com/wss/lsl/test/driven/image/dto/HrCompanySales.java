package com.wss.lsl.test.driven.image.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * HR店员。3.7.3添加
 * 
 * @author wei.ss
 * @date 2017年12月19日
 * @copyright wonhigh.cn
 */
public class HrCompanySales implements Serializable {

	/**
	 * 序列ID
	 */
	private static final long serialVersionUID = 1L;

	private String brandCode; // 品牌编码
	private String shopOrgCodeLong; // 门店长编码
	private String shopName; // 门店名称
	private String affiliationCode; // 本部编码
	private String memberId; // 店员的memberId
	private String salesId; // 零售店员主键

	private String id; // 主键
	private String zoneName;// 地区
	private String provinceName; // 子地区
	private String mangerCity; // 管理城市
	private String bizCity; // 经营城市
	private String costCenter; // 成本中心
	private String companyName; // 签约公司
	private String unitName;// 组织名称
	private String unitCode; // 组织编码
	private String unitTypeName;// 组织类型
	private String storeCode;// 零售代码
	private String employeeCode;// 工号
	private String employeeName;// 姓名
	private int sex;// 性别:0:男 1:女
	private String jobName;// 职务
	private String jobCode;// 职务编码
	private String jobTypeName;// 职务类型
	private String positionName;// 岗位
	private String positionCode;// 岗位编码
	private String blocJobName;// 集团职务
	private String blocPositionName;// 集团岗位
	private String uperEmployeeName;// 直接上级
	private String uperPositionName;// 直接上级岗位
	private String entryDate;// 入职日期
	private String confirmationTime; // 转正日期
	private int regularType;// 转正类型:1:提前转正 2:延期转正 3:到期转正 4:弃用
	private String phoneNo;// 移动电话
	private String workMail;// 工作邮箱
	private String belleStarttime;// 百丽工龄起算日
	private String incumbencySeniority;// 百丽在职工龄
	private String socialSeniority;// 社会工龄
	private String startWordTtime;// 从事本岗日期
	private String preparationTypeName;// 编制类型
	private String employeePropertyName;// 员工性质
	private String nationality;// 国籍
	private String birthday;// 出生日期
	private String cardType;// 证件类型
	private String cardId;// 证件号码
	private int employeeAge;// 年龄
	private String qualifications;// 学历
	private String school;// 学校
	private String profession;// 专业
	private String whetherMarray;// 婚姻状况:未婚 已婚
	private int whetherBirth;// 是否生育:0否 1是
	private String bankName;// 工资卡银行
	private String bankAccountNum;// 工资卡号
	private int fromType = 3;// 来源类型（1-预入职 2-面试登记 3-员工信息新增 4-员工信息导入）
	private String city;// 工作地点
	private int oaSecurity;// OA安全级别
	private int employeeStatus;// -1：临时1:试用 2:正式4:离职 5:离职在途 6:退休 8:实习
	private Integer leaveType;// 1：辞职（主动）；2：辞职（被动）；3：辞退；
	private int effectStatus = 0;// 生效状态（0-未生效，1-已生效）
	private String userNo;// 登录账户
	private String password;// 密码

	private String createUser;
	private Date createTime;
	private String updateUser;
	private Date updateTime;
	private Integer isValid;

	// 非数据库字段
	private int modelType;
	private String modelName;
	private String brandName;
	private String wxTicktUrl; // 卡券二维码地址
	private String memberLoginname;// 会员账号
	private String memberWxTicktUrl; // 店员二维码
	private Integer recruitMemberNum = 0; // 招募会员数量

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getMangerCity() {
		return mangerCity;
	}

	public void setMangerCity(String mangerCity) {
		this.mangerCity = mangerCity;
	}

	public String getBizCity() {
		return bizCity;
	}

	public void setBizCity(String bizCity) {
		this.bizCity = bizCity;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getUnitTypeName() {
		return unitTypeName;
	}

	public void setUnitTypeName(String unitTypeName) {
		this.unitTypeName = unitTypeName;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobTypeName() {
		return jobTypeName;
	}

	public void setJobTypeName(String jobTypeName) {
		this.jobTypeName = jobTypeName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getBlocJobName() {
		return blocJobName;
	}

	public void setBlocJobName(String blocJobName) {
		this.blocJobName = blocJobName;
	}

	public String getBlocPositionName() {
		return blocPositionName;
	}

	public void setBlocPositionName(String blocPositionName) {
		this.blocPositionName = blocPositionName;
	}

	public String getUperEmployeeName() {
		return uperEmployeeName;
	}

	public void setUperEmployeeName(String uperEmployeeName) {
		this.uperEmployeeName = uperEmployeeName;
	}

	public String getUperPositionName() {
		return uperPositionName;
	}

	public void setUperPositionName(String uperPositionName) {
		this.uperPositionName = uperPositionName;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getConfirmationTime() {
		return confirmationTime;
	}

	public void setConfirmationTime(String confirmationTime) {
		this.confirmationTime = confirmationTime;
	}

	public int getRegularType() {
		return regularType;
	}

	public void setRegularType(int regularType) {
		this.regularType = regularType;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getWorkMail() {
		return workMail;
	}

	public void setWorkMail(String workMail) {
		this.workMail = workMail;
	}

	public String getBelleStarttime() {
		return belleStarttime;
	}

	public void setBelleStarttime(String belleStarttime) {
		this.belleStarttime = belleStarttime;
	}

	public String getIncumbencySeniority() {
		return incumbencySeniority;
	}

	public void setIncumbencySeniority(String incumbencySeniority) {
		this.incumbencySeniority = incumbencySeniority;
	}

	public String getSocialSeniority() {
		return socialSeniority;
	}

	public void setSocialSeniority(String socialSeniority) {
		this.socialSeniority = socialSeniority;
	}

	public String getStartWordTtime() {
		return startWordTtime;
	}

	public void setStartWordTtime(String startWordTtime) {
		this.startWordTtime = startWordTtime;
	}

	public String getPreparationTypeName() {
		return preparationTypeName;
	}

	public void setPreparationTypeName(String preparationTypeName) {
		this.preparationTypeName = preparationTypeName;
	}

	public String getEmployeePropertyName() {
		return employeePropertyName;
	}

	public void setEmployeePropertyName(String employeePropertyName) {
		this.employeePropertyName = employeePropertyName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public int getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(int employeeAge) {
		this.employeeAge = employeeAge;
	}

	public String getQualifications() {
		return qualifications;
	}

	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getWhetherMarray() {
		return whetherMarray;
	}

	public void setWhetherMarray(String whetherMarray) {
		this.whetherMarray = whetherMarray;
	}

	public int getWhetherBirth() {
		return whetherBirth;
	}

	public void setWhetherBirth(int whetherBirth) {
		this.whetherBirth = whetherBirth;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(String bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}

	public int getFromType() {
		return fromType;
	}

	public void setFromType(int fromType) {
		this.fromType = fromType;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getOaSecurity() {
		return oaSecurity;
	}

	public void setOaSecurity(int oaSecurity) {
		this.oaSecurity = oaSecurity;
	}

	public int getEmployeeStatus() {
		return employeeStatus;
	}

	public void setEmployeeStatus(int employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	public Integer getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(Integer leaveType) {
		this.leaveType = leaveType;
	}

	public int getEffectStatus() {
		return effectStatus;
	}

	public void setEffectStatus(int effectStatus) {
		this.effectStatus = effectStatus;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getShopOrgCodeLong() {
		return shopOrgCodeLong;
	}

	public void setShopOrgCodeLong(String shopOrgCodeLong) {
		this.shopOrgCodeLong = shopOrgCodeLong;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAffiliationCode() {
		return affiliationCode;
	}

	public void setAffiliationCode(String affiliationCode) {
		this.affiliationCode = affiliationCode;
	}

	public int getModelType() {
		return modelType;
	}

	public void setModelType(int modelType) {
		this.modelType = modelType;
	}

	public String getModelName() {
		return modelName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getIsValid() {
		return isValid;
	}

	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getWxTicktUrl() {
		return wxTicktUrl;
	}

	public void setWxTicktUrl(String wxTicktUrl) {
		this.wxTicktUrl = wxTicktUrl;
	}

	public String getMemberLoginname() {
		return memberLoginname;
	}

	public void setMemberLoginname(String memberLoginname) {
		this.memberLoginname = memberLoginname;
	}

	public String getMemberWxTicktUrl() {
		return memberWxTicktUrl;
	}

	public void setMemberWxTicktUrl(String memberWxTicktUrl) {
		this.memberWxTicktUrl = memberWxTicktUrl;
	}

	public Integer getRecruitMemberNum() {
		return recruitMemberNum;
	}

	public void setRecruitMemberNum(Integer recruitMemberNum) {
		this.recruitMemberNum = recruitMemberNum;
	}

	@Override
	public String toString() {
		return "HrCompanySales [brandCode=" + brandCode + ", shopOrgCodeLong="
				+ shopOrgCodeLong + ", shopName=" + shopName
				+ ", affiliationCode=" + affiliationCode + ", memberId="
				+ memberId + ", salesId=" + salesId + ", id=" + id
				+ ", zoneName=" + zoneName + ", provinceName=" + provinceName
				+ ", mangerCity=" + mangerCity + ", bizCity=" + bizCity
				+ ", costCenter=" + costCenter + ", companyName=" + companyName
				+ ", unitName=" + unitName + ", unitCode=" + unitCode
				+ ", unitTypeName=" + unitTypeName + ", storeCode=" + storeCode
				+ ", employeeCode=" + employeeCode + ", employeeName="
				+ employeeName + ", sex=" + sex + ", jobName=" + jobName
				+ ", jobCode=" + jobCode + ", jobTypeName=" + jobTypeName
				+ ", positionName=" + positionName + ", positionCode="
				+ positionCode + ", blocJobName=" + blocJobName
				+ ", blocPositionName=" + blocPositionName
				+ ", uperEmployeeName=" + uperEmployeeName
				+ ", uperPositionName=" + uperPositionName + ", entryDate="
				+ entryDate + ", confirmationTime=" + confirmationTime
				+ ", regularType=" + regularType + ", phoneNo=" + phoneNo
				+ ", workMail=" + workMail + ", belleStarttime="
				+ belleStarttime + ", incumbencySeniority="
				+ incumbencySeniority + ", socialSeniority=" + socialSeniority
				+ ", startWordTtime=" + startWordTtime
				+ ", preparationTypeName=" + preparationTypeName
				+ ", employeePropertyName=" + employeePropertyName
				+ ", nationality=" + nationality + ", birthday=" + birthday
				+ ", cardType=" + cardType + ", cardId=" + cardId
				+ ", employeeAge=" + employeeAge + ", qualifications="
				+ qualifications + ", school=" + school + ", profession="
				+ profession + ", whetherMarray=" + whetherMarray
				+ ", whetherBirth=" + whetherBirth + ", bankName=" + bankName
				+ ", bankAccountNum=" + bankAccountNum + ", fromType="
				+ fromType + ", city=" + city + ", oaSecurity=" + oaSecurity
				+ ", employeeStatus=" + employeeStatus + ", leaveType="
				+ leaveType + ", effectStatus=" + effectStatus + ", userNo="
				+ userNo + ", password=" + password + ", createUser="
				+ createUser + ", createTime=" + createTime + ", updateUser="
				+ updateUser + ", updateTime=" + updateTime + ", isValid="
				+ isValid + ", modelType=" + modelType + ", modelName="
				+ modelName + ", brandName=" + brandName + ", wxTicktUrl="
				+ wxTicktUrl + ", memberLoginname=" + memberLoginname
				+ ", memberWxTicktUrl=" + memberWxTicktUrl
				+ ", recruitMemberNum=" + recruitMemberNum + "]";
	}

}
