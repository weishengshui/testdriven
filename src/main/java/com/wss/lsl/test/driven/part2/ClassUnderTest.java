package com.wss.lsl.test.driven.part2;

import com.wss.lsl.test.driven.part2.dao.UserDao;
import com.wss.lsl.test.driven.part2.entity.User;

public class ClassUnderTest {

	private UserDao niceUserDao;

	private UserDao strictUserDao;

	public int niceSave(User user) {

		return niceUserDao.save(user);
	}

	public int niceUpdate(User user) {

		return niceUserDao.update(user);
	}

	public int strictSave(User user) {

		return strictUserDao.save(user);
	}

	public int strictUpdate(User user) {

		return strictUserDao.update(user);
	}

	public void setNiceUserDao(UserDao niceUserDao) {
		this.niceUserDao = niceUserDao;
	}

	public void setStrictUserDao(UserDao strictUserDao) {
		this.strictUserDao = strictUserDao;
	}

}
