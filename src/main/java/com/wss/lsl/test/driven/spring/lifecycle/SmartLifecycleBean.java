package com.wss.lsl.test.driven.spring.lifecycle;

import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Service;

@Service
public class SmartLifecycleBean implements SmartLifecycle {

	private volatile boolean running;

	// isAutoStartup()返回true，会自动调start()
	@Override
	public void start() {
		System.out.println("SmartLifecycleBean start");
		running = true;

	}

	@Override
	public void stop() {
		System.out.println("SmartLifecycleBean stop");
		running = false;

	}

	@Override
	public boolean isRunning() {
		System.out.println("SmartLifecycleBean isRunning");
		return running;
	}

	@Override
	public int getPhase() {
		System.out.println("SmartLifecycleBean getPhase");
		return 0;
	}

	@Override
	public boolean isAutoStartup() {
		System.out.println("SmartLifecycleBean getPhase");
		return true;
	}

	@Override
	public void stop(Runnable callback) {
		System.out.println("SmartLifecycleBean stopcallback");
		stop();
		callback.run();
	}

}
