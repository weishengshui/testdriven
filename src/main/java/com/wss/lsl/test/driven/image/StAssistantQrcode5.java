package com.wss.lsl.test.driven.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.wss.lsl.test.driven.image.dto.HrCompanySales;
import com.wss.lsl.test.driven.util.CollectionUtil;

/**
 * ST工牌二期处理
 * 
 * @author wei.ss
 * @date 2018年3月23日
 * @copyright wonhigh.cn
 */
public class StAssistantQrcode5 {

	private static final Logger LOG = LoggerFactory
			.getLogger(StAssistantQrcode5.class);

	// 边框的高度和宽度
	private static final int BORDER_H = 165;
	private static final int BORDER_W = 434;
	// 二维码定位
	private static final int QR_BORDER_W = 123;
	private static final int QR_BORDER_H = 121;

	// 单张图片宽度、高度
	private static final int IMAGE_W = 660;
	private static final int IMAGE_H = 1080;

	// 二维码大小
	private static final int QRCODE_SIZE = 410 + 5;

	private static final int ROW_COUNT = 2;
	private static final int COL_COUNT = 4;

	// 一张纸承载工牌的数量
	private static final int PAGE_SIZE = ROW_COUNT * COL_COUNT;

	// 合成反面
	public static File mergeReverse(File destDir, File reverseImage,
			List<File> qrcodeFiles, List<HrCompanySales> sales, int pageNo)
			throws Exception {
		File image3 = new File(destDir, "第" + pageNo + "页--反面.jpg");

		BufferedImage i2 = getImage(reverseImage);

		// 这种方式更优
		Graphics2D g = i2.createGraphics();
		// 消除线条锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		int index = 0;
		File qrcodeFile = null;
		BufferedImage i1 = null;
		int xx = 0, yy =0;
		HrCompanySales sale = null;
		Font font = new Font("微软雅黑", Font.PLAIN, 85);
		Color color = Color.BLUE;
		outer: for (int i = 0; i < ROW_COUNT; i++) {
			// 反面的顺序反过来
			for (int j = COL_COUNT - 1; j >= 0; j--) {
				index = i * (COL_COUNT) + (COL_COUNT - j - 1);
				if (index >= sales.size()) {
					break outer;
				}
				sale = sales.get(index);
				qrcodeFile = qrcodeFiles.get(index);
				i1 = zoomImage(qrcodeFile);
				g.drawImage(i1, BORDER_W + ((IMAGE_W) * j) + QR_BORDER_W,
						BORDER_H + (IMAGE_H-5) * i + QR_BORDER_H, i1.getWidth(),
						i1.getWidth(), Color.WHITE, null);
				
				// 标识，需要删除。TODO
				xx =  BORDER_W + ((IMAGE_W) * j) + QR_BORDER_W;
				yy = BORDER_H + (IMAGE_H-5) * i + QR_BORDER_H;
				font = new Font("微软雅黑", Font.PLAIN, 85);
				g.setFont(font);
				g.setColor(color);
				g.drawString(sale.getEmployeeName(),xx, yy);
			}
		}

		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_ATOP, 1.0f));
		g.dispose();

		ImageIO.write(i2, "jpg", image3);

		return image3;
	}

	// 合成正面
	private static File frontage(File destDir, File frontImage,
			List<HrCompanySales> sales, int pageNo) throws IOException {
		File image3 = new File(destDir, "第" + pageNo + "页--正面.jpg");

		BufferedImage i2 = getImage(frontImage);

		// 这种方式更优
		Graphics2D g = i2.createGraphics();
		// 消除线条锯齿
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		int x = 67, y = 540;
		int borderH = BORDER_H;
		int imageH = IMAGE_H;
		Font font = null;
		Color color = null;
		// color = new Color(0xd5, 0xc8, 0xbe);
		color = Color.BLACK;

		int index = 0;
		HrCompanySales sale = null;
		int xx = 0, yy = 0;
		outer: for (int i = 0; i < ROW_COUNT; i++) {
			// 反面的顺序反过来
			for (int j = 0; j < COL_COUNT; j++) {
				index = i * (COL_COUNT) + j;
				if (index >= sales.size()) {
					break outer;
				}
				sale = sales.get(index);
				
				xx =  BORDER_W + (IMAGE_W * j)
						+ x;
				yy = borderH + imageH * i + y;
				font = new Font("微软雅黑", Font.PLAIN, 74);
				g.setFont(font);
				g.setColor(color);
				g.drawString(sale.getEmployeeName(),xx, yy);
				System.out.println(String.format("i=%d,j=%d:xx=%d,yy=%d", i, j, xx, yy));
				
				// 字体大小是名字的0.8
				font = new Font("微软雅黑", Font.PLAIN, 74);
				g.setFont(font);
				g.setColor(color);
				g.drawString(sale.getEmployeeCode(), BORDER_W + (IMAGE_W * j)
						+ x, borderH + imageH * i + y + (85 + 20));
				
				// 字体大小是名字的8/14
				font = new Font("微软雅黑", Font.PLAIN, 50);
				g.setFont(font);
				g.setColor(color);
				g.drawString(sale.getShopName(), BORDER_W + (IMAGE_W * j) + x,
						borderH + imageH * i + y + (85 + 45) * 2 - 75);
			}
		}

		g.setComposite(AlphaComposite
				.getInstance(AlphaComposite.SRC_ATOP, 1.0f));
		g.dispose();
		ImageIO.write(i2, "jpg", image3);

		return image3;
	}

	private static BufferedImage getImage(File file) throws IOException {
		return ImageIO.read(file);
	}

	/*
	 * 图片按比率缩放 size为文件大小
	 */
	public static BufferedImage zoomImage(File srcQrcodeFile) throws Exception {
		BufferedImage bufImg = ImageIO.read(srcQrcodeFile);
		int width = bufImg.getWidth();

		double rate = ((double) (QRCODE_SIZE)) / width; // 获取长宽缩放比例

		Image Itemp = bufImg.getScaledInstance(bufImg.getWidth(),
				bufImg.getHeight(), BufferedImage.SCALE_SMOOTH);

		AffineTransformOp ato = new AffineTransformOp(
				AffineTransform.getScaleInstance(rate, rate), null);
		Itemp = ato.filter(bufImg, null);
		return (BufferedImage) Itemp;
	}

	public static void main(String[] args) throws Exception {

		File destDir = new File("E:\\tmp\\st\\2\\22");
		File frontImage = new File("E:\\tmp\\st\\2\\st_11.jpg");
		File reverseImage = new File("E:\\tmp\\st\\2\\st_22.jpg");

		List<HrCompanySales> specialSales = new ArrayList<HrCompanySales>();
		List<File> qrcodeFiles = new ArrayList<>();
		File qrcodeFile = new File("E:\\tmp\\st\\2\\showqrcode15.jpg");
		int count = 18;
		int employeeCode = 131115453;
		HrCompanySales sales = null;
		for (int i = 0; i < count; i++) {
			// 店员信息
			sales = new HrCompanySales();
			sales.setEmployeeCode(employeeCode++ + "");
			sales.setEmployeeName("店员名字" + (i + 1));
			sales.setShopName("南山沙河天虹ST" + (i + 1));
			specialSales.add(sales);

			// 二维码信息
			qrcodeFiles.add(qrcodeFile);
		}

		List<File> saleImages = mergeImage(destDir, frontImage, reverseImage,
				specialSales, qrcodeFiles);

		System.out.println(JSON.toJSONString(saleImages));
	}

	public static List<File> mergeImage(File destDir, File frontImage,
			File reverseImage, List<HrCompanySales> specialSales,
			List<File> qrcodeFiles) throws Exception {

		if (CollectionUtil.isEmpty(specialSales)) {
			LOG.warn("没有工牌需要合成");
			return null;
		}
		int pageCount = specialSales.size() / PAGE_SIZE;
		pageCount = specialSales.size() % PAGE_SIZE == 0 ? pageCount
				: pageCount + 1;
		List<File> files = new ArrayList<File>(pageCount * 2);
		List<HrCompanySales> tempSales = null;
		List<File> tempQrcodeFiles = null;
		int toIndex = 0;
		int fromIndex = 0;
		for (int i = 0; i < pageCount; i++) {
			fromIndex = i * PAGE_SIZE;
			toIndex = fromIndex + PAGE_SIZE;
			if (toIndex > specialSales.size()) {
				toIndex = specialSales.size();
			}
			tempSales = specialSales.subList(fromIndex, toIndex);
			tempQrcodeFiles = qrcodeFiles.subList(fromIndex, toIndex);

			// 合成正面
			files.add(frontage(destDir, frontImage, tempSales, i + 1));
			// 合成反面
			files.add(mergeReverse(destDir, reverseImage, tempQrcodeFiles,
					tempSales, i + 1));
		}

		// 把二维码图片删掉
		// for (File qrcodeFile : qrcodeFiles) {
		// TODO 正式代码要删掉
		// qrcodeFile.delete();
		// }

		return files;
	}

}
