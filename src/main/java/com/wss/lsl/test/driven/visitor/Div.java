package com.wss.lsl.test.driven.visitor;

/**
 * 加法
 * 
 * @author weiss
 *
 */
public class Div extends BinaryOperator {

	public Div(Node left, Node right) {
		super(left, right);
	}
}
