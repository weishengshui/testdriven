package com.wss.lsl.test.driven.visitor;

public class Number extends Node {

	private double value;

	public Number(double value) {
		super();
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

}
