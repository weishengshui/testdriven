package com.wss.lsl.test.driven.part2;

/**
 * 订单价格计算的伪实现
 * 
 * @author Administrator
 * 
 */
public class PricingServiceTestDouble extends PricingService {

	private float discount;

	public PricingServiceTestDouble(float discount) {
		super();
		this.discount = discount;
	}

	@Override
	public float getDiscountPercentage(Customer c, Product p) {
		return this.discount;
	}
}
