package com.wss.lsl.test.driven.util;

import java.util.Collection;
import java.util.Map;

/**
 * 集合校验工具类
 * 
 * @author wang.c
 * @date 2016年11月9日
 * @copyright wonhigh.cn
 */
public class CollectionUtil {

	/**
	 * 
	 * Collection判空
	 * 
	 * @param collection
	 * @return
	 * @author wang.c
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return (collection == null || collection.isEmpty());
	}

	/**
	 * 
	 * map判空
	 * 
	 * @param map
	 * @return
	 * @author wang.c
	 */
	public static boolean isEmpty(Map<?, ?> map) {
		return (map == null || map.isEmpty());
	}
}
