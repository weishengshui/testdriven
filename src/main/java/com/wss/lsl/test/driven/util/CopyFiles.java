package com.wss.lsl.test.driven.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashSet;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class CopyFiles {

	private static final Set<String> ignoreNames = new HashSet<String>();

	static {
		ignoreNames.add(".metadata");
		ignoreNames.add(".recommenders");
		ignoreNames.add(".settings");
		ignoreNames.add("build");
		ignoreNames.add(".svn");
		ignoreNames.add("bin");
		ignoreNames.add("target");
	}

	private static final FilenameFilter ignoreFiles = new FilenameFilter() {

		public boolean accept(File dir, String name) {
			return !ignoreNames.contains(dir.getName());
		}
	};

	public static void main(String[] args) throws IOException {
		File fromDir = new File("E:\\wonhigh_workspace");
		File toDir = new File("E:\\wonhigh_workspace2");
		copyFile(fromDir, toDir);
		System.out.println("Done!");
		// FileInputStream fis = new FileInputStream(dir);
		// FileChannel fc = fis.getChannel();
		//
		// fc.transferTo(0, fc.size(), new FileOutputStream(new
		// File("")).getChannel());
		//
		// fc.close();
		// fis.close();
	}

	public static void copyFile(File file1, File file2) throws IOException {
		if (file1.isDirectory()) {
			if (!file2.exists()) {
				file2.mkdirs();
			}
			File[] files = file1.listFiles(ignoreFiles);
			for (File file3 : files) {
				copyFile(file3, new File(file2, file3.getName()));
			}
		} else {
			FileInputStream fis = null;
			FileChannel fc = null;

			FileOutputStream fos = null;
			FileChannel foc = null;

			fis = new FileInputStream(file1);
			fos = new FileOutputStream(file2);
			fc = fis.getChannel();
			foc = fos.getChannel();
			fc.transferTo(0, fc.size(), foc);
			System.out.println(String.format("copyed %s-->%s",
					file1.getAbsolutePath(), file2.getAbsolutePath()));

			fc.close();
			foc.close();
			fis.close();
			fos.close();
		}
	}

}
