var page = require('webpage').create();
var system = require('system');
var url = system.args[1];
var pdfFilePath = system.args[2];

console.log(url);
console.log(pdfFilePath);
page.open(url, function(status) {
	console.log("Status: " + status);
	if (status === "success") {
		console.log("page success");
	}
});

function waitFor(testFx) {
	setTimeout(
			function() {
				var condition = (typeof (testFx) === "string" ? eval(testFx)
						: testFx());
				phantom.exit();
			}, 10000);
};

page.onLoadFinished = function() {
	console.log("page.onLoadFinished");
	// printArgs.apply(this, arguments);
	var p = page;
	var f = function() {
		console.log("  ", window.outerHeight, "   ");
		
		var windowHeight = page.evaluate(function(){
			return document.body.scrollHeight;
		});
		console.log("windowHeight  ", windowHeight, "   ");
		p.viewportSize = {
				width : 1250
				,height : windowHeight
		};
		
		page.render(pdfFilePath, {
			format : 'pdf',
			quality : '100'
		});
		console.log("success");
	};
	waitFor(f);
};

function printArgs() {
	var i, ilen;
	for (i = 0, ilen = arguments.length; i < ilen; ++i) {
		console.log("    arguments[" + i + "] = "
				+ JSON.stringify(arguments[i]));
	}
	console.log("");
}

page.onConsoleMessage = function() {
	printArgs.apply(this, arguments);
};