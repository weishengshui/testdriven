const puppeteer = require('puppeteer');

// 命令行参数。用户输入的参数从第3个开始，即process.argv[2]
console.log(process.argv);

(async () => {
  const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  await page.emulate({
                 viewport: { width: 1920, height: 1080 },
                 userAgent:
                   'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
               });
  // const url = "https://www.baidu.com/";
  await page.goto(url, {waitUntil: ['domcontentloaded', 'networkidle0']});
  // 当发生页面js代码没有捕获的异常时触发。
  page.on('pagerror', () => {})
  // 当页面崩溃时触发。
  page.on('error', () => {})
  
  const html = await page.evaluate(() => {
	  // 点击展开
	let tds = document.querySelectorAll('.td-detail');
        if (tds && tds.length && tds.length > 0) {
		for(let i = 0; i < tds.length; i++) {
            		tds[i].click();
		}
	}
        // 点击展开
	let btns = document.querySelectorAll('button.el-button');
        if (btns && btns.length && btns.length > 0) {
                for(let i = 0; i < btns.length; i++) {
                        btns[i].click();
                }
        }
    return document.querySelector('html').innerHTML;
	});
  const pageWidth =  await page.evaluate(function(){return document.body.scrollWidth;});
  const pageHeight =  await page.evaluate(function(){return document.body.scrollHeight;});
  // await page.screenshot({path: 'example.png', type: "png", clip:{x: 0, y: 0, width:20000, height: pageHeight}});
  // await page.screenshot({path: 'example.png', type: "png", fullPage: true});
  // await page.waitFor(3000);
  // await page.emulateMedia('print');
  // await page.emulateMedia("screen");
  await page.pdf({path: "example.pdf", format: "A2", printBackground: true, margin:{top: "2cm", bottom: "2cm"}});

  await browser.close();
})();
