// const puppeteer = require('C:\\Users\\weiss\\node_modules\\puppeteer');
const puppeteer = require('/root/node_modules/puppeteer');

// 命令行参数。用户输入的参数从第3个开始，即process.argv[2]
var env = process.argv[2];
var reportType = process.argv[3];

var domains = {};
domains["dev"] = "https://treport.1hykj.com";
domains["testhy"] = "https://treport.1hykj.com";
domains["prehy"] = "https://prereport.1hykj.com";
domains["prohy"] = "https://report.1hykj.com";

var reportPaths = {};
reportPaths["AntiFraudSample"] = "/persionalBeforeLoanReport";
reportPaths["AntiFraudEnterprice"] = "/enterpriseLoanReport";
reportPaths["TAOBAO_E"] = "/taobaoReport";
reportPaths["MULTI_SOURCES_LOANS"] = "/longTermLending";
reportPaths["CALL_RISK"] = "/callRisk";
reportPaths["BLACK_LIST"] = "/blackList";
reportPaths["PersonalCreditScore"] = "/persionalCreditScoreReport";

const url_prefix = domains[env];
const report_path = reportPaths[reportType];
const id = process.argv[4];
const pdfPath = process.argv[5];

console.log(url_prefix);
console.log(report_path);
console.log(id);
console.log(pdfPath);
(async () => {
  const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  await page.emulate({
                 viewport: { width: 1920, height: 1080 },
                 userAgent:
                   'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
               });
  
  // const url = "https://treport.1hykj.com/enterpriseLoanReport?id=8a2831786cd0b176016cd0b625eb0008";
  const url = url_prefix + report_path + "?expansion=true&id=" + id;
  console.log(url);
  
  await page.goto(url, {waitUntil: ['domcontentloaded', 'networkidle0']});  
  console.log("x111");
  
  const pageWidth =  await page.evaluate(function(){return document.body.scrollWidth;});
  const pageHeight =  await page.evaluate(function(){return document.body.scrollHeight;});
  
  await page.pdf({path: pdfPath, format: "A2", printBackground: true, margin:{top: "2cm", bottom: "2cm"}});

  await browser.close();
  console.log("export pdf success");
})();
